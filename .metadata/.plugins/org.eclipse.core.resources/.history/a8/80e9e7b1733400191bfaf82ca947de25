package ca.medavie.subrogation.ui;

import java.util.Locale;

import javax.faces.context.FacesContext;

import org.springframework.context.i18n.LocaleContextHolder;

/**
 * Responsible for maintaining the language actively used in the application
 */
public class LocaleManager extends LocalManager
{
    private static Locale currentLocale = Locale.ENGLISH;
    private static Locale nextLocale = Locale.FRENCH;


    /**
     * Default ctor
     */
    public LocaleManager()
    {
        // set up locals
        LocaleContextHolder.setLocale( currentLocale );
        FacesContext.getCurrentInstance().getViewRoot().setLocale( currentLocale );
    }


    /**
     * Change the locale based on the value of the newLocale property
     * 
     * @return Always returns to calling UI 
     */
    public static String changeLocale()
    {
        LocaleContextHolder.setLocale( nextLocale );
        FacesContext.getCurrentInstance().getViewRoot().setLocale( nextLocale );

        // switch locals
        nextLocale = currentLocale;
        currentLocale = LocaleContextHolder.getLocale();        
        return null;
    }


    /**
     * @return Returns the locale code as a string
     */
    public Locale getLocale()
    {
        return currentLocale;
    }


    /**
     * @return Returns the localizedDisplayLanguage.
     */
    public String getChangelocalDisplayLanguage()
    {
        return nextLocale.getDisplayLanguage( nextLocale );
    }
}
