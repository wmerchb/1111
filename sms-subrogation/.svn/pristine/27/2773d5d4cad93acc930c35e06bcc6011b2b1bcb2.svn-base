
package ca.medavie.subrogation.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import org.apache.log4j.Logger;

import com.icesoft.faces.component.panelpopup.PanelPopup;
import com.icesoft.faces.context.effects.JavascriptContext;

/**
 * Bean manager for the pop-up window
 */
public class PopUpBean
{

    private static final Logger logger = Logger.getLogger( PopUpBean.class );
    /**
     * The singleton PopUpBean instance
     */
    private static PopUpBean instance = null;

    /**
     * Toggle render the pop-up window
     */
    private Boolean render = false;

    /**
     * Collection of the messages to be displayed by to the user
     */
    private Collection<FacesMessage> systemMessages = new ArrayList<FacesMessage>();

    /**
     * Message header
     */
    private String messageHeader;

    /**
     * The image style class - default info image
     */
    private String imageStyle = "icePnlGrdImageInfo";

    /**
     * A MessageFactory used to map service message to web application message bundles
     */
    private static MessageFactory messageFactory = MessageFactory.getInstance();

    /**
     * Map containing the various image style classes
     */
    private static Map<Severity, String> imageStyles = new HashMap<Severity, String>();
    static
    {
        imageStyles.put( FacesMessage.SEVERITY_ERROR, "icePnlGrdImageError" );
        imageStyles.put( FacesMessage.SEVERITY_FATAL, "icePnlGrdImageError" );
        imageStyles.put( FacesMessage.SEVERITY_INFO, "icePnlGrdImageInfo" );
        imageStyles.put( FacesMessage.SEVERITY_WARN, "icePnlGrdImageWarn" );
    }

    /**
     * Localized message headers
     */
    private static final Map<MapKey, String> messageHeaders = new HashMap<MapKey, String>();
    static
    {
        messageHeaders.put( MapKey.ENGLISH_ERROR,
            messageFactory.getMessageStringForKey( "ui_header_error", Locale.ENGLISH ) );
        messageHeaders.put( MapKey.ENGLISH_FATAL,
            messageFactory.getMessageStringForKey( "ui_header_fatal", Locale.ENGLISH ) );
        messageHeaders.put( MapKey.ENGLISH_INFO,
            messageFactory.getMessageStringForKey( "ui_header_info", Locale.ENGLISH ) );
        messageHeaders.put( MapKey.ENGLISH_WARNING,
            messageFactory.getMessageStringForKey( "ui_header_warning", Locale.ENGLISH ) );

        messageHeaders.put( MapKey.FRENCH_ERROR,
            messageFactory.getMessageStringForKey( "ui_header_error", Locale.FRENCH ) );
        messageHeaders.put( MapKey.FRENCH_FATAL,
            messageFactory.getMessageStringForKey( "ui_header_fatal", Locale.FRENCH ) );
        messageHeaders.put( MapKey.FRENCH_INFO, messageFactory.getMessageStringForKey( "ui_header_info", Locale.FRENCH ) );
        messageHeaders.put( MapKey.FRENCH_WARNING,
            messageFactory.getMessageStringForKey( "ui_header_warning", Locale.FRENCH ) );

    }


    /**
     * Constructor
     */
    private PopUpBean()
    {
        super();
    }


    /**
     * Returns a singleton instance of the PopUpBean. If the instance has not been set it creates a new PopUpBean/
     *
     * @return instance
     */
    public static PopUpBean getInstance()
    {
        if ( instance == null )
        {
            instance = new PopUpBean();
        }
        return instance;
    }


    /**
     * Toggles the message pop-up window to be visible or not.
     *
     */
    public void togglePopup()
    {
        logger.debug( "togglePopup executed" );
        FacesContext facesContext = FacesContext.getCurrentInstance();
        /**
         * Set focus on the pop-up window
         */
        JavascriptContext.applicationFocus( facesContext, "popupForm:popupWindow" );
        /**
         * Grab the pop-up window
         */
        PanelPopup panelPopup = (PanelPopup) facesContext.getViewRoot().findComponent( "popupForm:popupWindow" );
        if ( panelPopup != null )
        {
            render = !render;
        }
        /**
         * fetches the maximum severity
         */
        Severity severity = facesContext.getMaximumSeverity();
        //        int severityLevelError = FacesMessage.SEVERITY_ERROR.getOrdinal();
        //        int severityLevelFatal = FacesMessage.SEVERITY_FATAL.getOrdinal();

        // only show info and warn messages
        //        if ( severity == null
        //            || ( severity.getOrdinal() == severityLevelError || severity.getOrdinal() == severityLevelFatal ) )
        //        {
        //            logger.debug( BaseBean.class, "togglePopup", "inside else" );
        //            render = !render;
        //        }

        /**
         * fetch the locale language, i.e. en/fr
         */
        String locale = facesContext.getViewRoot().getLocale().getLanguage();
        messageHeader = messageHeaders.get( MapKey.valueOf( severity, locale ) );
        imageStyle = imageStyles.get( severity );

        /**
         * Need to clear the messages after the user chooses to close the pop-up window
         */
        if ( !render )
        {
            systemMessages.clear();
            logger.debug( "togglePopup inside !render block" );
        }
        logger.debug( "render is " + render );
    }


    /**
     * Enum class used to fetch localized messages
     */
    private static enum MapKey {
        /**
         * The message is English and has a severity of 'Error'
         */
        ENGLISH_ERROR(FacesMessage.SEVERITY_ERROR, Locale.ENGLISH),

        /**
         * The message is English and has a severity of 'Fatal'
         */
        ENGLISH_FATAL(FacesMessage.SEVERITY_FATAL, Locale.ENGLISH),

        /**
         * The message is English and has a severity of 'Warning'
         */
        ENGLISH_WARNING(FacesMessage.SEVERITY_WARN, Locale.ENGLISH),

        /**
         * The message is English and has a severity of 'Information'
         */
        ENGLISH_INFO(FacesMessage.SEVERITY_INFO, Locale.ENGLISH),

        /**
         * The message is French and has a severity of 'Error'
         */
        FRENCH_ERROR(FacesMessage.SEVERITY_ERROR, Locale.FRENCH),

        /**
         * The message is French and has a severity of 'Fatal'
         */
        FRENCH_FATAL(FacesMessage.SEVERITY_FATAL, Locale.FRENCH),

        /**
         * The message is French and has a severity of 'Warning'
         */
        FRENCH_WARNING(FacesMessage.SEVERITY_WARN, Locale.FRENCH),

        /**
         * The message is French and has a severity of 'Information'
         */
        FRENCH_INFO(FacesMessage.SEVERITY_INFO, Locale.FRENCH);

        /**
         * The Severity of the FacesMessage
         */
        private Severity severity;

        /**
         * The Locale of the FacesMessage
         */
        private Locale locale;


        /**
         * A Map key used for fetching localized messages. 
         * 
         * @param severity - The Severity of the FacesMessage
         * @param locale - The Locale of the FacesMessage
         */
        MapKey( Severity severity, Locale locale )
        {
            this.severity = severity;
            this.locale = locale;
        }


        /**
         * Iterates through the MapKey values and find the MapKey instance that has both the severity and locale
         * passed.
         *
         * @param severity - The Severity of the FacesMessage
         * @param locale - The Locale of the FacesMessage
         * @return the MapKey instance that matches the severity and locale
         */
        public static MapKey valueOf( Severity severity, String locale )
        {
            for ( MapKey mapKey : MapKey.values() )
            {
                if ( mapKey.severity.equals( severity ) && mapKey.locale.getLanguage().equals( locale ) )
                {
                    return mapKey;
                }
            }
            return null;
        }
    }


    /**
     * Custom implementation of the PhaseListener for the subrogation application
     */
    public static class CustomPhaseListener implements PhaseListener
    {

        /**
         * 
         */
        private static final long serialVersionUID = 3687469197916118535L;


        /**
         * Handle a notification that the processing for a process validation phase has just been completed.
         * @param event - a PhaseEvent
         */
        public void afterPhase( PhaseEvent event )
        {
            logger.debug( "afterPhase executed" );
            /**
             * Handles validation events
             */
            logger.debug( "afterPhase event is: " + event.getPhaseId() );
            FacesContext context = FacesContext.getCurrentInstance();
            Iterator<FacesMessage> messages = context.getMessages();
            //Collection<FacesMessage> systemMessages = getInstance().getSystemMessages();

            Collection<FacesMessage> systemMessages = getInstance().getSystemMessages();

            if ( event.getPhaseId().equals( PhaseId.PROCESS_VALIDATIONS ) )
            {
                logger.debug( "afterPhase-- Process Validations" );

                /**
                 * If there are messages to be display call togglePopup
                 */
                for ( ; messages.hasNext(); )
                {
                    systemMessages.add( messages.next() );
                }
                /**
                 * Must ensure not to toggle the pop-up to close if already opened
                 */
                if ( !systemMessages.isEmpty() && ! ( getInstance().getRender() ) )
                {
                    logger.debug( "afterPhase-- has Messages" );
                    getInstance().togglePopup();
                }
            }

            if ( event.getPhaseId().equals( PhaseId.INVOKE_APPLICATION ) )
            {
                logger.debug( "afterPhase -- RENDER_RESPONSE" );

                /**
                 * If there are messages to be display call togglePopup
                 */
                for ( ; messages.hasNext(); )
                {
                    systemMessages.add( messages.next() );
                }
                /**
                 * Must ensure not to toggle the pop-up to close if already opened
                 */
                if ( !systemMessages.isEmpty() && ! ( getInstance().getRender() ) )
                {
                    logger.debug( "afterPhase Has Messages" );
                    getInstance().togglePopup();
                }
            }

            if ( event.getPhaseId().equals( PhaseId.APPLY_REQUEST_VALUES ) )
            {
                logger.debug( "afterPhase -- APPLY_REQUEST_VALUES" );
                if ( !systemMessages.isEmpty() && ! ( messages.hasNext() ) )
                {
                    for ( FacesMessage message : systemMessages )
                    {
                        context.addMessage( null, message );
                    }
                }
                /**
                 * Have to check for additional messages not rendered during the Process Validations
                 */
                else if ( systemMessages.isEmpty() && messages.hasNext() )
                {
                    for ( ; messages.hasNext(); )
                    {
                        systemMessages.add( messages.next() );
                    }
                    /**
                     * Must ensure not to toggle the pop-up to close if already opened
                     */
                    if ( !systemMessages.isEmpty() && ! ( getInstance().getRender() ) )
                    {
                        logger.debug( "afterPhase Has Messages" );
                        getInstance().togglePopup();
                    }
                }
            }

        }


        /**
         * Handle a notification that the processing for a particular phase of the request 
         * processing lifecycle is about to begin.
         * @param event - a PhaseEvent
         */
        public void beforePhase( PhaseEvent event )
        {
            Collection<FacesMessage> systemMessages = getInstance().getSystemMessages();
            logger.debug( "beforePhase event : " + event.getPhaseId() );

        }


        /**
         * Return the identifier of the request processing phase during which this listener is interested in processing 
         * PhaseEvent events. Legal values are the singleton instances defined by the PhaseId class, 
         * including PhaseId.ANY_PHASE to indicate an interest in being notified for all standard phases.
         */
        public PhaseId getPhaseId()
        {
            return PhaseId.ANY_PHASE;
        }
    }


    /**
     * @return Returns the render.
     */
    public Boolean getRender()
    {
        return render;
    }


    /**
     * @param render The render to set.
     */
    public void setRender( Boolean render )
    {
        this.render = render;
    }


    /**
     * @return Returns the systemMessages.
     */
    public Collection<FacesMessage> getSystemMessages()
    {
        return systemMessages;
    }


    /**
     * @param systemMessages The systemMessages to set.
     */
    public void setSystemMessages( Collection<FacesMessage> systemMessages )
    {
        this.systemMessages = systemMessages;
    }


    /**
     * @return Returns the messageHeader.
     */
    public String getMessageHeader()
    {
        return messageHeader;
    }


    /**
     * @param messageHeader The messageHeader to set.
     */
    public void setMessageHeader( String messageHeader )
    {
        this.messageHeader = messageHeader;
    }


    /**
     * @return Returns the imageStyle.
     */
    public String getImageStyle()
    {
        return imageStyle;
    }


    /**
     * @param imageStyle The imageStyle to set.
     */
    public void setImageStyle( String imageStyle )
    {
        this.imageStyle = imageStyle;
    }
}
