
package ca.medavie.subrogation.followup;

import java.util.Calendar;
import java.util.List;

import org.apache.commons.mail.Email;
import org.apache.log4j.Logger;

import ca.medavie.subrogation.SubrogationService;
import ca.medavie.subrogation.common.SpringAuthenticationUtil;
import ca.medavie.subrogation.entities.FollowUp;
import ca.medavie.utilization.RequestResult;

/**
 * Class to handle the daily generation and sending of follow up emails (See JUWA-240).
 * 
 * This class is meant to be managed by Spring, and called daily in the morning.
 * Will find follow ups that are scheduled for Tomorrow's date and send them.
 *
 */
public class FollowUpEmailJob
{
    
    private static final Logger logger = Logger.getLogger( FollowUpEmailJob.class );
    /** The Subgrogation Service */
    private SubrogationService subrogationService;

    /** The emailFactory instance */
    private EmailFactory emailFactory;


    /**
     * Execute the job.
     */
    public void execute()
    {
        
        logger.debug(":Beginning follow up email job execution" );

        //Need to authenticate since we're executing without user login
        SpringAuthenticationUtil.configureAuthentication( "ROLE_SubrogationAdminRole" );

        //Find follow ups scheduled for tomorrow
        Calendar cal = Calendar.getInstance();
        cal.add( Calendar.DATE, 1 );
        RequestResult<List<FollowUp>> result = subrogationService.setAndFindFollowUps( cal.getTime() );

        if ( result == null || !result.isSuccess() || result.getRequestResult() == null )
        {
            logger.error(":Error retreiving Subrogations that require follow up emails" );
        }

        List<FollowUp> followUps = result.getRequestResult();

        //Loop over and sends emails for each followUp
        for ( FollowUp fu : followUps )
        {
            try
            {
                //Generate Email object and send
                Email email = emailFactory.generateFollowUpEmail( fu );
                
                //Send the email
                email.send();

                //If successful, reset state and reschedule for another 3 months in the future.
                cal.setTime( fu.getFollowUpDate() );
                cal.add( Calendar.MONTH, 3 );
                fu.setFollowUpDate( cal.getTime() );
                fu.setState( null );
            }
            catch ( Exception e )
            {
                logger.error(":Exception occurred sending email for subrogation: "
                    + fu.getSubrogation().getId().toString() );
                e.printStackTrace();

                //Set failures to be run tomorrow
                fu.setState( null );
                cal.setTime( fu.getFollowUpDate() );
                cal.add( Calendar.DATE, 1 );
                fu.setFollowUpDate( cal.getTime() );
            }
        }

        //Persist updated followups
        if ( !followUps.isEmpty() )
        {
            subrogationService.setFollowUps( followUps );
        }

        logger.debug(":Finished follow up email job execution" );
    }


    public void setSubrogationService( SubrogationService subrogationService )
    {
        this.subrogationService = subrogationService;
    }


    public void setEmailFactory( EmailFactory emailFactory )
    {
        this.emailFactory = emailFactory;
    }

}
