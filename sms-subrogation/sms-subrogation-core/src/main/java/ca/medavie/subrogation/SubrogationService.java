
package ca.medavie.subrogation;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import ca.medavie.subrogation.entities.FollowUp;
import ca.medavie.subrogation.entities.Subrogation;
import ca.medavie.utilization.RequestResult;
import ca.medavie.utilization.SearchCriteria;
import ca.medavie.utilization.common.DateRange;

/**
 *  Interface for subrogation services for the front end
 */
public interface SubrogationService
{

    /**
     * Find Subrogation by search criteria
     * 
     * @param SearchCriteria
     *            - aSearchCriteria
     * @return RequestResult
     */
    public RequestResult<Collection<Subrogation>> findSubrogationBySearchCriteria( SearchCriteria aSearchCriteria );


    /**
     * Save Update Subrogation
     * 
     * @param Subrogation
     *            - aSubrogation
     * @return RequestResult
     */
    public RequestResult setSubrogation( Subrogation aSubrogation );


    /**
     * remove Subrogation
     * 
     * @param Subrogation
     *            - aSubrogation
     * @return RequestResult
     */
    public RequestResult removeSubrogation( Subrogation aSubrogation );


    /**
     * Find Subscriber First Names
     * 
     * @param String
     *            - First Name
     * @return RequestResult
     */
    public RequestResult findSubscriberFirstNames( String firstName );


    /**
     * Find Subrogation by File Number
     * 
     * @param Long
     *            - file number (primary key)
     * @return RequestResult
     */
    public RequestResult<Subrogation> findSubrogationByFileNumber( Long AFileNumber );


    /**
     * Find Subscriber Last Names
     * 
     * @param String
     *            - Last Name
     * @return RequestResult
     */
    public RequestResult findSubscriberLastNames( String lastName );


    /**
     * Find Subscriber Group IDs
     * 
     * @param String
     *            - group ID
     * @return RequestResult
     */
    public RequestResult findSubscriberGroupIDs( String groupID );


    /**
     * Find Subscriber Sub IDs
     * 
     * @param String
     *            - Sub IDs
     * @return RequestResult
     */
    public RequestResult findSubscriberSubIDs( String subIDs );


    /**
     * Find number of files opened Sub IDs
     * 
     * @param DateRange
     *            - The date range
     * @return RequestResult
     */
    public RequestResult findNumberOfFilesOpened( DateRange dateRange );


    /**
     * Find number of files closed Sub IDs
     * 
     * @param DateRange
     *            - The date range
     * @return RequestResult
     */
    public RequestResult findNumberOfFilesClosed( DateRange dateRange );


    /**
     * Find the sum of recovery amount
     * 
     * @param DateRange
     *            - The date range
     * @return RequestResult
     */
    public RequestResult findSumOfRecoveryAmount( DateRange dateRange );


    /**
     * Find the sum of summary amount
     * 
     * @param DateRange
     *            - The date range
     * @return RequestResult
     */
    public RequestResult findSumOfSummaryAmount( DateRange dateRange );


    /**
     * Find the follow ups for a given date. Uses the 'set' prefix because after retrieving the
     * FollowUps, the FollowUps are updated to have their state = 'Executing'. This ensures that
     * multiple threads don't end up accessing and sending duplicate Follow Up Emails.
     * @param aDate
     * @return RequestResult<List<FollowUp>>
     */
    public RequestResult<List<FollowUp>> setAndFindFollowUps( Date aDate );


    /**
     * Saves or updates a collection of FollowUp objects.
     * 
     * @param aFollowUpList
     * @return RequestResult<Void>
     */
    public RequestResult<Void> setFollowUps( List<FollowUp> aFollowUpList );
}
