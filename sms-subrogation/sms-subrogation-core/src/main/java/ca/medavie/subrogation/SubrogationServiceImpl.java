
package ca.medavie.subrogation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;

import ca.bluecross.atl.utilization.blo.Message;
import ca.medavie.subrogation.entities.FollowUp;
import ca.medavie.subrogation.entities.Subrogation;
import ca.medavie.subrogation.exceptions.SubrogationException;
import ca.medavie.subrogation.repository.FileCountsRepository;
import ca.medavie.subrogation.repository.SubrogationRepository;
import ca.medavie.subrogation.repository.SubscriberSearchRepository;
import ca.medavie.utilization.RequestResult;
import ca.medavie.utilization.SearchCriteria;
import ca.medavie.utilization.common.DateRange;
import ca.medavie.utilization.validation.ValidationStrategy;

/**
 * Class providing subrogation services for the front end.
 */
public class SubrogationServiceImpl implements SubrogationService, BeanFactoryAware
{

    private static final Logger logger = Logger.getLogger( SubrogationServiceImpl.class );
    /**
     * a SubrogationRepository
     * */
    private SubrogationRepository subrogationRepository;

    /**
     * a subscriberSearchRepository
     * */
    private SubscriberSearchRepository subscriberSearchRepository;

    /**
     * a fileCountsRepository
     * */
    private FileCountsRepository fileCountsRepository;

    /** the bean factory */
    private BeanFactory beanFactory;

    /**validation strategy*/
    private ValidationStrategy subrogationValidationStrategy;


    /**
     * Find a collection of Subrogations by search criteria
     * 
     * @param SearchCriteria
     *            - aSearchCriteria
     * @return RequestResult<Collection<Subrogation>> - Subrogations matching the search criteria
     */
    public RequestResult<Collection<Subrogation>> findSubrogationBySearchCriteria( SearchCriteria aSearchCriteria )
    {
        String thisMethod = "findSubrogationBySearchCriteria";

        if ( aSearchCriteria == null )
        {
            logger.error( this.getClass(), new IllegalArgumentException( "The passed SearchCriteria was null." ) );
            throw new IllegalArgumentException( "The passed SearchCriteria was null." );
        }

        RequestResult<Collection<Subrogation>> result = new RequestResult<Collection<Subrogation>>();
        Collection<Subrogation> clients = null;

        try
        {
            clients = subrogationRepository.retrieveSubrogationBySearchCriteria( aSearchCriteria );
        }
        catch ( SubrogationException e )
        {
            logger.error( e.getMessage() );
            result.setSuccess( false );
            return result;
        }

        if ( clients == null || clients.size() == 0 )
        {
            logger.debug( "Subrogation Not Found with search criteria : ." + aSearchCriteria.toString() );
        }
        else
        {
            logger.debug( "There are " + clients.size() + " client(s) found." );
        }
        result.setRequestResult( clients );
        result.setSuccess( true );

        return result;
    }


    /**
     * Save Update Subrogation
     * 
     * @param Subrogation
     *            - aSubrogation
     * @return RequestResult
     */

    public RequestResult setSubrogation( Subrogation aSubrogation )
    {

        RequestResult result = new RequestResult();
        // in oder to keep the object set to the request
        result.setSuccess( true );
        // do business logic validation in the service layer.
        aSubrogation.setValidationStrategy( subrogationValidationStrategy );

        List<Message> messages = new ArrayList<Message>();
        if ( !aSubrogation.isValid( messages ) )
        {
            // add error message. if does not pass the rule.
            result.addMessage( messages.get( 0 ) );
            // set bad object to RequestResult
            result.setRequestResult( aSubrogation );
            //DON'T set false to RequestResult, otherwise it will not able to set the bad subrogation to it.
            // Check ca.medavie.utilization.RequestResult at line 73
            //result.setSuccess( false );
            return result;
        }

        try
        {
            subrogationRepository.saveUpdateSubrogation( aSubrogation );
        }
        catch ( SubrogationException e )
        {
            logger.error( e.getMessage() );
            result.setSuccess( false );
            return result;
        }

        result.setRequestResult( aSubrogation );
        result.setSuccess( true );

        return result;
    }


    /**
     * Remove a Subrogation
     * 
     * @param Subrogation
     *            - aSubrogation
     * @return RequestResult
     */

    public RequestResult removeSubrogation( Subrogation aSubrogation )
    {

        RequestResult result = new RequestResult();

        try
        {
            subrogationRepository.deleteSubrogation( aSubrogation );
        }
        catch ( SubrogationException e )
        {
            logger.error( e.getMessage() );
            result.setSuccess( false );
            return result;
        }

        result.setRequestResult( aSubrogation );
        result.setSuccess( true );

        return result;
    }


    /**
     * Find Subscriber By File Number
     * 
     * @param Long
     *            - File number
     * @return RequestResult
     */
    public RequestResult<Subrogation> findSubrogationByFileNumber( Long aFileNumber )
    {
        String thisMethod = "findSubscriberByFilenumber";
        if ( aFileNumber == null )
        {
            logger.error( ":AFileNumber object passed in were NULL. " );
            throw new IllegalArgumentException( ":AFileNumber object passed in were NULL." );
        }

        RequestResult<Subrogation> result = new RequestResult<Subrogation>();
        Subrogation subrogation = null;

        try
        {
            subrogation = subrogationRepository.retrieveSubrogationByFileNumber( aFileNumber );
        }
        catch ( Exception e )
        {
            logger.error( e.getMessage() );
            result.setSuccess( false );
            return result;
        }

        result.setRequestResult( subrogation );
        result.setSuccess( true );

        logger.debug( "The Subrogation with file number of " + aFileNumber + " was found." );

        return result;
    }


    /**
     * Find Subscriber First Names
     * 
     * @param String
     *            - First Name
     * @return RequestResult
     */
    public RequestResult findSubscriberFirstNames( String firstName )
    {

        String thisMethod = "findSubscriberFirstNames";
        if ( firstName == null )
        {
            logger.error( "firstName object passed in were NULL. " );
            throw new IllegalArgumentException( ":firstName object passed in were NULL." );
        }

        RequestResult result = new RequestResult();
        Collection<String> firstNames = null;

        try
        {
            firstNames = subscriberSearchRepository.retrieveSubscriberFirstNames( firstName );
        }
        catch ( Exception e )
        {
            logger.error( e.getMessage() );
            result.setSuccess( false );
            return result;
        }

        result.setRequestResult( firstNames );
        result.setSuccess( true );

        logger.debug( "There are " + firstNames.size() + " client(s) found." );

        return result;
    }


    /**
     * Find Subscriber Last Names
     * 
     * @param String
     *            - Last Name
     * @return RequestResult
     */
    public RequestResult findSubscriberLastNames( String lastName )
    {
        String thisMethod = "findSubscriberLastNames";
        if ( lastName == null )
        {
            logger.error( ":lastName object passed in were NULL. " );
            throw new IllegalArgumentException( ":lastName object passed in were NULL." );
        }

        RequestResult result = new RequestResult();
        Collection<String> lastNames = null;

        try
        {
            lastNames = subscriberSearchRepository.retrieveSubscriberLastNames( lastName );
        }
        catch ( Exception e )
        {
            logger.error( e.getMessage() );
            result.setSuccess( false );
            return result;
        }

        result.setRequestResult( lastNames );
        result.setSuccess( true );

        logger.debug( "There are " + lastNames.size() + " client(s) found." );

        return result;
    }


    /**
     * Find Subscriber Group IDs
     * 
     * @param String
     *            - group ID
     * @return RequestResult
     */
    public RequestResult findSubscriberGroupIDs( String groupID )
    {
        String thisMethod = "findSubscriberGroupIDs";
        if ( groupID == null )
        {
            logger.error( ":groupID object passed in were NULL. " );
            throw new IllegalArgumentException( ":groupID object passed in were NULL." );
        }

        RequestResult result = new RequestResult();
        Collection<String> groupIDs = null;

        try
        {
            groupIDs = subscriberSearchRepository.retrieveSubscriberGroupIDs( groupID );
        }
        catch ( Exception e )
        {
            logger.error( e.getMessage() );
            result.setSuccess( false );
            return result;
        }

        result.setRequestResult( groupIDs );
        result.setSuccess( true );

        logger.debug( "There are " + groupIDs.size() + " client(s) found." );

        return result;
    }


    /**
     * Find Subscriber Sub IDs
     * 
     * @param String
     *            - Sub IDs
     * @return RequestResult
     */
    public RequestResult findSubscriberSubIDs( String subID )
    {
        String thisMethod = "findSubscriberGroupIDs";
        if ( subID == null )
        {
            logger.error( ":subIDs object passed in were NULL. " );
            throw new IllegalArgumentException( ":subIDs object passed in were NULL." );
        }

        RequestResult result = new RequestResult();
        Collection<String> subIDs = null;

        try
        {
            subIDs = subscriberSearchRepository.retrieveSubscriberSubIDs( subID );
        }
        catch ( Exception e )
        {
            logger.error( e.getMessage() );
            result.setSuccess( false );
            return result;
        }

        result.setRequestResult( subIDs );
        result.setSuccess( true );

        logger.debug( "There are " + subIDs.size() + " client(s) found." );

        return result;
    }


    /**
     * Find the total number of files closed in the certain period of time
     *
     * @param DateRange - dateRange A Certain SearchCriteria
     * @return A Collection of number of files closed 
     */

    public RequestResult findNumberOfFilesClosed( DateRange dateRange )
    {
        String thisMethod = "findNumberOfFilesClosed";
        if ( dateRange == null )
        {
            logger.error( ":dateRange object passed in were NULL. " );
            throw new IllegalArgumentException( ":dateRange object passed in were NULL." );
        }

        RequestResult result = new RequestResult();
        Collection<Integer> numberofFilesClosed = null;

        try
        {
            numberofFilesClosed = fileCountsRepository.retrieveNumberOfFilesClosed( dateRange );
        }
        catch ( Exception e )
        {
            logger.error( e.getMessage() );
            result.setSuccess( false );
            return result;
        }

        result.setRequestResult( numberofFilesClosed );
        result.setSuccess( true );

        //        logger.debug( this.getClass(), "findNumberOfFilesClosed", "the number of closed is[ "
        //            + ( (Integer) ( numberofFilesClosed.toArray()[0] ) ).intValue() + " ]" );

        return result;
    }


    /**
     * Find the total number of files opened in the certain period of time
     *
     * @param DateRange - dateRange A Certain SearchCriteria
     * @return A Collection of number of files opened 
     */
    public RequestResult findNumberOfFilesOpened( DateRange dateRange )
    {
        String thisMethod = "findNumberOfFilesOpened";
        logger.debug( "findNumberOfFilesOpened is executed" );

        if ( dateRange == null )
        {
            logger.error( ":dateRange object passed in were NULL. " );
            throw new IllegalArgumentException( ":dateRange object passed in were NULL." );
        }

        RequestResult result = new RequestResult();
        Collection<Integer> numberofFilesOpened = null;

        try
        {
            numberofFilesOpened = fileCountsRepository.retrieveNumberOfFilesOpened( dateRange );
        }
        catch ( Exception e )
        {
            logger.error( e.getMessage() );
            result.setSuccess( false );
            return result;
        }

        result.setRequestResult( numberofFilesOpened );
        result.setSuccess( true );

        //        logger.debug( this.getClass(), "findNumberOfFilesOpened", "the number of closed is[ "
        //            + ( (Integer) ( numberofFilesOpened.toArray()[0] ) ).intValue() + " ]" );

        return result;
    }


    /**
     * Get the sum of the recovery amount in the certain period of time
     * 
     * @param DateRange - a date range
     * 
     * @return List - A list of result, actually there is only one result
     */
    public RequestResult findSumOfRecoveryAmount( DateRange dateRange )
    {
        String thisMethod = "findSumOfRecoveryAmount";
        if ( dateRange == null )
        {
            logger.error( ":dateRange object passed in were NULL. " );
            throw new IllegalArgumentException( ":dateRange object passed in were NULL." );
        }

        RequestResult result = new RequestResult();
        Collection<BigDecimal> recoveryAmounts = null;

        try
        {
            recoveryAmounts = fileCountsRepository.retrieveRecoveryAmount( dateRange );
        }
        catch ( Exception e )
        {
            logger.error( e.getMessage() );
            result.setSuccess( false );
            return result;
        }

        result.setRequestResult( recoveryAmounts );
        result.setSuccess( true );

        //        logger.debug( this.getClass(), "findSumOfRecoveryAmount", "the sum of recovery amount is[ "
        //            + ( (BigDecimal) ( recoveryAmounts.toArray()[0] ) ).doubleValue() + " ]" );

        return result;
    }


    /**
     * Get the sum of the summary amount in the certain period of time
     * 
     * @param DateRange - a date range
     * 
     * @return List - A list of result, actually there is only one result
     */
    public RequestResult findSumOfSummaryAmount( DateRange dateRange )
    {
        String thisMethod = "findSumOfSummaryAmount";
        if ( dateRange == null )
        {
            logger.error( ":dateRange object passed in were NULL. " );
            throw new IllegalArgumentException( ":dateRange object passed in were NULL." );
        }

        RequestResult result = new RequestResult();
        Collection<BigDecimal> summaryAmounts = null;

        try
        {
            summaryAmounts = fileCountsRepository.retrieveSummaryAmount( dateRange );
        }
        catch ( Exception e )
        {
            logger.error( e.getMessage() );
            result.setSuccess( false );
            return result;
        }

        result.setRequestResult( summaryAmounts );
        result.setSuccess( true );

        //        logger.debug( this.getClass(), "findSumOfSummaryAmount", "the sum of summary amount is[ "
        //            + ( (BigDecimal) ( summaryAmounts.toArray()[0] ) ).doubleValue() + " ]" );

        return result;
    }


    /**
     * @param subscriberSearchRepository the subscriberSearchRepository to set
     */
    public void setSubscriberSearchRepository( SubscriberSearchRepository subscriberSearchRepository )
    {
        this.subscriberSearchRepository = subscriberSearchRepository;
    }


    /**
     * @return the aSubrogationRepository
     */
    public SubrogationRepository getSubrogationRepository()
    {
        return subrogationRepository;
    }


    /**
     * @param subrogationRepository the aSubrogationRepository to set
     */
    public void setSubrogationRepository( SubrogationRepository subrogationRepository )
    {
        this.subrogationRepository = subrogationRepository;
    }


    public BeanFactory getBeanFactory()
    {
        return beanFactory;
    }


    public void setBeanFactory( BeanFactory aBeanFactory ) throws BeansException
    {
        this.beanFactory = aBeanFactory;

    }


    /**
     * @return the subrogationValidationStrategy
     */
    public ValidationStrategy getSubrogationValidationStrategy()
    {
        return subrogationValidationStrategy;
    }


    /**
     * @param subrogationValidationStrategy the subrogationValidationStrategy to set
     */
    public void setSubrogationValidationStrategy( ValidationStrategy subrogationValidationStrategy )
    {
        this.subrogationValidationStrategy = subrogationValidationStrategy;
    }


    /**
     * @return the subscriberSearchRepository
     */
    public SubscriberSearchRepository getSubscriberSearchRepository()
    {
        return subscriberSearchRepository;
    }


    /**
     * @return the fileCountsRepository
     */
    public FileCountsRepository getFileCountsRepository()
    {
        return fileCountsRepository;
    }


    /**
     * @param fileCountsRepository the fileCountsRepository to set
     */
    public void setFileCountsRepository( FileCountsRepository fileCountsRepository )
    {
        this.fileCountsRepository = fileCountsRepository;
    }


    /* (non-Javadoc)
     * @see ca.medavie.subrogation.SubrogationService#setAndFindFollowUps(java.util.Date)
     */
    public RequestResult<List<FollowUp>> setAndFindFollowUps( Date aDate )
    {
        String thisMethod = "setAndFindFollowUps";

        if ( aDate == null )
        {
            logger.error( ":aDate object passed in were NULL. " );
            throw new IllegalArgumentException( ":aDate object passed in were NULL." );
        }

        RequestResult<List<FollowUp>> rr = new RequestResult<List<FollowUp>>();

        try
        {
            rr.setRequestResult( subrogationRepository.retrieveUpdateFollowUps( aDate ) );
            rr.setSuccess( true );
        }
        catch ( Exception e )
        {
            logger.error( e.getMessage() );
            rr.setSuccess( false );
        }

        return rr;
    }


    /* (non-Javadoc)
     * @see ca.medavie.subrogation.SubrogationService#setFollowUps(java.util.list<ca.medavie.subrogation.entities.FollowUp>)
     */
    public RequestResult<Void> setFollowUps( List<FollowUp> aFollowUpList )
    {
        String thisMethod = "setFollowUps";

        if ( aFollowUpList == null )
        {
            logger.error( ":aFollowUpList object passed in were NULL. " );
            throw new IllegalArgumentException( ":aFollowUpList object passed in were NULL." );
        }

        RequestResult<Void> rr = new RequestResult<Void>();

        try
        {
            subrogationRepository.saveFollowUps( aFollowUpList );
            rr.setSuccess( true );
        }
        catch ( Exception e )
        {
            logger.error( e.getMessage() );
            rr.setSuccess( false );
        }

        return rr;
    }

}
