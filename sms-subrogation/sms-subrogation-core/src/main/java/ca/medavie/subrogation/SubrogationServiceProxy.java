
package ca.medavie.subrogation;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

import ca.medavie.subrogation.entities.FollowUp;
import ca.medavie.subrogation.entities.Subrogation;
import ca.medavie.utilization.RequestResult;
import ca.medavie.utilization.SearchCriteria;
import ca.medavie.utilization.common.DateRange;

/**
 * Proxy for the SubrogationService .
 * 
 * The sole purpose of this class is provide an entry point to load the actual
 * service class.
 * 
 */
public class SubrogationServiceProxy implements SubrogationService, BeanFactoryAware
{

    private static final Logger logger = Logger.getLogger( SubrogationServiceProxy.class );
    /** Needs to know it's parent factory - required for datasource alias */
    private BeanFactory parentBeanFactory = null;

    /** the actual organisation service */
    private SubrogationService subrogationService;


    /**
     * Constructor Proxy to the service.
     * 
     */
    public SubrogationServiceProxy()
    {
        // Default constructor
    }


    /**
     * Synchronize access to the service.
     * 
     * @return SubrogationService
     */
    private synchronized SubrogationService getService()
    {
        if ( subrogationService == null )
        {
            init();
        }
        return subrogationService;
    }


    /**
     * Get spring to load the service.
     */
    private void init()
    {
        try
        {
            if ( subrogationService == null )
            {
                // load config files
                ApplicationContext factory = new ClassPathXmlApplicationContext("ca/medavie/subrogation/Sabrogation-Service-context.xml"  );
                // get the service by spring
                subrogationService = (SubrogationService) factory.getBean( "subrogationService" );

            }

            logger.debug( "Subrogation service resources initialized successfully." );
        }
        catch ( Throwable t )
        {
            logger.error( "initSpring", t );
            throw new IllegalStateException( "Spring configuration initialization failed." );
        }

    }


    /**
     * 
     * @see org.springframework.beans.factory.BeanFactoryAware#setBeanFactory(org.springframework.beans.factory.BeanFactory)
     */
    public void setBeanFactory( BeanFactory arg0 ) throws BeansException
    {
        parentBeanFactory = arg0;

    }


    /**
     * Proxy Method for findSubrogationBySearchCriteria in SubrogationService
     * */
    public RequestResult findSubrogationBySearchCriteria( SearchCriteria aSearchCriteria )
    {
        return getService().findSubrogationBySearchCriteria( aSearchCriteria );
    }


    /**
     * Proxy Method for setSubrogation in SubrogationService
     * */
    public RequestResult setSubrogation( Subrogation aSubrogation )
    {

        return getService().setSubrogation( aSubrogation );
    }


    /**
     * Proxy Method for removeSubrogation in SubrogationService
     * */
    public RequestResult removeSubrogation( Subrogation aSubrogation )
    {

        return getService().removeSubrogation( aSubrogation );
    }


    /**
     * Proxy Method for findSubrogationByFileNumber in SubrogationService
     * */
    public RequestResult findSubrogationByFileNumber( Long AFileNumber )
    {
        return getService().findSubrogationByFileNumber( AFileNumber );
    }


    /**
     * Proxy Method for findSubscriberFirstNames in SubrogationService
     * */
    public RequestResult findSubscriberFirstNames( String aFirstName )
    {
        return getService().findSubscriberFirstNames( aFirstName );
    }


    /**
     * Proxy Method for findSubscriberGroupIDs in SubrogationService
     * */
    public RequestResult findSubscriberGroupIDs( String aGroupID )
    {
        return this.getService().findSubscriberGroupIDs( aGroupID );
    }


    /**
     * Proxy Method for findSubscriberLastNames in SubrogationService
     * */
    public RequestResult findSubscriberLastNames( String aLastName )
    {
        return getService().findSubscriberLastNames( aLastName );
    }


    /**
     * Proxy Method for findSubscriberSubIDs in SubrogationService
     * */
    public RequestResult findSubscriberSubIDs( String aSubID )
    {
        return getService().findSubscriberSubIDs( aSubID );
    }


    /**
     * Proxy Method for findNumberOfFilesClosed in SubrogationService
     * */
    public RequestResult findNumberOfFilesClosed( DateRange dateRange )
    {

        return getService().findNumberOfFilesClosed( dateRange );
    }


    /**
     * Proxy Method for findNumberOfFilesOpened in SubrogationService
     * */
    public RequestResult findNumberOfFilesOpened( DateRange dateRange )
    {
        return getService().findNumberOfFilesOpened( dateRange );
    }


    /**
     * Proxy Method for findSumOfRecoveryAmount in SubrogationService
     * */
    public RequestResult findSumOfRecoveryAmount( DateRange dateRange )
    {
        return getService().findSumOfRecoveryAmount( dateRange );
    }


    /**
     * Proxy Method for findSumOfSummaryAmount in SubrogationService
     * */
    public RequestResult findSumOfSummaryAmount( DateRange dateRange )
    {
        return getService().findSumOfSummaryAmount( dateRange );
    }


    /**
     * @return the subrogationService
     */
    public SubrogationService getSubrogationService()
    {
        return subrogationService;
    }


    /**
     * @param subrogationService the subrogationService to set
     */
    public void setSubrogationService( SubrogationService subrogationService )
    {
        this.subrogationService = subrogationService;
    }


    public RequestResult<List<FollowUp>> setAndFindFollowUps( Date aDate )
    {
        return getService().setAndFindFollowUps( aDate );
    }


    public RequestResult<Void> setFollowUps( List<FollowUp> aFollowUpList )
    {
        return getService().setFollowUps( aFollowUpList );
    }

}
