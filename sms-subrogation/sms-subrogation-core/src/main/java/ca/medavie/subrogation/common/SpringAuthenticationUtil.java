
package ca.medavie.subrogation.common;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Class to get authentication for methods that require it without a user logging in.
 *
 */
public class SpringAuthenticationUtil
{
    //Don't want people instantiating this
    private SpringAuthenticationUtil()
    {
    }


    /**
     * Remove our authentication.
     */
    public static void clearAuthentication()
    {
        SecurityContextHolder.getContext().setAuthentication( null );
    }


    /**
     * Get authentication with a Role.
     * @param aRole
     */
    public static void configureAuthentication( String aRole )
    {
        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList( aRole );
        Authentication auth = new UsernamePasswordAuthenticationToken( "user", aRole, authorities );
        SecurityContextHolder.getContext().setAuthentication( auth );
    }
}
