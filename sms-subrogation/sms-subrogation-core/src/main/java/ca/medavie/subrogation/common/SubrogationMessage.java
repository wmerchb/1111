
package ca.medavie.subrogation.common;

import ca.bluecross.atl.utilization.blo.Message;

public class SubrogationMessage extends Message
{

    public SubrogationMessage( String messageCode, String messageText )
    {
        super( messageCode, messageText );
        // TODO Auto-generated constructor stub
    }

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * @param aMessageCode
     * @param aMessageText
     */

    public static final SubrogationMessage messagestub = new SubrogationMessage( "", "" );

}
