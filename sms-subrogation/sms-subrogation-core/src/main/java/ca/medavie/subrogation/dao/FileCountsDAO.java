
package ca.medavie.subrogation.dao;

import java.math.BigDecimal;
import java.util.List;

import ca.medavie.utilization.common.DateRange;

/**
 * Data access interface
 */
public interface FileCountsDAO
{
    /**
     * Get the number of files opened in the certain period of time .
     * 
     * @param DateRange - a date range
     * 
     * @return List - A list of number, actually there is only one result
     */
    public List<Integer> retrieveNumberOfFilesOpened( DateRange dateRange );


    /**
     * Get the number of files closed in the certain period of time .
     * 
     * @param DateRange - a date range
     * 
     * @return List - A list of number, actually there is only one result
     */
    public List<Integer> retrieveNumberOfFilesClosed( DateRange dateRange );


    /**
     * Get the sum of the recovery amount in the certain period of time
     * 
     * @param DateRange - a date range
     * 
     * @return List - A list of result, actually there is only one result
     */
    public List<BigDecimal> retrieveRecoveryAmount( DateRange dateRange );


    /**
     * Get the sum of the summary amount in the certain period of time
     * 
     * @param DateRange - a date range
     * 
     * @return List - A list of result, actually there is only one result
     */
    public List<BigDecimal> retrieveSummaryAmount( DateRange dateRange );

}
