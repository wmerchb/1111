
package ca.medavie.subrogation.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import ca.medavie.subrogation.entities.FollowUp;
import ca.medavie.subrogation.entities.Subrogation;
import ca.medavie.utilization.SearchCriteria;

/**
 * Data access interface
 */
public interface SubrogationDAO
{

    /**
     * Get Subrogations by ID (AKA File number )
     * 
     * @param Long
     *            - A file number (primary key)
     * @return Subrogation - a Subrogation found by this file number
     */
    public Subrogation retrieveSubrogationByFileNumber( Long aFileNumber );


    /**
     * 
     * Set a Subrogation
     * 
     * @param Subrogation
     *            - aSubrogation
     */
    public void saveUpdateSubrogation( Subrogation aSubrogation );


    /**
     * 
     * delete a Subrogation
     * 
     * @param Subrogation
     *            - aSubrogation
     */
    public void deleteSubrogation( Subrogation aSubrogation );


    /**
     * Get Subrogations by search criteria
     * 
     * @param SearchCriteria
     *            - A certain search criteria
     * @return Collection - a collection of Subrogations
     */
    public Collection<Subrogation> retrieveSubrogationBySearchCriteria( final SearchCriteria aSearchCriteria );


    /**
     * Get all FollowUps for a given date.
     * Updates the state of each FollowUp to be 'Executing' to prevent duplicate emails
     * being sent by multiple threads.
     * @param aDate
     * @return List<FollowUp>
     */
    public List<FollowUp> findAndUpdateFollowUps( Date aDate );


    /**
     * Save or update a collection of FollowUp objects. 
     * @param aFollowUpList
     */
    public void saveUpdateFollowUps( List<FollowUp> aFollowUpList );

}
