
package ca.medavie.subrogation.dao;

import java.util.List;

/**
 * Data access interface
 */
public interface SubscriberSearchDAO
{

    /**
     * 
     * Get First Name List
     * @param String - First Name
     * @return List - a list first name specified
     */

    public List<String> retrieveFirstNames( String firstName );


    /**
     * 
     * Get Last Name List
     * @param String - Last Name
     * @return List - a list Last name specified
     */

    public List<String> retrieveLastNames( String lastName );


    /**
     * 
     * Get Group ID List
     * @param String - Group ID
     * @return List - a list Group ID specified
     */

    public List<String> retrieveGroupIDs( String groupID );


    /**
     * 
     * Get Sub ID List
     * @param String - Sub ID
     * @return List - a list Sub ID specified
     */

    public List<String> retrieveSubIDs( String subID );

}
