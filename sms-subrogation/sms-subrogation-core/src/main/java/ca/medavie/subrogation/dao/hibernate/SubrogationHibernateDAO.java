
package ca.medavie.subrogation.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import ca.medavie.subrogation.dao.SubrogationDAO;
import ca.medavie.subrogation.dao.hibernate.searchCriteria.HibernateSubrogationSearchCriteria;
import ca.medavie.subrogation.dao.hibernate.searchCriteria.SubrogationSearchCriteria;
import ca.medavie.subrogation.entities.FollowUp;
import ca.medavie.subrogation.entities.Subrogation;
import ca.medavie.utilization.SearchCriteria;

/**
 * data access class
 */
public class SubrogationHibernateDAO extends HibernateDaoSupport implements SubrogationDAO
{

    private static final Logger logger = Logger.getLogger( SubrogationHibernateDAO.class );
    private static final int maxresult = 250;


    /**
     * 
     * Get a Subrogation by ID (File number).
     * 
     * @param Long
     *            - din
     * @return Subrogation - a Subrogation
     */
    public Subrogation retrieveSubrogationByFileNumber( Long aFileNumber )
    {

        if ( aFileNumber == null )
        {
            logger.error( "ID passed in were null. " );
            throw new IllegalArgumentException( "ID passed in were null" );
        }
        logger.debug( this.getClass().getName() + ".retrieveSubrogationByDin: calling retrieveSubrogationByDin..... " );

        return this.getHibernateTemplate().get( Subrogation.class, aFileNumber );
    }


    /**
     * Get Subrogation by search criteria
     * 
     * @param SearchCriteria
     *            - aSearchCriteria specific search criteria
     * @return Collection - a collection of Subrogations
     */
    @SuppressWarnings("unchecked")
    public Collection<Subrogation> retrieveSubrogationBySearchCriteria( final SearchCriteria aSearchCriteria )

    {
        String thisMethod = "retrieveSubrogationBySeachCriteria";
        logger.debug( this.getClass().getName()
            + ".retrieveSubrogationBySeachCriteria: calling retrieveSubrogationBySeachCriteria..... " );

        if ( aSearchCriteria == null )
        {
            logger.error( ":One or many of the parameters passed in were null. " );
            throw new IllegalArgumentException( "One or many of the parameters passed in were null." );
        }

        if ( ! ( aSearchCriteria instanceof SubrogationSearchCriteria ) )
        {
            logger.error( ":aSearchCriteria is not instance of a SubrogationSearchCriteria. " );
            throw new IllegalArgumentException( "Search criteria must be an instance of a SubrogationSearchCriteria" );
        }

        HibernateTemplate hibernateTemplate = this.getHibernateTemplate();
        hibernateTemplate.setAlwaysUseNewSession( true );

        List<Subrogation> result = hibernateTemplate.executeFind( new HibernateCallback<Object>()
        {
            public Object doInHibernate( Session session ) throws HibernateException
            {

                SecurityContext ctx = SecurityContextHolder.getContext();
                if ( ctx.getAuthentication() == null )
                {
                    logger.debug( "Couldn't get Authentication from SecurityContext." );
                    return null;
                }

                String userId = ctx.getAuthentication().getName().toUpperCase();
                logger.debug( "Authorized Name: " + userId );

                HibernateSubrogationSearchCriteria hibernateSubrogationSearchCriteria = new HibernateSubrogationSearchCriteria(
                    (SubrogationSearchCriteria) aSearchCriteria );

                hibernateSubrogationSearchCriteria.setSession( session );
                Criteria criteria = hibernateSubrogationSearchCriteria.getCriteria();
                criteria.setMaxResults( maxresult );

                List<Subrogation> subrogations = criteria.list();

                return subrogations;
            }
        } );

        logger.debug( ".retrieveSubrogationBySeachCriteria: totally " + result.size() + " found !" );

        return result;
    }


    /**
     * 
     * Set a Subrogation
     * 
     * @param Subrogation
     *            - aSubrogation
     */
    public void saveUpdateSubrogation( Subrogation aSubrogation )
    {

        if ( aSubrogation == null )
        {
            logger.error( "aSubrogation passed in were null. " );
            throw new IllegalArgumentException( "aSubrogation passed in were null." );
        }

        Calendar cal = Calendar.getInstance();
        Calendar tomorrow = Calendar.getInstance();
        tomorrow.add( Calendar.DATE, 1 );
        //Determine new follows ups
        Set<FollowUp> newFollowUps = new HashSet<FollowUp>();
        if ( aSubrogation.getWaiverLetterFollowUp() != null )
        {
            FollowUp ff = new FollowUp();
            cal.setTime( aSubrogation.getWaiverLetterFollowUp() );
            if ( cal.before( tomorrow ) )
            {
                cal.add( Calendar.MONTH, ff.MONTH_DELAY );
            }
            ff.setFollowUpDate( cal.getTime() );
            ff.setType( ff.WAIVER );
            newFollowUps.add( ff );
        }

        if ( aSubrogation.getMoreInfoFollowUp() != null )
        {
            FollowUp ff = new FollowUp();
            ff.setFollowUpDate( aSubrogation.getMoreInfoFollowUp() );
            cal.setTime( aSubrogation.getMoreInfoFollowUp() );
            if ( cal.before( tomorrow ) )
            {
                cal.add( Calendar.MONTH, ff.MONTH_DELAY );
            }
            ff.setFollowUpDate( cal.getTime() );
            ff.setType( ff.MORE_INFO );
            newFollowUps.add( ff );
        }

        if ( aSubrogation.getLawyerLetterFollowUp() != null )
        {
            FollowUp ff = new FollowUp();
            cal.setTime( aSubrogation.getLawyerLetterFollowUp() );
            if ( cal.before( tomorrow ) )
            {
                cal.add( Calendar.MONTH, ff.MONTH_DELAY );
            }
            ff.setFollowUpDate( cal.getTime() );
            ff.setType( ff.LAWYER );
            newFollowUps.add( ff );
        }

        if ( aSubrogation.getInsuranceLetterFollowUp() != null )
        {
            FollowUp ff = new FollowUp();
            cal.setTime( aSubrogation.getInsuranceLetterFollowUp() );
            if ( cal.before( tomorrow ) )
            {
                cal.add( Calendar.MONTH, ff.MONTH_DELAY );
            }
            ff.setFollowUpDate( cal.getTime() );
            ff.setType( ff.INSURANCE );
            newFollowUps.add( ff );
        }

        if ( aSubrogation.getSummaryProvidedFollowUp() != null )
        {
            FollowUp ff = new FollowUp();
            cal.setTime( aSubrogation.getSummaryProvidedFollowUp() );
            if ( cal.before( tomorrow ) )
            {
                cal.add( Calendar.MONTH, ff.MONTH_DELAY );
            }
            ff.setFollowUpDate( cal.getTime() );
            ff.setType( ff.SUMMARY_PROVIDED );
            newFollowUps.add( ff );
        }

        if ( aSubrogation.getId() != null )
        {
            //To avoid orphan child record, fetch original object
            Subrogation originalSubrogation = getHibernateTemplate().load( Subrogation.class, aSubrogation.getId() );

            //Bring new properties into original object
            originalSubrogation.setAccidentDate( aSubrogation.getAccidentDate() );
            originalSubrogation.setBirthDate( aSubrogation.getBirthDate() );
            originalSubrogation.setComment( aSubrogation.getComment() );
            originalSubrogation.setDateFileOpened( aSubrogation.getDateFileOpened() );
            originalSubrogation.setExtraInfo1( aSubrogation.getExtraInfo1() );
            originalSubrogation.setExtraInfo2( aSubrogation.getExtraInfo2() );
            originalSubrogation.setFirstName( aSubrogation.getFirstName() );
            originalSubrogation.setGroupID( aSubrogation.getGroupID() );
            originalSubrogation.setInsuranceLetterFollowUp( aSubrogation.getInsuranceLetterFollowUp() );
            originalSubrogation.setInsuranceLetterSent( aSubrogation.getInsuranceLetterSent() );
            originalSubrogation.setLastName( aSubrogation.getLastName() );
            originalSubrogation.setLawyerFee( aSubrogation.getLawyerFee() );
            originalSubrogation.setLawyerLetterFollowUp( aSubrogation.getLawyerLetterFollowUp() );
            originalSubrogation.setLawyerLetterSent( aSubrogation.getLawyerLetterSent() );
            originalSubrogation.setMoreInfoFollowUp( aSubrogation.getMoreInfoFollowUp() );
            originalSubrogation.setMoreInfoLetterSent( aSubrogation.getMoreInfoLetterSent() );
            originalSubrogation.setNetRecovery( aSubrogation.getNetRecovery() );
            originalSubrogation.setProvince( aSubrogation.getProvince() );
            originalSubrogation.setRecoveryAmount( aSubrogation.getRecoveryAmount() );
            originalSubrogation.setRecoveryDate( aSubrogation.getRecoveryDate() );
            originalSubrogation.setSection( aSubrogation.getSection() );
            originalSubrogation.setSeries( aSubrogation.getSeries() );
            originalSubrogation.setSubID( aSubrogation.getSubID() );
            originalSubrogation.setSummaryAmount( aSubrogation.getSummaryAmount() );
            originalSubrogation.setSummaryDate( aSubrogation.getSummaryDate() );
            originalSubrogation.setSummaryProvidedFollowUp( aSubrogation.getSummaryProvidedFollowUp() );
            originalSubrogation.setSummaryProvidedSent( aSubrogation.getSummaryProvidedSent() );
            originalSubrogation.setValidationStrategy( aSubrogation.getValidationStrategy() );
            originalSubrogation.setWaiverLetterFollowUp( aSubrogation.getWaiverLetterFollowUp() );
            originalSubrogation.setWaiverLetterSent( aSubrogation.getWaiverLetterSent() );

            //Make sure followups is initialized for our Subrogation object
            if ( originalSubrogation.getFollowUps() == null )
            {
                originalSubrogation.setFollowUps( new HashSet<FollowUp>() );
            }

            //Only want follow ups on open files. File is open if recovery date == null or >= today
            if ( aSubrogation.getRecoveryDate() != null )
            {
                SimpleDateFormat dateFormat = new SimpleDateFormat( "dd MMM yyyy" );
                String simpleDateToday = dateFormat.format( new Date() );
                String simpleRecoveryDate = dateFormat.format( aSubrogation.getRecoveryDate() );
                if ( aSubrogation.getRecoveryDate().before( new Date() ) || simpleDateToday.equals( simpleRecoveryDate ) )
                {

                    //Will cause any followups in the db to be removed for this sub (now that it's closed).
                    newFollowUps.clear();
                }

            }

            //Determine which followups need removing and remove them
            Set<FollowUp> removeFollowUps = new HashSet<FollowUp>();
            for ( FollowUp f : originalSubrogation.getFollowUps() )
            {
                if ( !newFollowUps.contains( f ) )
                {
                    removeFollowUps.add( f );
                }
            }

            for ( FollowUp f : removeFollowUps )
            {
                originalSubrogation.getFollowUps().remove( f );
            }

            //Add in the new ones
            for ( FollowUp f : newFollowUps )
            {
                originalSubrogation.getFollowUps().add( f );
            }

            getHibernateTemplate().saveOrUpdate( originalSubrogation );
        }
        else
        {
            if ( aSubrogation.getFollowUps() == null )
            {
                aSubrogation.setFollowUps( new HashSet<FollowUp>() );
            }
            //Add in the new ones
            for ( FollowUp f : newFollowUps )
            {
                aSubrogation.getFollowUps().add( f );
            }
            getHibernateTemplate().saveOrUpdate( aSubrogation );
        }
    }


    /**
     * 
     * delete a Subrogation
     * 
     * @param Subrogation
     *            - aSubrogation
     */
    public void deleteSubrogation( Subrogation aSubrogation )
    {
        String thisMethod = "deleteSubrogation";

        if ( aSubrogation == null )
        {
            logger.error( "aSubrogation passed in were null. " );
            throw new IllegalArgumentException( "aSubrogation passed in were null." );
        }
        getHibernateTemplate().delete( aSubrogation );

    }


    /* (non-Javadoc)
     * @see ca.medavie.subrogation.dao.SubrogationDAO#findAndUpdateFollowUps(java.util.Date)
     */
    public List<FollowUp> findAndUpdateFollowUps( Date aDate )
    {
        Session session = getSession();
        session.enableFilter( "lessEqualToday" ).setParameter( "date", aDate );
        Query q = session.createQuery( "from FollowUp f where f.state is empty or f.state is null" );
        List<FollowUp> followUps = q.list();

        for ( FollowUp p : followUps )
        {
            p.setState( "EXECUTING" );
            session.update( p );
        }
        session.flush();

        return followUps;
    }


    /* (non-Javadoc)
     * @see ca.medavie.subrogation.dao.SubrogationDAO#saveUpdateFollowUps(java.util.list<ca.medavie.subrogation.entities.FollowUp>)
     */
    public void saveUpdateFollowUps( List<FollowUp> aFollowUpList )
    {

        for ( FollowUp p : aFollowUpList )
        {
            getHibernateTemplate().merge( p );
        }
        getHibernateTemplate().flush();
    }

}
