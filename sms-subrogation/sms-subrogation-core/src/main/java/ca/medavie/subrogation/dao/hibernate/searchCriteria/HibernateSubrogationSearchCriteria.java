
package ca.medavie.subrogation.dao.hibernate.searchCriteria;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import ca.medavie.subrogation.entities.Subrogation;
import ca.medavie.utilization.HibernateSearchCriteria;

/**
 *Subrogation search criteria
 * 
 * @author bcswang
 * 
 * */
public class HibernateSubrogationSearchCriteria implements HibernateSearchCriteria
{

	private static final Logger logger = Logger.getLogger( HibernateSubrogationSearchCriteria.class);
    private Session session;
    private final SubrogationSearchCriteria searchCriteria;
    


    /**
     * Constructor
     * 
     * @param aSearchCriteria
     */
    public HibernateSubrogationSearchCriteria( SubrogationSearchCriteria aSearchCriteria )
    {
        this.searchCriteria = aSearchCriteria;
    }


    /**
     * 
     * @see ca.medavie.utilization.HibernateSearchCriteria#getCriteria()
     */
    public Criteria getCriteria()
    {
        if ( session == null )
        {
        	logger.error ( "getCriteria", new IllegalStateException( "Session must be set." ) );
            throw new IllegalStateException( "Session must be set." );
        }

        Criteria subrogationSearchCriteria = session.createCriteria( Subrogation.class ).setResultTransformer(
            Criteria.DISTINCT_ROOT_ENTITY );

        if ( searchCriteria.isGroupIDSet() )
        {
            subrogationSearchCriteria.add( Restrictions.eq( "groupID", searchCriteria.getGroupID() ) );
        }

        if ( searchCriteria.isSubIDSet() )
        {
            subrogationSearchCriteria.add( Restrictions.eq( "subID", searchCriteria.getSubID() ) );
        }

        if ( searchCriteria.isFirstNameSet() )
        {
            subrogationSearchCriteria.add( Restrictions.ilike( "firstName",
                searchCriteria.getFirstName().toLowerCase(), MatchMode.ANYWHERE ) );
        }

        if ( searchCriteria.isLastNameSet() )
        {
            subrogationSearchCriteria.add( Restrictions.ilike( "lastName", searchCriteria.getLastName().toLowerCase(),
                MatchMode.ANYWHERE ) );
        }
        
        if(searchCriteria.isSummaryFromDateSet())
        {
            //Apply 'Greater than or equal to'
            subrogationSearchCriteria.add( Restrictions.ge( "summaryDate", searchCriteria.getSummaryFromDate() ) );
        }
        
        if(searchCriteria.isSummaryToDateSet())
        {
            //Apply 'Less than or equal to'
            subrogationSearchCriteria.add( Restrictions.le( "summaryDate", searchCriteria.getSummaryToDate() ) );
        }
        
        if(searchCriteria.isSummaryAmountSet())
        {
            subrogationSearchCriteria.add( Restrictions.eq( "summaryAmount", searchCriteria.getSummaryAmount() ) );
        }
        
        

        return subrogationSearchCriteria;
    }


    /**
     * 
     * @see ca.medavie.utilization.HibernateSearchCriteria#setSession(org.hibernate.Session)
     */
    public void setSession( Session s )
    {
        session = s;

    }

}
