
package ca.medavie.subrogation.dao.hibernate.searchCriteria;

import java.math.BigDecimal;
import java.util.Date;

import ca.medavie.utilization.SearchCriteria;

/**
 * 
 * The Search Criteria for finding specific subrogation
 */
public class SubrogationSearchCriteria implements SearchCriteria
{
    /** The group ID to search on*/
    private String groupID;

    /** The subscriber ID to search on*/
    private String subID;

    /** The First Name to search on */
    private String firstName;

    /** The Last name to search on */
    private String lastName;

    /** The start of the summary date range to search on */
    private Date summaryFromDate;

    /** The end of the summary date range to search on */
    private Date summaryToDate;

    /** The summary amount to search on */
    private BigDecimal summaryAmount;


    /**
     * Default Constructor for building search criteria
     * 
     */
    public SubrogationSearchCriteria()
    {

    }


    /**
     * Returns true if groupID
     * 
     * @return - true if groupID is set
     */
    public boolean isGroupIDSet()
    {
        if ( groupID != null )
        {
            return true;
        }
        return false;
    }


    /**
     * Returns true if subID is set
     * 
     * @return - true if subID is set
     */
    public boolean isSubIDSet()
    {
        if ( subID != null )
        {
            return true;
        }
        return false;
    }


    /**
     * Returns true if firstName is set
     * 
     * @return - true if firstName is set
     */
    public boolean isFirstNameSet()
    {
        if ( firstName != null )
        {
            return true;
        }
        return false;
    }


    /**
     * Returns true if lastName is set
     * 
     * @return - true if lastName is set
     */
    public boolean isLastNameSet()
    {
        if ( lastName != null )
        {
            return true;
        }
        return false;
    }


    /**
     * Return true if fromDate is set
     * @return boolean
     */
    public boolean isSummaryFromDateSet()
    {
        return summaryFromDate != null;
    }


    /**
     * Return true if to date is set
     * @return boolean
     */
    public boolean isSummaryToDateSet()
    {
        return summaryToDate != null;
    }


    /**
     * Return true if summary amount is set
     * @return boolean
     */
    public boolean isSummaryAmountSet()
    {
        return summaryAmount != null;
    }


    /**
     * @return the groupID
     */
    public String getGroupID()
    {
        return groupID;
    }


    /**
     * @param aGroupID - the groupID to set
     */
    public void setGroupID( String aGroupID )
    {
        this.groupID = aGroupID;
    }


    /**
     * @return the subID
     */
    public String getSubID()
    {
        return subID;
    }


    /**
     * @param aSubID - the subID to set
     */
    public void setSubID( String aSubID )
    {
        this.subID = aSubID;
    }


    /**
     * @return the firstName
     */
    public String getFirstName()
    {
        return firstName;
    }


    /**
     * @param aFirstName - the firstName to set
     */
    public void setFirstName( String aFirstName )
    {
        this.firstName = aFirstName;
    }


    /**
     * @return the lastName
     */
    public String getLastName()
    {
        return lastName;
    }


    /**
     * @param aLastName - the lastName to set
     */
    public void setLastName( String aLastName )
    {
        this.lastName = aLastName;
    }


    /**
     * Get The start of the summary date range to search on
     * @return Date
     */
    public Date getSummaryFromDate()
    {
        return summaryFromDate;
    }


    /**
     * Set The start of the summary date range to search on
     * @param aSummaryFromDate
     */
    public void setSummaryFromDate( Date aSummaryFromDate )
    {
        this.summaryFromDate = aSummaryFromDate;
    }


    /**
     * Get The end of the summary date range to search on
     * @return Date
     */
    public Date getSummaryToDate()
    {
        return summaryToDate;
    }


    /**
     * Set The end of the summary date range to search on
     * @param aSummaryToDate
     */
    public void setSummaryToDate( Date aSummaryToDate )
    {
        this.summaryToDate = aSummaryToDate;
    }


    /**
     * Get The summary amount to search on
     * @return BigDecimal
     */
    public BigDecimal getSummaryAmount()
    {
        return summaryAmount;
    }


    /**
     * Set The summary amount to search on
     * @param aSummaryAmount
     */
    public void setSummaryAmount( BigDecimal aSummaryAmount )
    {
        this.summaryAmount = aSummaryAmount;
    }


    public String toString()
    {
        StringBuffer buffer = new StringBuffer();
        buffer.append( "SubrogationSearchCriteria[" );

        if ( isFirstNameSet() )
        {
            buffer.append( "fistName = " + firstName );
        }

        if ( isLastNameSet() )
        {
            buffer.append( "lastName = " + lastName );
        }

        if ( isSubIDSet() )
        {
            buffer.append( "subID = " + subID );
        }

        if ( isGroupIDSet() )
        {
            buffer.append( "groupID = " + groupID );
        }

        if ( isSummaryFromDateSet() )
        {
            buffer.append( "fromDate" + summaryFromDate );
        }

        if ( isSummaryToDateSet() )
        {
            buffer.append( "toDate" + summaryToDate );
        }

        if ( isSummaryAmountSet() )
        {
            buffer.append( "summaryAmount" + summaryAmount );
        }

        buffer.append( ']' );

        return buffer.toString();
    }

}
