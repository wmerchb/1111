
package ca.medavie.subrogation.dao.jdbc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import ca.medavie.subrogation.dao.FileCountsDAO;
import ca.medavie.utilization.common.DateRange;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * FileCountsJDBCDAO is a class that returns the number of files opened and closed between
 * two given dates, as well as the summary and recovery amount totals for those dates.
 */
public class FileCountsJDBCDAO extends JdbcDaoSupport implements FileCountsDAO
{
    
    private static final Logger logger = Logger.getLogger( FileCountsJDBCDAO.class );
    /** 
     * Query to return the number of file open
     * 
     * As long as there is no recovery date ,means the file is still open
     * 
     * */
    private static String QUERY_NUMBER_OF_FILE_OPEN = "SELECT COUNT(ID) AS FILESOPENED FROM SUBROGATION aa WHERE aa.DATE_FILE_OPENED >=? and aa.DATE_FILE_OPENED<=?";

    /** 
     * Query to return the number of file close
     * As long as there is a file closed date for it, means this file was closed.
     * */
    private static String QUERY_NUMBER_OF_FILE_CLOSED = "SELECT COUNT(ID)AS FILESCLOSED FROM SUBROGATION aa WHERE aa.RECOVERY_DATE >=? and aa.RECOVERY_DATE <=? and aa.RECOVERY_DATE IS NOT NULL";

    /** 
     * Query to return the amount of recovery
     * */
    private static String QUERY_REOVERY_AMOUNT = " SELECT SUM(RECOVERY_AMOUNT)AS TOTAL FROM SUBROGATION aa WHERE aa.RECOVERY_DATE>=? and aa.RECOVERY_DATE<=?";

    /** 
     * Query to return the amount of summary
     * */
    private static String QUERY_SUMMARY_AMOUNT = " SELECT SUM(SUMMARY_AMOUNT)AS TOTAL FROM SUBROGATION aa WHERE aa.SUMMARY_DATE>=? and aa.SUMMARY_DATE<=?";


    /**
     * Get the number of files opened in the certain period of time .
     * @param DateRange - a date range
     * @return List - A list of number, actually there is only one result
     */
    public List<Integer> retrieveNumberOfFilesOpened( DateRange aDateRange )
    {
        SecurityContext ctx = SecurityContextHolder.getContext();
        if ( ctx.getAuthentication() == null )
        {
            logger.debug( "Couldn't get Authentication from SecurityContext." );
            return null;
        }

        String thisMethod = "retrieveNumberOfFilesOpened";
        logger.debug( this.getClass().getName() + ".retrieveNumberOfFilesOpened: calling finding..... " );

        if ( aDateRange == null )
        {
            logger.error( ":dateRange passed in were null. " );
            throw new IllegalArgumentException( "One or many of the parameters passed in were null." );
        }
        Date startDate = aDateRange.getStart();
        Date endDate = aDateRange.getEnd();

        Date queryParmeters[] =
        { startDate, endDate };

        List<Integer> results = new ArrayList<Integer>();

        List<?> rows = null;

        logger.debug( "start query to get rows " );
        rows = getJdbcTemplate().queryForList( QUERY_NUMBER_OF_FILE_OPEN, (Object[]) queryParmeters );
        logger.debug( "rows size is " + rows.size() );

        for ( Object t : rows )
        {
            Map<?, ?> rowMap = (Map<?, ?>) t;
            BigDecimal filesOpened = (BigDecimal) rowMap.get( "FILESOPENED" );
            logger.debug( "filesOpened is " + filesOpened );
            results.add( filesOpened.intValue() );
        }

        return results;

    }


    /**
     * Get the number of files closed in the certain period of time .
     * @param DateRange - a date range
     * @return List - A list of number, actually there is only one result
     */
    public List<Integer> retrieveNumberOfFilesClosed( DateRange aDateRange )
    {

        SecurityContext ctx = SecurityContextHolder.getContext();
        if ( ctx.getAuthentication() == null )
        {
            logger.debug( "Couldn't get Authentication from SecurityContext." );
            return null;
        }

        String thisMethod = "retrieveNumberOfFilesClosed";
        logger.debug( this.getClass().getName() + ".retrieveNumberOfFilesClosed: calling finding..... " );

        if ( aDateRange == null )
        {
            logger.error( ":dateRange passed in were null. " );
            throw new IllegalArgumentException( "One or many of the parameters passed in were null." );
        }
        Date startDate = aDateRange.getStart();
        Date endDate = aDateRange.getEnd();

        Date queryParmeters[] =
        { startDate, endDate };

        List<Integer> results = new ArrayList<Integer>();

        List<?> rows = null;

        logger.debug( "start query to get rows " );
        rows = getJdbcTemplate().queryForList( QUERY_NUMBER_OF_FILE_CLOSED, (Object[]) queryParmeters );
        logger.debug( "rows size is " + rows.size() );

        for ( Object t : rows )
        {
            Map<?, ?> rowMap = (Map<?, ?>) t;
            BigDecimal filesClosed = (BigDecimal) rowMap.get( "FILESCLOSED" );
            logger.debug( "filesClosed is " + filesClosed );
            results.add( filesClosed.intValue() );
        }

        return results;

    }


    /**
     * Get the sum of the recovery amount in the certain period of time
     * @param DateRange - a date range
     * @return List - A list of result, actually there is only one result
     */
    public List<BigDecimal> retrieveRecoveryAmount( DateRange aDateRange )
    {

        SecurityContext ctx = SecurityContextHolder.getContext();
        if ( ctx.getAuthentication() == null )
        {
            logger.debug( "Couldn't get Authentication from SecurityContext." );
            return null;
        }

        String thisMethod = "retrieveRecoveryAmount";
        logger.debug( this.getClass().getName() + ".retrieveRecoveryAmount: calling finding..... " );

        if ( aDateRange == null )
        {
            logger.error( ":dateRange passed in were null. " );
            throw new IllegalArgumentException( "One or many of the parameters passed in were null." );
        }
        Date startDate = aDateRange.getStart();
        Date endDate = aDateRange.getEnd();

        Date queryParmeters[] =
        { startDate, endDate };

        List<BigDecimal> results = new ArrayList<BigDecimal>();

        List<?> rows = null;

        logger.debug( "start query to get rows " );
        rows = getJdbcTemplate().queryForList( QUERY_REOVERY_AMOUNT, (Object[]) queryParmeters );
        logger.debug( "rows size is " + rows.size() );

        for ( Object t : rows )
        {
            Map<?, ?> rowMap = (Map<?, ?>) t;
            BigDecimal recoveryAmount = (BigDecimal) rowMap.get( "TOTAL" );
            logger.debug( "recoveryAmount is " + recoveryAmount );
            results.add( recoveryAmount );
        }

        return results;

    }


    /**
     * Get the sum of the summary amount in the certain period of time
     * @param DateRange - a date range
     * @return List - A list of result, actually there is only one result
     */
    public List<BigDecimal> retrieveSummaryAmount( DateRange aDateRange )
    {

        SecurityContext ctx = SecurityContextHolder.getContext();
        if ( ctx.getAuthentication() == null )
        {
            logger.debug( "Couldn't get Authentication from SecurityContext." );
            return null;
        }

        String thisMethod = "retrieveSummaryAmount";
        logger.debug( this.getClass().getName() + ".retrieveSummaryAmount: calling finding..... " );

        if ( aDateRange == null )
        {
            logger.error( ":dateRange passed in were null. " );
            throw new IllegalArgumentException( "One or many of the parameters passed in were null." );
        }
        Date startDate = aDateRange.getStart();
        Date endDate = aDateRange.getEnd();

        Date queryParmeters[] =
        { startDate, endDate };

        List<BigDecimal> results = new ArrayList<BigDecimal>();

        List<?> rows = null;

        logger.debug( "start query to get rows " );
        rows = getJdbcTemplate().queryForList( QUERY_SUMMARY_AMOUNT, (Object[]) queryParmeters );
        logger.debug( "rows size is " + rows.size() );

        for ( Object t : rows )
        {
            Map<?, ?> rowMap = (Map<?, ?>) t;
            BigDecimal summaryAmount = (BigDecimal) rowMap.get( "TOTAL" );
            logger.debug( "summaryAmount is " + summaryAmount );
            results.add( summaryAmount );
        }

        return results;

    }

}
