
package ca.medavie.subrogation.dao.jdbc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import ca.medavie.subrogation.dao.SubscriberSearchDAO;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * TODO Fill in this class's Javadoc comment.
 */
public class SearchSubscriberJDBCDAO extends JdbcDaoSupport implements SubscriberSearchDAO
{

    private static final Logger logger = Logger.getLogger( SearchSubscriberJDBCDAO.class );
    /** 
     * Query to return unique Last Name List
     * */
    private static String UNIQUE_LAST_NAME_QUERY = " SELECT DISTINCT aa.LAST_NAME FROM SUBROGATION aa WHERE UPPER(aa.LAST_NAME) like ?  order by aa.LAST_NAME";

    /** 
     * Query to return any unique Last Name List
     * */
    private static String UNIQUE_ANY_LAST_NAME_QUERY = " SELECT DISTINCT aa.LAST_NAME FROM SUBROGATION aa order by aa.LAST_NAME asc";

    /** 
     * Query to return unique First Name List
     * */
    private static String UNIQUE_FIRST_NAME_QUERY = " SELECT DISTINCT aa.FIRST_NAME FROM SUBROGATION aa WHERE UPPER(aa.FIRST_NAME) like ?  order by aa.FIRST_NAME";

    /** 
     * Query to return any unique First Name List
     * */
    private static String UNIQUE_ANY_FIRST_NAME_QUERY = " SELECT DISTINCT aa.FIRST_NAME FROM SUBROGATION aa ORDER BY aa.FIRST_NAME asc";

    /** 
     * Query to return unique Group id List
     * */
    private static String UNIQUE_GRP_QUERY = " SELECT DISTINCT aa.GRP FROM SUBROGATION aa WHERE aa.GRP like ?  order by aa.GRP";

    /** 
     * Query to return any unique Group id List
     * */
    private static String UNIQUE_ANY_GRP_QUERY = " SELECT DISTINCT aa.GRP FROM SUBROGATION aa order by aa.GRP asc";

    /** 
     * Query to return unique sub_id List
     * */
    private static String UNIQUE_SUB_ID_QUERY = " SELECT DISTINCT aa.SUB_ID FROM SUBROGATION aa WHERE aa.SUB_ID like ?  order by aa.SUB_ID";

    /** 
     * Query to return any unique sub_id List
     * */
    private static String UNIQUE_ANY_SUB_ID_QUERY = " SELECT DISTINCT aa.SUB_ID FROM SUBROGATION aa order by aa.SUB_ID asc";

    /** 
     * Max search results
     * */
    private static int MAXRESULT = 50;


    /**
     * Get first names .
     * Find 10 first names at most from subrogation table.
     * @param String - a searching first name
     * @return List - A list of first names
     */
    public List<String> retrieveFirstNames( String firstNames )
    {

        SecurityContext ctx = SecurityContextHolder.getContext();
        if ( ctx.getAuthentication() == null )
        {
            logger.debug( "Couldn't get Authentication from SecurityContext." );
            return null;
        }
        
        logger.debug( this.getClass().getName() + ".retrieveFirstNames: calling finding..... " );

        if ( firstNames == null )
        {
            logger.error( ":firstNames passed in were null. " );
            throw new IllegalArgumentException( "One or many of the parameters passed in were null." );
        }
        String upperCase = firstNames.toUpperCase();

        String queryParmeters1[] =
        { upperCase + "%" };
        String queryParmeters2[] =
        { "%" + upperCase + "%" };

        List<String> results = new ArrayList<String>();

        List<?> rows1 = null;
        List<?> rows2 = null;
        List<?> rows3 = null;

        this.getJdbcTemplate().setMaxRows( MAXRESULT );
        rows1 = getJdbcTemplate().queryForList( UNIQUE_FIRST_NAME_QUERY, (Object[]) queryParmeters1 );
        rows2 = getJdbcTemplate().queryForList( UNIQUE_FIRST_NAME_QUERY, (Object[]) queryParmeters2 );

        if ( rows1.size() > 0 || rows2.size() > 0 )
        {
            if ( rows1.size() > 0 )
            {

                Iterator<?> rIter = rows1.iterator();
                while ( rIter.hasNext() )
                {
                    Map<?, ?> rowMap = (Map<?, ?>) rIter.next();
                    String firstName = (String) rowMap.get( "FIRST_NAME" );
                    results.add( firstName );
                }

                return results;
            }

            if ( rows2.size() > 0 )
            {

                Iterator<?> rIter = rows2.iterator();
                while ( rIter.hasNext() )
                {
                    Map<?, ?> rowMap = (Map<?, ?>) rIter.next();
                    String firstName = (String) rowMap.get( "FIRST_NAME" );
                    results.add( firstName );
                }

                return results;
            }

        }

        rows3 = getJdbcTemplate().queryForList( UNIQUE_ANY_FIRST_NAME_QUERY );
        Iterator<?> rIter = rows3.iterator();
        while ( rIter.hasNext() )
        {
            Map<?, ?> rowMap = (Map<?, ?>) rIter.next();
            String firstName = (String) rowMap.get( "FIRST_NAME" );
            results.add( firstName );
        }

        return results;

    }


    /**
     * 
     * Get Last Name List
     * @param String - Last Name
     * @return List - a list Last name specified
     */
    public List<String> retrieveLastNames( String lastNames )
    {

        SecurityContext ctx = SecurityContextHolder.getContext();
        if ( ctx.getAuthentication() == null )
        {
            logger.debug( "Couldn't get Authentication from SecurityContext." );
            return null;
        }

        logger.debug( this.getClass().getName() + ".retrieveLastNames: calling finding..... " );

        if ( lastNames == null )
        {
            logger.error( ":lastNames passed in were null. " );
            throw new IllegalArgumentException( "One or many of the parameters passed in were null." );
        }
        String upperCase = lastNames.toUpperCase();

        String queryParmeters1[] =
        { upperCase + "%" };
        String queryParmeters2[] =
        { "%" + upperCase + "%" };

        List<String> results = new ArrayList<String>();

        List<?> rows1 = null;
        List<?> rows2 = null;
        List<?> rows3 = null;

        this.getJdbcTemplate().setMaxRows( MAXRESULT );
        rows1 = getJdbcTemplate().queryForList( UNIQUE_LAST_NAME_QUERY, (Object[]) queryParmeters1 );
        rows2 = getJdbcTemplate().queryForList( UNIQUE_LAST_NAME_QUERY, (Object[]) queryParmeters2 );

        if ( rows1.size() > 0 || rows2.size() > 0 )
        {
            if ( rows1.size() > 0 )
            {

                Iterator<?> rIter = rows1.iterator();
                while ( rIter.hasNext() )
                {
                    Map<?, ?> rowMap = (Map<?, ?>) rIter.next();
                    String lastName = (String) rowMap.get( "LAST_NAME" );
                    results.add( lastName );
                }

                return results;
            }

            if ( rows2.size() > 0 )
            {

                Iterator<?> rIter = rows2.iterator();
                while ( rIter.hasNext() )
                {
                    Map<?, ?> rowMap = (Map<?, ?>) rIter.next();
                    String lastName = (String) rowMap.get( "LAST_NAME" );
                    results.add( lastName );
                }

                return results;
            }

        }

        rows3 = getJdbcTemplate().queryForList( UNIQUE_ANY_LAST_NAME_QUERY );
        Iterator<?> rIter = rows3.iterator();
        while ( rIter.hasNext() )
        {
            Map<?, ?> rowMap = (Map<?, ?>) rIter.next();
            String lastName = (String) rowMap.get( "LAST_NAME" );
            results.add( lastName );
        }

        return results;

    }


    /**
     * 
     * Get Group ID List
     * @param String - Group ID
     * @return List - a list Group ID specified
     */
    public List<String> retrieveGroupIDs( String groupID )
    {

        SecurityContext ctx = SecurityContextHolder.getContext();
        if ( ctx.getAuthentication() == null )
        {
            logger.debug( "Couldn't get Authentication from SecurityContext." );
            return null;
        }

        logger.debug( this.getClass().getName() + ".retrieveGroupIDs: calling finding..... " );

        if ( groupID == null )
        {
            logger.error( ":groupID passed in were null. " );
            throw new IllegalArgumentException( "One or many of the parameters passed in were null." );
        }
        String queryParmeters[] =
        { "%" + groupID + "%" };
        List<String> results = new ArrayList<String>();

        List<?> rows = null;
        this.getJdbcTemplate().setMaxRows( MAXRESULT );
        rows = getJdbcTemplate().queryForList( UNIQUE_GRP_QUERY, (Object[]) queryParmeters );

        if ( rows.size() > 0 )
        {

            Iterator<?> rIter = rows.iterator();
            while ( rIter.hasNext() )
            {
                Map<?, ?> rowMap = (Map<?, ?>) rIter.next();
                String groupIDs = (String) rowMap.get( "GRP" );
                results.add( groupIDs );
            }

            return results;

        }

        else
        {
            rows = getJdbcTemplate().queryForList( UNIQUE_ANY_GRP_QUERY );
            Iterator<?> rIter = rows.iterator();
            while ( rIter.hasNext() )
            {
                Map<?, ?> rowMap = (Map<?, ?>) rIter.next();
                String groupIDs = (String) rowMap.get( "GRP" );
                results.add( groupIDs );
            }

            return results;

        }

    }


    /**
     * 
     * Get Sub ID List
     * @param String - Sub ID
     * @return List - a list Sub ID specified
     */
    public List<String> retrieveSubIDs( String subID )
    {
        SecurityContext ctx = SecurityContextHolder.getContext();
        if ( ctx.getAuthentication() == null )
        {
            logger.debug( "Couldn't get Authentication from SecurityContext." );
            return null;
        }
        
        logger.debug( this.getClass().getName() + ".retrieveSubIDs: calling finding..... " );

        if ( subID == null )
        {
            logger.error( ":subID passed in were null. " );
            throw new IllegalArgumentException( "One or many of the parameters passed in were null." );
        }
        String queryParmeters[] =
        { "%" + subID + "%" };
        List<String> results = new ArrayList<String>();

        List<?> rows = null;
        this.getJdbcTemplate().setMaxRows( MAXRESULT );
        rows = getJdbcTemplate().queryForList( UNIQUE_SUB_ID_QUERY, (Object[]) queryParmeters );

        if ( rows.size() > 0 )
        {

            Iterator<?> rIter = rows.iterator();
            while ( rIter.hasNext() )
            {
                Map<?, ?> rowMap = (Map<?, ?>) rIter.next();
                String subId = (String) rowMap.get( "SUB_ID" );
                results.add( subId );
            }

            return results;
        }

        else
        {
            rows = getJdbcTemplate().queryForList( UNIQUE_ANY_SUB_ID_QUERY );
            Iterator<?> rIter = rows.iterator();
            while ( rIter.hasNext() )
            {
                Map<?, ?> rowMap = (Map<?, ?>) rIter.next();
                String subId = (String) rowMap.get( "SUB_ID" );
                results.add( subId );
            }

            return results;
        }

    }

}
