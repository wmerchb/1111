
package ca.medavie.subrogation.entities;

import java.util.Date;

/**
 * Class to represent a Follow Up. These objects schedule when Follow Up Emails will be
 * sent to users that require them. Their types map to the follow up dates found in the Subrogation App.
 *
 */
public class FollowUp
{
    /** Types of follow ups */
    public static final String WAIVER = "Waiver Letter";
    public static final String MORE_INFO = "More Info Letter";
    public static final String LAWYER = "Lawyer Letter";
    public static final String INSURANCE = "Insurance Letter";
    public static final String SUMMARY_PROVIDED = "Summary Provided";

    /** The amount of time between follow up reminders */
    public static final int MONTH_DELAY = 3;

    /** ID used in the database */
    private long id;

    /** Date to send follow up email on */
    private Date followUpDate;

    /** The follow up type of this instance */
    private String type;

    /** The state of the follow up during email generation */
    private String state;

    /** The subrogation associated with this follow up */
    private Subrogation subrogation;


    /**
     * Default constructor
     */
    public FollowUp()
    {

    }


    public Date getFollowUpDate()
    {
        return followUpDate;
    }


    public void setFollowUpDate( Date followUpDate )
    {
        this.followUpDate = followUpDate;
    }


    public String getType()
    {
        return type;
    }


    public void setType( String type )
    {
        this.type = type;
    }


    public String getState()
    {
        return state;
    }


    public void setState( String state )
    {
        this.state = state;
    }


    public long getId()
    {
        return id;
    }


    public void setId( long id )
    {
        this.id = id;
    }


    public Subrogation getSubrogation()
    {
        return subrogation;
    }


    public void setSubrogation( Subrogation subrogation )
    {
        this.subrogation = subrogation;
    }


    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( type == null ) ? 0 : type.hashCode() );
        return result;
    }


    /**
     * Compare based on type only. Used for set operations.
     */
    @Override
    public boolean equals( Object obj )
    {
        if ( this == obj )
        {
            return true;
        }
        if ( obj == null )
        {
            return false;
        }
        if ( getClass() != obj.getClass() )
        {
            return false;
        }
        FollowUp other = (FollowUp) obj;

        if ( type == null )
        {
            if ( other.type != null )
                return false;
        }
        else if ( !type.equals( other.type ) )
        {
            return false;
        }
        return true;
    }


    @Override
    public String toString()
    {
        return "FollowUp [id=" + id + ", followUpDate=" + followUpDate + ", type=" + type + ", state=" + state
            + ", subrogation=" + subrogation + "]";
    }

}
