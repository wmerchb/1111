
package ca.medavie.subrogation.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ca.medavie.utilization.validation.Validatable;
import ca.medavie.utilization.validation.ValidationStrategy;

public class Subrogation implements Validatable, Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 4545104337196261206L;

    private Long id;

    private Date dateFileOpened;

    private String groupID;

    private String section;

    private String series;

    private String province;

    private String extraInfo2;

    private String subID;

    private String firstName;

    private String lastName;

    private String extraInfo1;

    private Date accidentDate;

    private Date summaryDate;

    private Date birthDate;

    private BigDecimal summaryAmount;

    private Date recoveryDate;

    private BigDecimal recoveryAmount;

    private BigDecimal lawyerFee;

    private BigDecimal netRecovery;

    private Date waiverLetterSent;

    private Date lawyerLetterSent;

    private Date moreInfoLetterSent;

    private Date waiverLetterFollowUp;

    private Date lawyerLetterFollowUp;

    private Date moreInfoFollowUp;

    private Date insuranceLetterSent;

    private Date insuranceLetterFollowUp;

    private Date summaryProvidedSent;

    private Date summaryProvidedFollowUp;

    private String comment;

    private int hashCode;

    /** List of FollowUps associated with this Subrogation */
    private Set<FollowUp> followUps = new HashSet<FollowUp>();

    /** Strategy for validating */
    private ValidationStrategy validationStrategy;


    public Long getId()
    {
        return id;
    }


    public void setId( Long id )
    {
        this.id = id;
    }


    public Date getDateFileOpened()
    {
        return dateFileOpened;
    }


    public void setDateFileOpened( Date dateFileOpened )
    {
        this.dateFileOpened = dateFileOpened;
    }


    public String getGroupID()
    {
        return groupID;
    }


    public void setGroupID( String groupID )
    {
        this.groupID = groupID;
    }


    public String getSection()
    {
        return section;
    }


    public void setSection( String section )
    {
        this.section = section;
    }


    public String getSeries()
    {
        return series;
    }


    public void setSeries( String series )
    {
        this.series = series;
    }


    public String getProvince()
    {
        return province;
    }


    public void setProvince( String province )
    {
        this.province = province;
    }


    public String getExtraInfo2()
    {
        return extraInfo2;
    }


    public void setExtraInfo2( String extraInfo2 )
    {
        this.extraInfo2 = extraInfo2;
    }


    public String getSubID()
    {
        return subID;
    }


    public void setSubID( String subID )
    {
        this.subID = subID;
    }


    public String getFirstName()
    {
        return firstName;
    }


    public void setFirstName( String firstName )
    {
        this.firstName = firstName;
    }


    public String getLastName()
    {
        return lastName;
    }


    public void setLastName( String lastName )
    {
        this.lastName = lastName;
    }


    public String getExtraInfo1()
    {
        return extraInfo1;
    }


    public void setExtraInfo1( String extraInfo1 )
    {
        this.extraInfo1 = extraInfo1;
    }


    public Date getAccidentDate()
    {
        return accidentDate;
    }


    public void setAccidentDate( Date accidentDate )
    {
        this.accidentDate = accidentDate;
    }


    public Date getSummaryDate()
    {
        return summaryDate;
    }


    public void setSummaryDate( Date summaryDate )
    {
        this.summaryDate = summaryDate;
    }


    public Date getBirthDate()
    {
        return birthDate;
    }


    public void setBirthDate( Date birthDate )
    {
        this.birthDate = birthDate;
    }


    public BigDecimal getSummaryAmount()
    {
        return summaryAmount;
    }


    public void setSummaryAmount( BigDecimal summaryAmount )
    {
        this.summaryAmount = summaryAmount;
    }


    public Date getRecoveryDate()
    {
        return recoveryDate;
    }


    public void setRecoveryDate( Date recoveryDate )
    {
        this.recoveryDate = recoveryDate;
    }


    public BigDecimal getRecoveryAmount()
    {
        return recoveryAmount;
    }


    public void setRecoveryAmount( BigDecimal recoveryAmount )
    {
        this.recoveryAmount = recoveryAmount;
    }


    public BigDecimal getLawyerFee()
    {
        return lawyerFee;
    }


    public void setLawyerFee( BigDecimal lawyerFee )
    {
        this.lawyerFee = lawyerFee;
    }


    public BigDecimal getNetRecovery()
    {
        return netRecovery;
    }


    public void setNetRecovery( BigDecimal netRecovery )
    {
        this.netRecovery = netRecovery;
    }


    public Date getWaiverLetterSent()
    {
        return waiverLetterSent;
    }


    public void setWaiverLetterSent( Date waiverLetterSent )
    {
        this.waiverLetterSent = waiverLetterSent;
    }


    public Date getLawyerLetterSent()
    {
        return lawyerLetterSent;
    }


    public void setLawyerLetterSent( Date lawyerLetterSent )
    {
        this.lawyerLetterSent = lawyerLetterSent;
    }


    public Date getMoreInfoLetterSent()
    {
        return moreInfoLetterSent;
    }


    public void setMoreInfoLetterSent( Date moreInfoLetterSent )
    {
        this.moreInfoLetterSent = moreInfoLetterSent;
    }


    public Date getWaiverLetterFollowUp()
    {
        return waiverLetterFollowUp;
    }


    public void setWaiverLetterFollowUp( Date waiverLetterFollowUp )
    {
        this.waiverLetterFollowUp = waiverLetterFollowUp;
    }


    public Date getLawyerLetterFollowUp()
    {
        return lawyerLetterFollowUp;
    }


    public void setLawyerLetterFollowUp( Date lawyerLetterFollowUp )
    {
        this.lawyerLetterFollowUp = lawyerLetterFollowUp;
    }


    public Date getMoreInfoFollowUp()
    {
        return moreInfoFollowUp;
    }


    public void setMoreInfoFollowUp( Date moreInfoFollowUp )
    {
        this.moreInfoFollowUp = moreInfoFollowUp;
    }


    public Date getInsuranceLetterSent()
    {
        return insuranceLetterSent;
    }


    public void setInsuranceLetterSent( Date insuranceLetterSent )
    {
        this.insuranceLetterSent = insuranceLetterSent;
    }


    public Date getInsuranceLetterFollowUp()
    {
        return insuranceLetterFollowUp;
    }


    public void setInsuranceLetterFollowUp( Date insuranceLetterFollowUp )
    {
        this.insuranceLetterFollowUp = insuranceLetterFollowUp;
    }


    public Date getSummaryProvidedSent()
    {
        return summaryProvidedSent;
    }


    public void setSummaryProvidedSent( Date summaryProvidedSent )
    {
        this.summaryProvidedSent = summaryProvidedSent;
    }


    public Date getSummaryProvidedFollowUp()
    {
        return summaryProvidedFollowUp;
    }


    public void setSummaryProvidedFollowUp( Date summaryProvidedFollowUp )
    {
        this.summaryProvidedFollowUp = summaryProvidedFollowUp;
    }


    public String getComment()
    {
        return comment;
    }


    public void setComment( String comment )
    {
        this.comment = comment;
    }


    public boolean isValid( List messages )
    {
        if ( validationStrategy == null )
        {
            throw new IllegalStateException(
                "subrogation's validation strategy must be set before calling isValid method." );
        }

        // Validate the subrogation
        if ( !validationStrategy.validate( this, messages ) )
        {
            return false;
        }

        return true;
    }


    public ValidationStrategy getValidationStrategy()
    {
        return validationStrategy;
    }


    public void setValidationStrategy( ValidationStrategy validationStrategy )
    {
        this.validationStrategy = validationStrategy;

    }


    public String toString()
    {
        StringBuffer buffer = new StringBuffer();
        buffer.append( "Subrogation[" );
        buffer.append( "ID: " ).append( id );
        buffer.append( ", dateFileOpened: " ).append( dateFileOpened );
        buffer.append( ", groupID: " ).append( groupID );
        buffer.append( ", section: " ).append( section );
        buffer.append( ", series: " ).append( series );
        buffer.append( ", province: " ).append( province );
        buffer.append( ", extraInfo2: " ).append( extraInfo2 );

        buffer.append( ", subID: " ).append( subID );
        buffer.append( ", firstName: " ).append( firstName );
        buffer.append( ", lastName: " ).append( lastName );
        buffer.append( ", birthDate: " ).append( birthDate );
        buffer.append( ", extraInfo1: " ).append( extraInfo1 );
        buffer.append( ", accidentDate: " ).append( accidentDate );
        buffer.append( ", summaryDate: " ).append( summaryDate );
        buffer.append( ", summaryAmount: " ).append( summaryAmount );

        buffer.append( ", recoveryDate: " ).append( recoveryDate );
        buffer.append( ", recoveryAmount: " ).append( recoveryAmount );
        buffer.append( ", lawyerFee: " ).append( lawyerFee );
        buffer.append( ", netRecovery: " ).append( netRecovery );
        buffer.append( ", waiverLetterSent: " ).append( waiverLetterSent );
        buffer.append( ", lawyerLetterSent: " ).append( lawyerLetterSent );
        buffer.append( ", moreInfoLetterSent: " ).append( moreInfoLetterSent );

        buffer.append( ", waiverLetterFollowUp: " ).append( waiverLetterFollowUp );
        buffer.append( ", lawyerLetterFollowUp: " ).append( lawyerLetterFollowUp );
        buffer.append( ", moreInfoFollowUp: " ).append( moreInfoFollowUp );
        buffer.append( ", insuranceLetterSent: " ).append( insuranceLetterSent );
        buffer.append( ", insuranceLetterFollowUp: " ).append( insuranceLetterFollowUp );
        buffer.append( ", summaryProvidedSent: " ).append( summaryProvidedSent );
        buffer.append( ", summaryProvidedFollowUp: " ).append( summaryProvidedFollowUp );
        buffer.append( ", comment: " ).append( comment );

        buffer.append( ']' );
        return buffer.toString();
    }


    public int hashCode()
    {

        if ( hashCode == 0 )
        {
            int result = 17;
            result = 37 * result + id.intValue();

            hashCode = result;
        }
        return hashCode;
    }


    public Set<FollowUp> getFollowUps()
    {
        return followUps;
    }


    public void setFollowUps( Set<FollowUp> followUps )
    {
        this.followUps = followUps;
    }

}
