
package ca.medavie.subrogation.exceptions;

public class SubrogationException extends Exception
{
    private static final long serialVersionUID = 1L;

    public static final String DELETE_SUBROGATION_FALIED = "Delete subrogation Product failed.";


    /**
     * Public constructor which takes a string message to be included in the
     * exception message.
     * @param aMessage - text message
     */
    public SubrogationException( String aMessage )
    {
        super( aMessage );
    }


    /**
     * Public constructor which takes a string message to be included in the
     * exception message and a instance of Throwable. Used when chaining Exceptions.
     * @param aMessage - text message
     * @param aThrowable - a Throwable execpetion
     */
    public SubrogationException( String aMessage, Throwable aThrowable )
    {
        super( aMessage, aThrowable );
    }
}
