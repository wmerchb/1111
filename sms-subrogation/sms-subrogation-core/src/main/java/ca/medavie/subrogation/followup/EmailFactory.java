
package ca.medavie.subrogation.followup;

import java.util.regex.Pattern;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import org.apache.log4j.Logger;

import ca.medavie.subrogation.entities.FollowUp;
import ca.medavie.subrogation.entities.Subrogation;
import ca.medavie.subrogation.exceptions.SubrogationException;

/**
 * Class responsible for generating Follow Up emails based on a message template.
 * 
 * Currently supports one hard-coded English message: there is no requirement for bilingual emails, 
 * but this class can be updated to facilitate the change if need be.
 *
 */
public class EmailFactory
{
    
    private static final Logger logger = Logger.getLogger( EmailFactory.class );
    /** Email Subject template */
    private static final String SUBJECT_ROOT = "Follow up with letter for ";

    /** Email content template */
    private static final String MESSAGE_TEMPLATE = "Follow up required "
        + "for {FIRST NAME} {LAST NAME} {Group Number} AND {ID NUMBER}.";

    /** The email recipient */
    private String receiver;

    /** The email sender */
    private String sender;

    /** The email host name */
    private String hostName;


    /**
     * Generates an Email object, formatted for a specific FollowUp, based
     * on the Message Template defined above.
     * @param aFollowUp
     * @return Email
     * @throws SubrogationException 
     */
    public Email generateFollowUpEmail( FollowUp aFollowUp ) throws SubrogationException
    {

        String thisMethod = "generateFollowUpEmail";
        if ( aFollowUp == null )
        {
            logger.error( ":aFollowUp passed in were NULL. " );
            throw new IllegalArgumentException( "aFollowUp passed in were NULL. " );
        }

        Email email = new SimpleEmail();
        try
        {
            email.setHostName( hostName );
            email.setSSLOnConnect( false );
            email.setFrom( sender );
            email.setSubject( SUBJECT_ROOT + aFollowUp.getType() );
            email.setMsg( formatMessage( aFollowUp.getSubrogation() ) );
            email.addTo( receiver );
        }
        catch ( Exception e )
        {
            logger.error( ":Exception while creating Follow Up Email. " );
            e.printStackTrace();
            throw new SubrogationException( this.getClass() + thisMethod, e );
        }

        return email;
    }


    /**
     * Formats the message template to replace placeholder values with either
     * their real value or a message indicating that the value isn't provided.
     * @param aSubrogation
     * @return String
     */
    private String formatMessage( Subrogation aSubrogation )
    {
        String formattedMessage = MESSAGE_TEMPLATE;
        Pattern regex = Pattern.compile( "\\{([^}]*)\\}" );
        String regString = regex.toString();

        // Ensure that we account for null values.
        //Substritute strings in {} to their real values
        formattedMessage = formattedMessage.replaceFirst( regString,
            nullCheck( aSubrogation.getFirstName(), "First Name" ) );
        formattedMessage = formattedMessage.replaceFirst( regString,
            nullCheck( aSubrogation.getLastName(), "Last Name" ) );
        formattedMessage = formattedMessage.replaceFirst( regString, nullCheck( aSubrogation.getGroupID(), "Group ID" ) );
        formattedMessage = formattedMessage.replaceFirst( regString,
            nullCheck( aSubrogation.getSubID(), "Subrogation ID" ) );

        return formattedMessage;
    }


    /**
     * Checks for null or empty string values. Returns either the value (if it exists)
     * or a string saying that the value is not provided.
     * @param stringToCheck
     * @param fieldName
     * @return String
     */
    private String nullCheck( String stringToCheck, String fieldName )
    {
        if ( stringToCheck != null && !stringToCheck.isEmpty() )
        {
            return stringToCheck;
        }
        else
        {
            return "(" + fieldName + " not provided)";
        }
    }


    public void setReceiver( String receiver )
    {
        this.receiver = receiver;
    }


    public void setSender( String sender )
    {
        this.sender = sender;
    }


    public String getSender()
    {
        return sender;
    }


    public void setHostName( String hostName )
    {
        this.hostName = hostName;
    }

}
