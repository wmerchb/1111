
package ca.medavie.subrogation.repository;

import java.math.BigDecimal;
import java.util.Collection;

import ca.medavie.utilization.common.DateRange;

public interface FileCountsRepository
{
    /**
     * Find the total number of files opened in the certain period of time
     *
     * @param DateRange - dateRange A Certain SearchCriteria
     * @return A Collection of number of files opened 
     */
    public Collection<Integer> retrieveNumberOfFilesOpened( DateRange dateRange );


    /**
     * Find the total number of files closed in the certain period of time
     *
     * @param DateRange - dateRange A Certain SearchCriteria
     * @return A Collection of number of files closed 
     */
    public Collection<Integer> retrieveNumberOfFilesClosed( DateRange dateRange );


    /**
     * Find the recovery amount in the certain period of time
     *
     * @param DateRange - dateRange A Certain SearchCriteria
     * @return Collection - a collection of recovery amount
     */
    public Collection<BigDecimal> retrieveRecoveryAmount( DateRange dateRange );


    /**
     * Find the summary amount in the certain period of time
     *
     * @param DateRange - dateRange A Certain SearchCriteria
     * @return Collection - a collection of summary amount
     */
    public Collection<BigDecimal> retrieveSummaryAmount( DateRange dateRange );

}
