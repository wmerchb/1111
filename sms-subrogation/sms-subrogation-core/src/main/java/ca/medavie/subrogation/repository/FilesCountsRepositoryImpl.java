
package ca.medavie.subrogation.repository;

import java.math.BigDecimal;
import java.util.Collection;

import org.apache.log4j.Logger;

import ca.medavie.subrogation.dao.FileCountsDAO;
import ca.medavie.utilization.common.DateRange;

/**
 * Finds the total number of files opened over a given period of time
 */
public class FilesCountsRepositoryImpl implements FileCountsRepository
{

    private static final Logger logger = Logger.getLogger( FilesCountsRepositoryImpl.class );
    /*DAO for handling the data base transaction stuff*/
    private FileCountsDAO fileCountsDAO;


    /**
     * Find the total number of files opened in the certain period of time
     *
     * @param DateRange - dateRange A Certain SearchCriteria
     * @return A Collection of number of files opened 
     */
    public Collection<Integer> retrieveNumberOfFilesOpened( DateRange dateRange )

    {

        if ( dateRange == null )
        {

            logger.error( ":dateRange passed in were NULL. " );
            throw new IllegalArgumentException( "dateRange passed in were NULL. " );
        }

        Collection<Integer> filesOpened = null;
        filesOpened = fileCountsDAO.retrieveNumberOfFilesOpened( dateRange );

        return filesOpened;
    }


    /**
     * Find the total number of files closed in the certain period of time
     *
     * @param DateRange - dateRange A Certain SearchCriteria
     * @return A Collection of number of files closed 
     */
    public Collection<Integer> retrieveNumberOfFilesClosed( DateRange dateRange )

    {

        if ( dateRange == null )
        {

            logger.error( ":dateRange passed in were NULL. " );
            throw new IllegalArgumentException( "dateRange passed in were NULL. " );
        }

        Collection<Integer> filesClosed = null;
        filesClosed = fileCountsDAO.retrieveNumberOfFilesClosed( dateRange );

        return filesClosed;
    }


    /**
     * Find the recovery amount in the certain period of time
     *
     * @param DateRange - dateRange A Certain SearchCriteria
     * @return Collection - a collection of recovery amount
     */
    public Collection<BigDecimal> retrieveRecoveryAmount( DateRange dateRange )

    {

        if ( dateRange == null )
        {

            logger.error( ":dateRange passed in were NULL. " );
            throw new IllegalArgumentException( "dateRange passed in were NULL. " );
        }

        Collection<BigDecimal> recoveryAmounts = null;
        recoveryAmounts = fileCountsDAO.retrieveRecoveryAmount( dateRange );

        return recoveryAmounts;
    }


    /**
     * Find the summary amount in the certain period of time
     *
     * @param DateRange - dateRange A Certain SearchCriteria
     * @return Collection - a collection of summary amount
     */
    public Collection<BigDecimal> retrieveSummaryAmount( DateRange dateRange )

    {

        if ( dateRange == null )
        {

            logger.error( ":dateRange passed in were NULL. " );
            throw new IllegalArgumentException( "dateRange passed in were NULL. " );
        }

        Collection<BigDecimal> summary_amounts = null;
        summary_amounts = fileCountsDAO.retrieveSummaryAmount( dateRange );

        return summary_amounts;
    }


    /**
     * @return the fileCountsDAO
     */
    public FileCountsDAO getFileCountsDAO()
    {
        return fileCountsDAO;
    }


    /**
     * @param fileCountsDAO the fileCountsDAO to set
     */
    public void setFileCountsDAO( FileCountsDAO fileCountsDAO )
    {
        this.fileCountsDAO = fileCountsDAO;
    }

}
