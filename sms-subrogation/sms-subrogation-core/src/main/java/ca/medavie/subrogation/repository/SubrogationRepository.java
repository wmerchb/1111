
package ca.medavie.subrogation.repository;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import ca.medavie.subrogation.entities.FollowUp;
import ca.medavie.subrogation.entities.Subrogation;
import ca.medavie.subrogation.exceptions.SubrogationException;
import ca.medavie.utilization.SearchCriteria;

/**
 * Finds subscribers by search criteria (first/last names, sub/group IDs)
 */
public interface SubrogationRepository
{

    /**
     * Find subrogation client by search criteria
     *
     * @param A Certain SearchCriteria
     * @return A Collection of subrogation Objects
     * @throws SubrogationException
     */
    public Collection<Subrogation> retrieveSubrogationBySearchCriteria( SearchCriteria aSearchCriteria )
            throws SubrogationException;


    /**
     * Find subrogation by File number
     *
     * @param Long - A File number for the subrogation
     * @return Subrogation - a Subrogation found by this file number
     * @throws SubrogationException
     */
    public Subrogation retrieveSubrogationByFileNumber( Long aFileNumber ) throws SubrogationException;


    /**
     * Save update Subrogation
     *
     * @param a Subrogation Object
     * @throws SubrogationException
     * @return 
     */
    public void saveUpdateSubrogation( Subrogation aSubrogation ) throws SubrogationException;


    /**
     * Delete Subrogation
     *
     * @param a Subrogation Object
     * @return 
     */
    public void deleteSubrogation( Subrogation aSubrogation ) throws SubrogationException;


    /**
     * Find the follow ups for a given date. Uses the 'set' prefix because after retrieving the
     * FollowUps, the FollowUps are updated to have their state = 'Executing'. This ensures that
     * multiple threads don't end up accessing and sending duplicate Follow Up Emails.
     * @param aDate
     * @return List<FollowUp>
     */
    public List<FollowUp> retrieveUpdateFollowUps( Date aDate );


    /**
     * Saves or updates a collection of FollowUp objects.
     * 
     * @param aFollowUpList
     */
    public void saveFollowUps( List<FollowUp> aFollowUpList );
}
