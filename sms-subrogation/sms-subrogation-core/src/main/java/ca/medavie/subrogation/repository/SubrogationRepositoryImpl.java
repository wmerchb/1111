
package ca.medavie.subrogation.repository;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import ca.medavie.subrogation.dao.SubrogationDAO;
import ca.medavie.subrogation.entities.FollowUp;
import ca.medavie.subrogation.entities.Subrogation;
import ca.medavie.subrogation.exceptions.SubrogationException;
import ca.medavie.utilization.SearchCriteria;

/**
 * Finds subscribers by search criteria (first/last names, sub/group IDs)
 */
public class SubrogationRepositoryImpl implements SubrogationRepository
{

    private static final Logger logger = Logger.getLogger( SubrogationRepositoryImpl.class );
    /*DAO for handling the data base transaction stuff*/
    private SubrogationDAO subrogationDAO;


    /* (non-Javadoc)
     * @see ca.medavie.subrogation.repository.SubrogationRepository#retrieveSubrogationBySearchCriteria(ca.medavie.utilization.SearchCriteria)
     */
    public Collection<Subrogation> retrieveSubrogationBySearchCriteria( SearchCriteria searchCriteria )
            throws SubrogationException
    {
        if ( searchCriteria == null )
        {
            logger.error( ":searchCriteria passed in were NULL. " );
            throw new IllegalArgumentException( "searchCriteria passed in were NULL. " );
        }

        Collection<Subrogation> subrogations = null;
        subrogations = subrogationDAO.retrieveSubrogationBySearchCriteria( searchCriteria );

        return subrogations;
    }


    /* (non-Javadoc)
     * @see ca.medavie.subrogation.repository.SubrogationRepository#retrieveSubrogationByFileNumber(java.lang.Long)
     */
    public Subrogation retrieveSubrogationByFileNumber( Long aFileNumber ) throws SubrogationException
    {
        if ( aFileNumber == null )
        {

            logger.error( ":aFileNumber passed in were NULL. " );
            throw new IllegalArgumentException( "aFileNumber passed in were NULL. " );
        }

        Subrogation subrogation = null;
        subrogation = subrogationDAO.retrieveSubrogationByFileNumber( aFileNumber );

        return subrogation;
    }


    /* (non-Javadoc)
     * @see ca.medavie.subrogation.repository.SubrogationRepository#saveUpdateSubrogation(ca.medavie.subrogation.entities.Subrogation)
     */
    public void saveUpdateSubrogation( Subrogation aSubrogation ) throws SubrogationException
    {

        if ( aSubrogation == null )
        {

            logger.error( ":aSubrogation object passed in were NULL. " );
            throw new IllegalArgumentException( ":aSubrogation object passed in were NULL." );
        }
        subrogationDAO.saveUpdateSubrogation( aSubrogation );

    }


    /**
     * @return the aSubrogationDAO
     */
    public SubrogationDAO getSubrogationDAO()
    {
        return subrogationDAO;
    }


    /**
     * @param subrogationDAO the aSubrogationDAO to set
     */
    public void setSubrogationDAO( SubrogationDAO aSubrogationDAO )
    {
        subrogationDAO = aSubrogationDAO;
    }


    /* (non-Javadoc)
     * @see ca.medavie.subrogation.repository.SubrogationRepository#deleteSubrogation(ca.medavie.subrogation.entities.Subrogation)
     */
    public void deleteSubrogation( Subrogation aSubrogation ) throws SubrogationException
    {

        if ( aSubrogation == null )
        {

            logger.error( ":aSubrogation object passed in were NULL. " );
            throw new IllegalArgumentException( ":aSubrogation object passed in were NULL." );
        }
        subrogationDAO.deleteSubrogation( aSubrogation );

    }


    /* (non-Javadoc)
     * @see ca.medavie.subrogation.repository.SubrogationRepository#retrieveSubrogationForFollowup(java.util.Date)
     */
    public List<FollowUp> retrieveUpdateFollowUps( Date aDate )
    {

        if ( aDate == null )
        {
            logger.error( ":aDate object passed in were NULL. " );
            throw new IllegalArgumentException( ":aDate object passed in were NULL." );
        }

        return subrogationDAO.findAndUpdateFollowUps( aDate );
    }


    /* (non-Javadoc)
     * @see ca.medavie.subrogation.repository.SubrogationRepository#saveFollowUps(java.util.list<ca.medavie.subrogation.entities.FollowUp>)
     */
    public void saveFollowUps( List<FollowUp> aFollowUpList )
    {

        if ( aFollowUpList == null )
        {
            logger.error(":aFollowUpList object passed in were NULL. " );
            throw new IllegalArgumentException( ":aFollowUpList object passed in were NULL." );
        }

        subrogationDAO.saveUpdateFollowUps( aFollowUpList );
    }

}
