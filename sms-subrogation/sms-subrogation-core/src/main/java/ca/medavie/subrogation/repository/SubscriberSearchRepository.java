
package ca.medavie.subrogation.repository;

import java.util.Collection;

/**
 * Searches the individual items for subrogation's search
 */
public interface SubscriberSearchRepository
{
    /**
     * Find Subscriber First Names
     * 
     * @param String
     *            - First Name
     * @return Collection - a Collection of first names
     */
    public Collection<String> retrieveSubscriberFirstNames( String firstName );


    /**
     * Find Subscriber Last Names
     * 
     * @param String
     *            - Last Name
     *  @return Collection - a Collection of last names
     */
    public Collection<String> retrieveSubscriberLastNames( String lastName );


    /**
     * Find Subscriber Group IDs
     * 
     * @param String
     *            - group ID
     * @return Collection - a Collection of group ids
     */
    public Collection<String> retrieveSubscriberGroupIDs( String groupID );


    /**
     * Find Subscriber Sub IDs
     * 
     * @param String
     *            - Sub IDs
     * @return Collection - a Collection of sub ids
     */
    public Collection<String> retrieveSubscriberSubIDs( String subIDs );
}
