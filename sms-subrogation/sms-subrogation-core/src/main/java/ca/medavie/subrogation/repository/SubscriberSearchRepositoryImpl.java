
package ca.medavie.subrogation.repository;

import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;

import ca.medavie.subrogation.dao.SubscriberSearchDAO;

/**
 * Searches the individual items for subrogation's search
 */
public class SubscriberSearchRepositoryImpl implements SubscriberSearchRepository
{

    private static final Logger logger = Logger.getLogger( SubscriberSearchRepositoryImpl.class );
    /**
     * DAO for handling the data base transaction stuff
     * 
     * */
    private SubscriberSearchDAO subscriberSearchDAO;


    /**
     * Find Subscriber First Names
     * 
     * @param String
     *            - First Name
     * @return Collection - a Collection of first names
     */
    public Collection<String> retrieveSubscriberFirstNames( String firstName )
    {

        if ( firstName == null )
        {

            logger.error( ":firstName passed in were NULL. " );
            throw new IllegalArgumentException( "firstName passed in were NULL. " );
        }

        List<String> firstNams = null;
        try
        {
            firstNams = subscriberSearchDAO.retrieveFirstNames( firstName );
        }
        catch ( Exception e )
        {
            logger.error( e.getMessage() );
        }

        if ( firstNams == null )
        {
            logger.error( "Could not load first nams from subrogation table!" );
            return null;
        }
        return firstNams;
    }


    /**
     * Find Subscriber Last Names
     * 
     * @param String
     *            - Last Name
     *  @return Collection - a Collection of last names
     */
    public Collection<String> retrieveSubscriberLastNames( String lastName )
    {

        if ( lastName == null )
        {

            logger.error( ":LastName passed in were NULL. " );
            throw new IllegalArgumentException( "LastName passed in were NULL. " );
        }

        List<String> lastNams = null;
        try
        {
            lastNams = subscriberSearchDAO.retrieveLastNames( lastName );
        }
        catch ( Exception e )
        {
            logger.error( e.getMessage() );
        }

        if ( lastNams == null )
        {
            logger.error( "Could not load last nams from subrogation table!" );
            return null;
        }
        return lastNams;
    }


    /**
     * Find Subscriber Group IDs
     * 
     * @param String
     *            - group ID
     * @return Collection - a Collection of group ids
     */
    public Collection<String> retrieveSubscriberGroupIDs( String groupID )
    {

        if ( groupID == null )
        {

            logger.error( ":firstName passed in were NULL. " );
            throw new IllegalArgumentException( "firstName passed in were NULL. " );
        }

        List<String> groupIDs = null;
        try
        {
            groupIDs = subscriberSearchDAO.retrieveGroupIDs( groupID );
        }
        catch ( Exception e )
        {
            logger.error( e.getMessage() );
        }

        if ( groupIDs == null )
        {
            logger.error( "Could not load group ids from subrogation table!" );
            return null;
        }
        return groupIDs;
    }


    /**
     * Find Subscriber Sub IDs
     * 
     * @param String
     *            - Sub IDs
     * @return Collection - a Collection of sub ids
     */
    public Collection<String> retrieveSubscriberSubIDs( String subID )
    {

        if ( subID == null )
        {

            logger.error( ":firstName passed in were NULL. " );
            throw new IllegalArgumentException( "firstName passed in were NULL. " );
        }

        List<String> subIDs = null;
        try
        {
            subIDs = subscriberSearchDAO.retrieveSubIDs( subID );
        }
        catch ( Exception e )
        {
            logger.error( e.getMessage() );
        }

        if ( subIDs == null )
        {
            logger.error( "Could not load sub ids from subrogation table!" );
            return null;
        }
        return subIDs;
    }


    /**
     * @return the subscriberSearchDAO
     */
    public SubscriberSearchDAO getSubscriberSearchDAO()
    {
        return subscriberSearchDAO;
    }


    /**
     * @param subscriberSearchDAO the subscriberSearchDAO to set
     */
    public void setSubscriberSearchDAO( SubscriberSearchDAO subscriberSearchDAO )
    {
        this.subscriberSearchDAO = subscriberSearchDAO;
    }

}
