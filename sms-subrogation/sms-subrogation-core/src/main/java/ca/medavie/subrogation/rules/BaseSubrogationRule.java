
package ca.medavie.subrogation.rules;

import ca.bluecross.atl.utilization.blo.Message;
import ca.medavie.subrogation.repository.FileCountsRepository;
import ca.medavie.subrogation.repository.SubrogationRepository;
import ca.medavie.subrogation.repository.SubscriberSearchRepository;
import ca.medavie.utilization.validation.Rule;

/**
 * Base rule implementations.
 * 
 */
public abstract class BaseSubrogationRule implements Rule
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * a SubrogationRepository
     * */
    private SubrogationRepository subrogationRepository;

    /**
     * a subscriberSearchRepository
     * */
    private SubscriberSearchRepository subscriberSearchRepository;

    /**
     * a fileCountsRepository
     * */
    private FileCountsRepository fileCountsRepository;


    /**
     * @return the subrogationRepository
     */
    public SubrogationRepository getSubrogationRepository()
    {
        return subrogationRepository;
    }


    /**
     * @param subrogationRepository the subrogationRepository to set
     */
    public void setSubrogationRepository( SubrogationRepository subrogationRepository )
    {
        this.subrogationRepository = subrogationRepository;
    }


    /**
     * @return the subscriberSearchRepository
     */
    public SubscriberSearchRepository getSubscriberSearchRepository()
    {
        return subscriberSearchRepository;
    }


    /**
     * @param subscriberSearchRepository the subscriberSearchRepository to set
     */
    public void setSubscriberSearchRepository( SubscriberSearchRepository subscriberSearchRepository )
    {
        this.subscriberSearchRepository = subscriberSearchRepository;
    }


    /**
     * @return the fileCountsRepository
     */
    public FileCountsRepository getFileCountsRepository()
    {
        return fileCountsRepository;
    }


    /**
     * @param fileCountsRepository the fileCountsRepository to set
     */
    public void setFileCountsRepository( FileCountsRepository fileCountsRepository )
    {
        this.fileCountsRepository = fileCountsRepository;
    }


    /**
     * @see ca.medavie.utilization.validation.Rule#check(java.lang.Object)
     */
    public abstract boolean check( Object o );


    /**
     * @see ca.medavie.utilization.validation.Rule#getMessage()
     */
    public abstract Message getMessage();

}
