
package ca.medavie.subrogation.rules;

import org.apache.log4j.Logger;

import ca.bluecross.atl.utilization.blo.Message;
import ca.bluecross.atl.utilization.blo.ParameterizedMessage;

/**
 * Rules for a subscriber to be saved or updated
 * Rules says: The section must be a valid group
 */
public class ValidSecionRule extends BaseSubrogationRule
{

    private static final Logger logger = Logger.getLogger( ValidSecionRule.class );
    /**
     * This alternate product must be from product_info table
     */
    private static final long serialVersionUID = 1L;


    @Override
    public boolean check( Object o )
    {
        logger.debug("rule check executed!!" );
        return true;

    }


    @Override
    public Message getMessage()
    {
        ParameterizedMessage message = null;

        return message;
    }
}
