
package ca.medavie.subrogation.validation;

import java.util.Iterator;
import java.util.List;

import ca.medavie.utilization.validation.Rule;
import ca.medavie.utilization.validation.ValidationStrategy;

/**
 * TODO Fill in this class's Javadoc comment.
 */
public class SubrogationBaseValidationStrategy implements ValidationStrategy
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /** List of rules to checked by strategy */
    private List<Rule> rules;


    /**
     * Iterate through the rules
     * @param Object
     * @return boolean - Indicates whether the Object is valid.
     */
    public boolean validate( Object object, List messages )
    {
        messages.clear();
        Iterator<Rule> rulesIter = rules.iterator();
        while ( rulesIter.hasNext() )
        {
            Rule r = rulesIter.next();

            if ( !r.check( object ) )
            {
                messages.add( r.getMessage() );
                return false;
            }
        }
        return true;
    }


    /**
     * @return Returns the rules.
     */
    public List<Rule> getRules()
    {
        return rules;
    }


    /**
     * @param rules
     *          The rules to set.
     */
    public void setRules( List<Rule> aRules )
    {
        rules = aRules;
    }

}
