package ca.bluecode.version;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * Reads the version information from Maven generated properties file
 */
public class ApplicationVersion {

    private static final Logger logger = Logger.getLogger(ApplicationVersion.class);

    private String applicationName;
    private String version;
    private String timestamp;

    public ApplicationVersion() {

        Properties properties = new Properties();

        try {
            InputStream inStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("app.properties");

            // If maven did not build the application, it will be null
            if (inStream != null) {
                properties.load(inStream);
            }

        } catch (IOException e) {
            logger.error("ApplicationVersion(): Could not load application build properties", e);
        }

        this.applicationName = properties.getProperty("application.name");
        this.version = properties.getProperty("application.version");
        this.timestamp = properties.getProperty("build.timestamp");
    }

    /**
     * @return the version identifier of this release
     */
    public String getVersion() {
        return version;
    }

    /**
     * @return the English acronym of this application
     */
    public String getApplicationName() {
        return applicationName;
    }

    /**
     * @return the timestamp from the build as a String
     */
    public String getTimestamp() {
        return timestamp;
    }

}
