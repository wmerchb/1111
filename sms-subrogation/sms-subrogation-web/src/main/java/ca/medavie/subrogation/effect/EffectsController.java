
package ca.medavie.subrogation.effect;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import ca.medavie.subrogation.ui.BaseBean;

import java.io.Serializable;

/**
 *Class for handling the the effect actions bonding to the UI components.
 */

public class EffectsController extends BaseBean implements Serializable
{

    /**
     * Default serial number
     */
    private static final long serialVersionUID = 5813896495077490050L;


    /**
     * Slide up action
     * 
     ********PLEASE NOTE*******
     * You have to create separate effect wrappers attaching to the UIcomponents respectively. 
     * If you attache the same Effect wrapper to the different UI components, 
     * it only takes effect on the first one. 
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     * */
    public void slideUp( ActionEvent event )
    {

        String effectKey = "effectSlideUp";
        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        // do a look up for the effect
        EffectsModel.EffectWrapper effectWrapper = effectsModel.getEffects().get( effectKey );

        // if found we reset the effect to fire on the soon to occure
        // response.
        if ( effectWrapper != null )
        {
            effectWrapper.getEffect().setFired( false );
            effectsModel.setCurrentEffectWrapper( effectWrapper );

        }

    }


    /**
     * Slide up action for summary panel
     * 
     ********PLEASE NOTE*******
     * You have to create separate effect wrappers attaching to the UIcomponents respectively. 
     * If you attache the same Effect wrapper to the different UI components, 
     * it only takes effect on the first one. 
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     * */
    public void summarySlideUp( ActionEvent event )
    {

        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        EffectsModel.EffectWrapper summaryInfoPanelSlideUpWrapper = effectsModel.getSummaryInfoPanelSlideUpWrapper();

        if ( summaryInfoPanelSlideUpWrapper != null )
        {
            summaryInfoPanelSlideUpWrapper.getEffect().setFired( false );
            effectsModel.setSummaryInfoPanelSlideUpWrapper( summaryInfoPanelSlideUpWrapper );

        }

    }


    /**
     * Slide down action for summary panel
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     ********PLEASE NOTE*******
     * You have to create separate effect wrappers attaching to the UIcomponents respectively. 
     * If you attache the same Effect wrapper to the different UI components, 
     * it only takes effect on the first one. 
     * 
     * */
    public void summarySlideDown( ActionEvent event )
    {
        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        EffectsModel.EffectWrapper summaryInfoPanelSlideDownWrapper = effectsModel.getSummaryInfoPanelSlideDownWrapper();

        if ( summaryInfoPanelSlideDownWrapper != null )
        {
            summaryInfoPanelSlideDownWrapper.getEffect().setFired( false );
            effectsModel.setSummaryInfoPanelSlideDownWrapper( summaryInfoPanelSlideDownWrapper );

        }

    }


    /**
     * Slide up action recovery panel
     * 
     ********PLEASE NOTE*******
     * You have to create separate effect wrappers attaching to the UIcomponents respectively. 
     * If you attache the same Effect wrapper to the different UI components, 
     * it only takes effect on the first one. 
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     * */
    public void recoverySlideUp( ActionEvent event )
    {

        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        EffectsModel.EffectWrapper recoveryInfoPanelSlideUpWrapper = effectsModel.getRecoveryInfoPanelSlideUpWrapper();

        if ( recoveryInfoPanelSlideUpWrapper != null )
        {
            recoveryInfoPanelSlideUpWrapper.getEffect().setFired( false );
            effectsModel.setRecoveryInfoPanelSlideUpWrapper( recoveryInfoPanelSlideUpWrapper );

        }

    }


    /**
     * Slide down action for recovery panel
     * 
     ********PLEASE NOTE*******
     * You have to create separate effect wrappers attaching to the UIcomponents respectively. 
     * If you attache the same Effect wrapper to the different UI components, 
     * it only takes effect on the first one. 
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     * */
    public void recoverySlideDown( ActionEvent event )
    {
        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        EffectsModel.EffectWrapper recoveryInfoPanelSlideDownWrapper = effectsModel.getRecoveryInfoPanelSlideDownWrapper();

        if ( recoveryInfoPanelSlideDownWrapper != null )
        {
            recoveryInfoPanelSlideDownWrapper.getEffect().setFired( false );
            effectsModel.setRecoveryInfoPanelSlideDownWrapper( recoveryInfoPanelSlideDownWrapper );

        }

    }


    /**
     * Slide up action fro file status panel
     * 
     ********PLEASE NOTE*******
     * You have to create separate effect wrappers attaching to the UIcomponents respectively. 
     * If you attache the same Effect wrapper to the different UI components, 
     * it only takes effect on the first one. 
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     * */
    public void fileStatusSlideUp( ActionEvent event )
    {

        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        EffectsModel.EffectWrapper fileStatusPanelSlideUpWrapper = effectsModel.getFileStatusPanelSlideUpWrapper();

        if ( fileStatusPanelSlideUpWrapper != null )
        {
            fileStatusPanelSlideUpWrapper.getEffect().setFired( false );
            effectsModel.setFileStatusPanelSlideUpWrapper( fileStatusPanelSlideUpWrapper );

        }

    }


    /**
     * Slide down action for file status panel
     * 
     *      *********PLEASE NOTE*******
     * You have to create a separate effect wrapper attaching to the UIcomponents respectively, otherwise, 
     * the effect only happen the first UI component attached but not others
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     * */
    public void fileStatusSlideDown( ActionEvent event )
    {
        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        EffectsModel.EffectWrapper fileStatusPanelSlideDownWrapper = effectsModel.getFileStatusPanelSlideDownWrapper();

        if ( fileStatusPanelSlideDownWrapper != null )
        {
            fileStatusPanelSlideDownWrapper.getEffect().setFired( false );
            effectsModel.setFileStatusPanelSlideDownWrapper( fileStatusPanelSlideDownWrapper );

        }

    }


    /**
     * Slide up action for comment panel
     * 
     ********PLEASE NOTE*******
     * You have to create separate effect wrappers attaching to the UIcomponents respectively. 
     * If you attache the same Effect wrapper to the different UI components, 
     * it only takes effect on the first one. 
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     * */
    public void commentSlideUp( ActionEvent event )
    {

        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        EffectsModel.EffectWrapper commentPanelSlideUpWrapper = effectsModel.getCommentPanelSlideUpWrapper();

        if ( commentPanelSlideUpWrapper != null )
        {
            commentPanelSlideUpWrapper.getEffect().setFired( false );
            effectsModel.setCommentPanelSlideUpWrapper( commentPanelSlideUpWrapper );

        }

    }


    /**
     * Slide down action for comment panel
     * 
     ********PLEASE NOTE*******
     * You have to create separate effect wrappers attaching to the UIcomponents respectively. 
     * If you attache the same Effect wrapper to the different UI components, 
     * it only takes effect on the first one. 
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     * */
    public void commentSlideDown( ActionEvent event )
    {

        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        EffectsModel.EffectWrapper commentPanelSlideDownWrapper = effectsModel.getCommentPanelSlideDownWrapper();

        if ( commentPanelSlideDownWrapper != null )
        {
            commentPanelSlideDownWrapper.getEffect().setFired( false );
            effectsModel.setCommentPanelSlideDownWrapper( commentPanelSlideDownWrapper );

        }

    }


    /**
     * Appear action for subscriber search result panel
     * 
     ********PLEASE NOTE*******
     * You have to create separate effect wrappers attaching to the UIcomponents respectively. 
     * If you attache the same Effect wrapper to the different UI components, 
     * it only takes effect on the first one. 
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     * */
    public void searchResultAppear( ActionEvent event )
    {
        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        EffectsModel.EffectWrapper searchResultAppearWrapper = effectsModel.getSearchResultAppearWrapper();

        if ( searchResultAppearWrapper != null )
        {
            searchResultAppearWrapper.getEffect().setFired( false );
            effectsModel.setSearchResultAppearWrapper( searchResultAppearWrapper );

        }

    }


    /**
     * Fade action for new icon fading down up when switch to maintenance page
     * 
     ********PLEASE NOTE*******
     * You have to create separate effect wrappers attaching to the UIcomponents respectively. 
     * If you attache the same Effect wrapper to the different UI components, 
     * it only takes effect on the first one. 
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     * */
    public void newIconFade( ActionEvent event )
    {
        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        EffectsModel.EffectWrapper newIconFadeWrapper = effectsModel.getNewIconFadeWrapper();

        if ( newIconFadeWrapper != null )
        {
            newIconFadeWrapper.getEffect().setFired( false );
            effectsModel.setNewIconFadeWrapper( newIconFadeWrapper );

        }

    }


    /**
     * Appear action for new icon appearing up when switch to adding a new subscriber.
     * 
     ********PLEASE NOTE*******
     * You have to create separate effect wrappers attaching to the UIcomponents respectively. 
     * If you attache the same Effect wrapper to the different UI components, 
     * it only takes effect on the first one. 
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     * */
    public void newIconAppear( ActionEvent event )
    {
        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        EffectsModel.EffectWrapper newIconAppearWrapper = effectsModel.getNewIconAppearWrapper();

        if ( newIconAppearWrapper != null )
        {
            newIconAppearWrapper.getEffect().setFired( false );
            effectsModel.setNewIconAppearWrapper( newIconAppearWrapper );

        }

    }


    /**
     * Slide down action for search subscriber panel
     * 
     ********PLEASE NOTE*******
     * You have to create separate effect wrappers attaching to the UIcomponents respectively. 
     * If you attache the same Effect wrapper to the different UI components, 
     * it only takes effect on the first one. 
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     * */
    public void slideDown( ActionEvent event )
    {
        String effectKey = "effectSlideDown";
        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        // do a look up for the effect
        EffectsModel.EffectWrapper effectWrapper = effectsModel.getEffects().get( effectKey );

        // if found we reset the effect to fire on the soon to occure
        // response.
        if ( effectWrapper != null )
        {
            effectWrapper.getEffect().setFired( false );
            effectsModel.setCurrentEffectWrapper( effectWrapper );

        }

    }


    /**
     * Highlight action for the operation title.
     * 
     ********PLEASE NOTE*******
     * You have to create separate effect wrappers attaching to the UIcomponents respectively. 
     * If you attache the same Effect wrapper to the different UI components, 
     * it only takes effect on the first one. 
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     * */
    public void higlight( ValueChangeEvent vce )
    {
        String effectKey = "effectHighlight";
        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        // do a look up for the effect
        EffectsModel.EffectWrapper effectWrapper = effectsModel.getEffects().get( effectKey );

        // if found we reset the effect to fire on the soon to occure
        // response.
        if ( effectWrapper != null )
        {
            effectWrapper.getEffect().setFired( false );
            effectsModel.setCurrentEffectWrapper( effectWrapper );

        }

    }


    /**
     * Whole panel highlight when doing the reset
     * 
     ********PLEASE NOTE*******
     * You have to create separate effect wrappers attaching to the UIcomponents respectively. 
     * If you attache the same Effect wrapper to the different UI components, 
     * it only takes effect on the first one. 
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     * */
    public void wholePanelHighlight( ActionEvent event )
    {
        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        EffectsModel.EffectWrapper wholePanelHighlightWrapper = effectsModel.getWholePanelHighlightWrapper();

        if ( wholePanelHighlightWrapper != null )
        {
            wholePanelHighlightWrapper.getEffect().setFired( false );
            effectsModel.setWholePanelHighlightWrapper( wholePanelHighlightWrapper );

        }

    }


    /**
     * Whole panel Fade and Appear when doing the reset
     * 
     ********PLEASE NOTE*******
     * You have to create separate effect wrappers attaching to the UIcomponents respectively. 
     * If you attache the same Effect wrapper to the different UI components, 
     * it only takes effect on the first one. 
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     * */
    public void wholePanelFadeAndAppear( ActionEvent event )
    {
        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        EffectsModel.EffectWrapper wholePanelFadeAndAppearWrapper = effectsModel.getWholePanelFadeAndAppearWrapper();

        if ( wholePanelFadeAndAppearWrapper != null )
        {
            wholePanelFadeAndAppearWrapper.getEffect().setFired( false );
            effectsModel.setWholePanelFadeAndAppearWrapper( wholePanelFadeAndAppearWrapper );

        }

    }


    /**
     * Pulsing action for the search button when switch back to maintenance page
     * 
     ********PLEASE NOTE*******
     * You have to create separate effect wrappers attaching to the UIcomponents respectively. 
     * If you attache the same Effect wrapper to the different UI components, 
     * it only takes effect on the first one.
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     * */
    public void searchButtonPulsate( ActionEvent event )
    {
        String effectKey = "effectPulsate";
        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        // do a look up for the effect
        EffectsModel.EffectWrapper effectWrapper = effectsModel.getEffects().get( effectKey );

        // if found we reset the effect to fire on the soon to occure
        // response.
        if ( effectWrapper != null )
        {
            effectWrapper.getEffect().setFired( false );
            effectsModel.setCurrentEffectWrapper( effectWrapper );

        }

    }


    /**
     * Pulsing action 
     * 
     ********PLEASE NOTE*******
     * You have to create separate effect wrappers attaching to the UIcomponents respectively. 
     * If you attache the same Effect wrapper to the different UI components, 
     * it only takes effect on the first one.
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     * */
    public void errorMessagesPulsate( ActionEvent event )
    {
        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        EffectsModel.EffectWrapper birthDateErrorEW = effectsModel.getBirthDateErrorEW();

        if ( birthDateErrorEW != null )
        {
            birthDateErrorEW.getEffect().setFired( false );
            effectsModel.setBirthDateErrorEW( birthDateErrorEW );
        }

        EffectsModel.EffectWrapper summaryDateErrorEW = effectsModel.getSummaryDateErrorEW();

        if ( summaryDateErrorEW != null )
        {
            summaryDateErrorEW.getEffect().setFired( false );
            effectsModel.setSummaryDateErrorEW( summaryDateErrorEW );

        }

        EffectsModel.EffectWrapper summaryAmountErrorEW = effectsModel.getSummaryAmountErrorEW();

        if ( summaryAmountErrorEW != null )
        {
            summaryAmountErrorEW.getEffect().setFired( false );
            effectsModel.setSummaryAmountErrorEW( summaryAmountErrorEW );

        }

        EffectsModel.EffectWrapper fileOpenErrorEW = effectsModel.getFileOpenErrorEW();

        if ( fileOpenErrorEW != null )
        {
            fileOpenErrorEW.getEffect().setFired( false );
            effectsModel.setFileOpenErrorEW( fileOpenErrorEW );

        }

        EffectsModel.EffectWrapper accidentDateErrorEW = effectsModel.getAccidentDateErrorEW();

        if ( accidentDateErrorEW != null )
        {
            accidentDateErrorEW.getEffect().setFired( false );
            effectsModel.setAccidentDateErrorEW( accidentDateErrorEW );

        }

        EffectsModel.EffectWrapper recoveryDateErrorEW = effectsModel.getRecoveryDateErrorEW();

        if ( recoveryDateErrorEW != null )
        {
            recoveryDateErrorEW.getEffect().setFired( false );
            effectsModel.setRecoveryDateErrorEW( recoveryDateErrorEW );

        }

        EffectsModel.EffectWrapper recoveryAmountErrorEW = effectsModel.getRecoveryAmountErrorEW();

        if ( recoveryAmountErrorEW != null )
        {
            recoveryAmountErrorEW.getEffect().setFired( false );
            effectsModel.setRecoveryAmountErrorEW( recoveryAmountErrorEW );

        }

        EffectsModel.EffectWrapper lawyerFeeErrorEW = effectsModel.getLawyerFeeErrorEW();

        if ( lawyerFeeErrorEW != null )
        {
            lawyerFeeErrorEW.getEffect().setFired( false );
            effectsModel.setLawyerFeeErrorEW( lawyerFeeErrorEW );

        }

        EffectsModel.EffectWrapper netRecoveryErrorEW = effectsModel.getNetRecoveryErrorEW();

        if ( netRecoveryErrorEW != null )
        {
            netRecoveryErrorEW.getEffect().setFired( false );
            effectsModel.setNetRecoveryErrorEW( netRecoveryErrorEW );

        }

        EffectsModel.EffectWrapper wlsDateErrorEW = effectsModel.getWlsDateErrorEW();

        if ( wlsDateErrorEW != null )
        {
            wlsDateErrorEW.getEffect().setFired( false );
            effectsModel.setWlsDateErrorEW( wlsDateErrorEW );

        }

        EffectsModel.EffectWrapper llsDateErrorEW = effectsModel.getLlsDateErrorEW();

        if ( llsDateErrorEW != null )
        {
            llsDateErrorEW.getEffect().setFired( false );
            effectsModel.setLlsDateErrorEW( llsDateErrorEW );

        }

        EffectsModel.EffectWrapper misDateErrorEW = effectsModel.getMisDateErrorEW();
        if ( misDateErrorEW != null )
        {
            misDateErrorEW.getEffect().setFired( false );
            effectsModel.setMisDateErrorEW( misDateErrorEW );

        }

        EffectsModel.EffectWrapper wlfDateErrorEW = effectsModel.getWlfDateErrorEW();
        if ( wlfDateErrorEW != null )
        {
            wlfDateErrorEW.getEffect().setFired( false );
            effectsModel.setWlfDateErrorEW( wlfDateErrorEW );

        }

        EffectsModel.EffectWrapper llfDateErrorEW = effectsModel.getLlfDateErrorEW();
        if ( llfDateErrorEW != null )
        {
            llfDateErrorEW.getEffect().setFired( false );
            effectsModel.setLlfDateErrorEW( llfDateErrorEW );

        }

        EffectsModel.EffectWrapper mifDateErrorEW = effectsModel.getMifDateErrorEW();
        if ( mifDateErrorEW != null )
        {
            mifDateErrorEW.getEffect().setFired( false );
            effectsModel.setMifDateErrorEW( mifDateErrorEW );

        }

        EffectsModel.EffectWrapper ilDateErrorEW = effectsModel.getIlDateErrorEW();
        if ( ilDateErrorEW != null )
        {
            ilDateErrorEW.getEffect().setFired( false );
            effectsModel.setIlDateErrorEW( ilDateErrorEW );

        }

        EffectsModel.EffectWrapper ilfDateErrorEW = effectsModel.getIlfDateErrorEW();
        if ( ilfDateErrorEW != null )
        {
            ilfDateErrorEW.getEffect().setFired( false );
            effectsModel.setIlfDateErrorEW( ilfDateErrorEW );

        }

        EffectsModel.EffectWrapper spsDateErrorEW = effectsModel.getSpsDateErrorEW();
        if ( spsDateErrorEW != null )
        {
            spsDateErrorEW.getEffect().setFired( false );
            effectsModel.setSpsDateErrorEW( spsDateErrorEW );

        }

        EffectsModel.EffectWrapper spfDateErrorEW = effectsModel.getSpfDateErrorEW();
        if ( spfDateErrorEW != null )
        {
            spfDateErrorEW.getEffect().setFired( false );
            effectsModel.setSpfDateErrorEW( spfDateErrorEW );

        }

    }


    /**
     * highligh effect for files count
     * 
     ********PLEASE NOTE*******
     * You have to create separate effect wrappers attaching to the UIcomponents respectively. 
     * If you attache the same Effect wrapper to the different UI components, 
     * it only takes effect on the first one. 
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     * */
    public void filesCountHighlight( ActionEvent event )
    {

        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        EffectsModel.EffectWrapper fileCountInfoEFWrapper = effectsModel.getFileCountInfoEFWrapper();

        if ( fileCountInfoEFWrapper != null )
        {
            fileCountInfoEFWrapper.getEffect().setFired( false );
            effectsModel.setFileCountInfoEFWrapper( fileCountInfoEFWrapper );

        }

    }


    /**
     * pulse effect for from date to date error
     * 
     ********PLEASE NOTE*******
     * You have to create separate effect wrappers attaching to the UIcomponents respectively. 
     * If you attache the same Effect wrapper to the different UI components, 
     * it only takes effect on the first one. 
     * 
     * @param ValueChangeEvent - a value change action event
     * 
     * */
    public void pulseFromToDateError( ActionEvent event )
    {

        // get the callers session effects data model
        EffectsModel effectsModel = (EffectsModel) FacesUtils.getManagedBean( BaseBean.EFFECTS_MODEL );

        EffectsModel.EffectWrapper fromToDateErrorEW = effectsModel.getFromToDateErrorEW();

        if ( fromToDateErrorEW != null )
        {
            fromToDateErrorEW.getEffect().setFired( false );
            effectsModel.setFromToDateErrorEW( fromToDateErrorEW );

        }

    }

}
