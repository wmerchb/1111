
package ca.medavie.subrogation.effect;

import com.icesoft.faces.context.effects.*;

import java.util.HashMap;
import java.util.Collection;

/**
 *Class for holding the the effect information (wrappers)
 */

public class EffectsModel
{

    // current effect that consists of a label and the actual Effect.
    private EffectWrapper currentEffectWrapper;

    /**
     * summary effect wrapper
     * */
    private EffectWrapper summaryTitleEffectWrapper;

    /**
     * slide up effect for summary information panel
     * */
    private EffectWrapper summaryInfoPanelSlideUpWrapper;

    /**
     * slide down effect for summary information panel
     * */
    private EffectWrapper summaryInfoPanelSlideDownWrapper;

    /**
     * slide up effect for summary information panel
     * */
    private EffectWrapper recoveryInfoPanelSlideUpWrapper;

    /**
     * slide down effect for summary information panel
     * */
    private EffectWrapper recoveryInfoPanelSlideDownWrapper;

    /**
     * slide up effect for file status information panel
     * */
    private EffectWrapper fileStatusPanelSlideUpWrapper;

    /**
     * slide down effect for file status information panel
     * */
    private EffectWrapper fileStatusPanelSlideDownWrapper;

    /**
     * appear effect for subscriber search result
     * */
    private EffectWrapper searchResultAppearWrapper;

    /**
     * Appear effect for new icon
     * */
    private EffectWrapper newIconAppearWrapper;

    /**
     * Fade effect for new icon
     * */
    private EffectWrapper newIconFadeWrapper;

    /**
     *High light for whole panel when doing the reset action
     * */

    /**
     * error message pulse
     * **/
    private EffectWrapper wholePanelHighlightWrapper;

    private EffectWrapper wholePanelFadeAndAppearWrapper;

    private EffectWrapper summaryAmountErrorEW;

    private EffectWrapper summaryDateErrorEW;

    private EffectWrapper birthDateErrorEW;

    private EffectWrapper fileOpenErrorEW;

    private EffectWrapper accidentDateErrorEW;

    private EffectWrapper recoveryDateErrorEW;

    private EffectWrapper recoveryAmountErrorEW;

    private EffectWrapper lawyerFeeErrorEW;

    private EffectWrapper netRecoveryErrorEW;

    private EffectWrapper wlsDateErrorEW;

    private EffectWrapper llsDateErrorEW;

    private EffectWrapper misDateErrorEW;

    private EffectWrapper wlfDateErrorEW;

    private EffectWrapper llfDateErrorEW;

    private EffectWrapper mifDateErrorEW;

    private EffectWrapper ilDateErrorEW;

    private EffectWrapper ilfDateErrorEW;

    private EffectWrapper spsDateErrorEW;

    private EffectWrapper spfDateErrorEW;
    /***********************************/

    /**file count highlight effect*/
    private EffectWrapper fileCountInfoEFWrapper;

    /**Effect wrapper for comment panel slide up*/
    private EffectWrapper commentPanelSlideUpWrapper;

    /**Effect wrapper for comment panel slide down*/
    private EffectWrapper commentPanelSlideDownWrapper;

    /**Effect wrapper for error message for from to date*/
    private EffectWrapper fromToDateErrorEW;

    // map of possible effects.
    private HashMap<String, EffectWrapper> effects;

    private String fade = "effectFade";

    private String show = "effectAppear";

    private String slideUp = "effectSlideUp";

    private String slideDown = "effectSlideDown";

    private String highlight = "effectHighlight";

    private String pulsate = "effectPulsate";


    /**
     * Creates a new instance of the effects model.
     */
    public EffectsModel()
    {
        init();

    }


    /**
     * Initialising all the effects object.
     */
    protected void init()
    {
        // build a list of our know effects
        effects = new HashMap<String, EffectWrapper>( 11 );

        summaryTitleEffectWrapper = new EffectWrapper( "Summary title effect", new Highlight( "#DEE5EB" ) );

        SlideUp s3 = new SlideUp();
        s3.setDuration( 0.5f );
        summaryInfoPanelSlideUpWrapper = new EffectWrapper( "Summary_Info_Slide_Up", s3 );

        SlideDown s4 = new SlideDown();
        s4.setDuration( 0.5f );
        summaryInfoPanelSlideDownWrapper = new EffectWrapper( "Summary_Info_Slide_Down", s4 );

        SlideUp s5 = new SlideUp();
        s5.setDuration( 0.5f );
        recoveryInfoPanelSlideUpWrapper = new EffectWrapper( "Recover_Info_Slide_Up", s5 );

        SlideDown s6 = new SlideDown();
        s6.setDuration( 0.5f );
        recoveryInfoPanelSlideDownWrapper = new EffectWrapper( "Recovery_Info_Slide_Down", s6 );

        SlideUp s7 = new SlideUp();
        s7.setDuration( 0.5f );
        fileStatusPanelSlideUpWrapper = new EffectWrapper( "Recover_Info_Slide_Up", s7 );

        SlideDown s8 = new SlideDown();
        s8.setDuration( 0.5f );
        fileStatusPanelSlideDownWrapper = new EffectWrapper( "Recovery_Info_Slide_Down", s8 );

        SlideUp s9 = new SlideUp();
        s9.setDuration( 0.5f );
        commentPanelSlideUpWrapper = new EffectWrapper( "comment panel slide up", s9 );

        SlideDown s10 = new SlideDown();
        s10.setDuration( 0.5f );
        commentPanelSlideDownWrapper = new EffectWrapper( "comment panel slide down", s10 );

        Appear ap1 = new Appear();
        ap1.setDuration( 1.0f );
        searchResultAppearWrapper = new EffectWrapper( "Search Result Appear", ap1 );

        Appear ap2 = new Appear();
        ap2.setDuration( 1.0f );
        newIconAppearWrapper = new EffectWrapper( "New Icon Appear", ap2 );

        Fade fd2 = new Fade();
        fd2.setDuration( 1.0f );
        newIconFadeWrapper = new EffectWrapper( "New Icon Fade", fd2 );

        Highlight h2 = new Highlight( "#DEE5EB" );
        wholePanelHighlightWrapper = new EffectWrapper( "Whole panel highlight", h2 );

        EffectQueue wholePanelFA = new EffectQueue( "wholePanelFA" );

        Fade wholeFade = new Fade();
        wholeFade.setDuration( 0.1f );

        Appear wholeAppear = new Appear();
        wholeAppear.setDuration( 0.5f );

        wholePanelFA.add( wholeFade );
        wholePanelFA.add( wholeAppear );
        wholePanelFadeAndAppearWrapper = new EffectWrapper( "Whole Panel Fade and Appear", wholePanelFA );

        Pulsate p1 = new Pulsate();
        p1.setDuration( 1.0f );
        summaryAmountErrorEW = new EffectWrapper( "summary amount error", p1 );

        Pulsate p2 = new Pulsate();
        p2.setDuration( 1.0f );
        summaryDateErrorEW = new EffectWrapper( "summary date error", p2 );

        Pulsate p3 = new Pulsate();
        p3.setDuration( 1.0f );
        fileOpenErrorEW = new EffectWrapper( "file open date error", p3 );

        Pulsate p4 = new Pulsate();
        p4.setDuration( 1.0f );
        accidentDateErrorEW = new EffectWrapper( "accident date error", p4 );

        Pulsate p5 = new Pulsate();
        p5.setDuration( 1.0f );
        recoveryDateErrorEW = new EffectWrapper( "recovery date error", p5 );

        Pulsate p6 = new Pulsate();
        p6.setDuration( 1.0f );
        recoveryAmountErrorEW = new EffectWrapper( "recovery amount error", p6 );

        Pulsate p7 = new Pulsate();
        p7.setDuration( 1.0f );
        lawyerFeeErrorEW = new EffectWrapper( "lawyer fee error", p7 );

        Pulsate p8 = new Pulsate();
        p8.setDuration( 1.0f );
        netRecoveryErrorEW = new EffectWrapper( "net recovery error", p8 );

        Pulsate p9 = new Pulsate();
        p9.setDuration( 1.0f );
        wlsDateErrorEW = new EffectWrapper( "waiver letter sent date error", p9 );

        Pulsate p10 = new Pulsate();
        p10.setDuration( 1.0f );
        llsDateErrorEW = new EffectWrapper( "lawyer letter sent date error", p10 );

        Pulsate p11 = new Pulsate();
        p11.setDuration( 1.0f );
        misDateErrorEW = new EffectWrapper( "more information letter sent date error", p11 );

        Pulsate p12 = new Pulsate();
        p12.setDuration( 1.0f );
        wlfDateErrorEW = new EffectWrapper( "waiver letter follow up sent date error", p12 );

        Pulsate p13 = new Pulsate();
        p13.setDuration( 1.0f );
        llfDateErrorEW = new EffectWrapper( "lawyer letter follow up sent date error", p13 );

        Pulsate p14 = new Pulsate();
        p14.setDuration( 1.0f );
        mifDateErrorEW = new EffectWrapper( "more information follow up sent date error", p14 );

        Pulsate p15 = new Pulsate();
        p15.setDuration( 1.0f );
        ilDateErrorEW = new EffectWrapper( "insurance letter sent date error", p15 );

        Pulsate p16 = new Pulsate();
        p16.setDuration( 1.0f );
        ilfDateErrorEW = new EffectWrapper( "insurance letter follow up sent date error", p16 );

        Pulsate p17 = new Pulsate();
        p17.setDuration( 1.0f );
        spsDateErrorEW = new EffectWrapper( "summary provided sent date error", p17 );

        Pulsate p18 = new Pulsate();
        p18.setDuration( 1.0f );
        spfDateErrorEW = new EffectWrapper( "summary provided followup date error", p18 );

        Pulsate p19 = new Pulsate();
        p19.setDuration( 1.0f );
        fromToDateErrorEW = new EffectWrapper( "from date to date error", p19 );

        Pulsate p20 = new Pulsate();
        p20.setDuration( 1.0f );
        birthDateErrorEW = new EffectWrapper( "birth date error", p20 );

        Highlight h1 = new Highlight( "#DEE5EB" );
        h1.setDuration( 0.5f );
        fileCountInfoEFWrapper = new EffectWrapper( "files count highlight effect", h1 );

        effects.put( "effectHighlight", new EffectWrapper( "page.effect.highlight.title", new Highlight( "#DEE5EB" ) ) );
        // pulsate
        Pulsate pulsate = new Pulsate();
        pulsate.setDuration( 1.0f );
        effects.put( "effectPulsate", new EffectWrapper( "page.effect.pulsate.title", pulsate ) );

        effects.put( "effectSlideUp", new EffectWrapper( "page.effect.slide.title", new SlideUp() ) );
        effects.put( "effectSlideDown", new EffectWrapper( "page.effect.slide.title", new SlideDown() ) );

    }


    public EffectWrapper getCurrentEffectWrapper()
    {
        return currentEffectWrapper;
    }


    public void setCurrentEffectWrapper( EffectWrapper currentEffectWrapper )
    {
        this.currentEffectWrapper = currentEffectWrapper;
    }


    public HashMap<String, EffectWrapper> getEffects()
    {
        return effects;
    }


    /**
     * Gets a list of EffectWrapper objects.
     * 
     * @return collection of EffectWrapper key
     */
    public Collection<String> getEffectKeys()
    {
        return effects.keySet();
    }


    public String getShow()
    {
        return show;
    }


    public void setShow( String show )
    {
        this.show = show;
    }


    public String getFade()
    {
        return fade;
    }


    public void setFade( String fade )
    {
        this.fade = fade;
    }


    public String getSlideDown()
    {
        return slideDown;
    }


    public void setSlideDown( String slideDown )
    {
        this.slideDown = slideDown;
    }


    public String getSlideUp()
    {
        return slideUp;
    }


    public void setSlideUp( String slideUp )
    {
        this.slideUp = slideUp;
    }


    /**
     * @return the highlight
     */
    public String getHighlight()
    {
        return highlight;
    }


    /**
     * @param highlight the highlight to set
     */
    public void setHighlight( String highlight )
    {
        this.highlight = highlight;
    }


    /**
     * @return the pulsate
     */
    public String getPulsate()
    {
        return pulsate;
    }


    /**
     * @param pulsate the pulsate to set
     */
    public void setPulsate( String pulsate )
    {
        this.pulsate = pulsate;
    }


    /**
     * @return the summarySlideUpWrapper
     */
    public EffectWrapper getSummaryInfoPanelSlideUpWrapper()
    {
        return summaryInfoPanelSlideUpWrapper;
    }


    /**
     * @param summarySlideUpWrapper the summarySlideUpWrapper to set
     */
    public void setSummaryInfoPanelSlideUpWrapper( EffectWrapper summarySlideUpWrapper )
    {
        this.summaryInfoPanelSlideUpWrapper = summarySlideUpWrapper;
    }


    /**
     * @return the summaryTitleEffectWrapper
     */
    public EffectWrapper getSummaryTitleEffectWrapper()
    {

        return summaryTitleEffectWrapper;
    }


    /**
     * @param summaryTitleEffectWrapper the summaryTitleEffectWrapper to set
     */
    public void setSummaryTitleEffectWrapper( EffectWrapper summaryTitleEffectWrapper )
    {
        this.summaryTitleEffectWrapper = summaryTitleEffectWrapper;
    }


    /**
     * @return the summaryInfoPanelSlideDownWrapper
     */
    public EffectWrapper getSummaryInfoPanelSlideDownWrapper()
    {
        return summaryInfoPanelSlideDownWrapper;
    }


    /**
     * @param summaryInfoPanelSlideDownWrapper the summaryInfoPanelSlideDownWrapper to set
     */
    public void setSummaryInfoPanelSlideDownWrapper( EffectWrapper summaryInfoPanelSlideDownWrapper )
    {
        this.summaryInfoPanelSlideDownWrapper = summaryInfoPanelSlideDownWrapper;
    }


    /**
     * @return the recoveryInfoPanelSlideUpWrapper
     */
    public EffectWrapper getRecoveryInfoPanelSlideUpWrapper()
    {
        return recoveryInfoPanelSlideUpWrapper;
    }


    /**
     * @param recoveryInfoPanelSlideUpWrapper the recoveryInfoPanelSlideUpWrapper to set
     */
    public void setRecoveryInfoPanelSlideUpWrapper( EffectWrapper recoveryInfoPanelSlideUpWrapper )
    {
        this.recoveryInfoPanelSlideUpWrapper = recoveryInfoPanelSlideUpWrapper;
    }


    /**
     * @return the recoveryInfoPanelSlideDownWrapper
     */
    public EffectWrapper getRecoveryInfoPanelSlideDownWrapper()
    {
        return recoveryInfoPanelSlideDownWrapper;
    }


    /**
     * @param recoveryInfoPanelSlideDownWrapper the recoveryInfoPanelSlideDownWrapper to set
     */
    public void setRecoveryInfoPanelSlideDownWrapper( EffectWrapper recoveryInfoPanelSlideDownWrapper )
    {
        this.recoveryInfoPanelSlideDownWrapper = recoveryInfoPanelSlideDownWrapper;
    }


    /**
     * @return the fileStatusPanelSlideUpWrapper
     */
    public EffectWrapper getFileStatusPanelSlideUpWrapper()
    {
        return fileStatusPanelSlideUpWrapper;
    }


    /**
     * @param fileStatusPanelSlideUpWrapper the fileStatusPanelSlideUpWrapper to set
     */
    public void setFileStatusPanelSlideUpWrapper( EffectWrapper fileStatusPanelSlideUpWrapper )
    {
        this.fileStatusPanelSlideUpWrapper = fileStatusPanelSlideUpWrapper;
    }


    /**
     * @return the fileStatusPanelSlideDownWrapper
     */
    public EffectWrapper getFileStatusPanelSlideDownWrapper()
    {
        return fileStatusPanelSlideDownWrapper;
    }


    /**
     * @param fileStatusPanelSlideDownWrapper the fileStatusPanelSlideDownWrapper to set
     */
    public void setFileStatusPanelSlideDownWrapper( EffectWrapper fileStatusPanelSlideDownWrapper )
    {
        this.fileStatusPanelSlideDownWrapper = fileStatusPanelSlideDownWrapper;
    }


    /**
     * @return the searchResultAppearWrapper
     */
    public EffectWrapper getSearchResultAppearWrapper()
    {
        return searchResultAppearWrapper;
    }


    /**
     * @param searchResultAppearWrapper the searchResultAppearWrapper to set
     */
    public void setSearchResultAppearWrapper( EffectWrapper searchResultAppearWrapper )
    {
        this.searchResultAppearWrapper = searchResultAppearWrapper;
    }


    /**
     * @return the newIconAppearWrapper
     */
    public EffectWrapper getNewIconAppearWrapper()
    {
        return newIconAppearWrapper;
    }


    /**
     * @param newIconAppearWrapper the newIconAppearWrapper to set
     */
    public void setNewIconAppearWrapper( EffectWrapper newIconAppearWrapper )
    {
        this.newIconAppearWrapper = newIconAppearWrapper;
    }


    /**
     * @return the newIconFadeWrapper
     */
    public EffectWrapper getNewIconFadeWrapper()
    {
        return newIconFadeWrapper;
    }


    /**
     * @param newIconFadeWrapper the newIconFadeWrapper to set
     */
    public void setNewIconFadeWrapper( EffectWrapper newIconFadeWrapper )
    {
        this.newIconFadeWrapper = newIconFadeWrapper;
    }


    /**
     * @return the wholePanelHighlightWrapper
     */
    public EffectWrapper getWholePanelHighlightWrapper()
    {
        return wholePanelHighlightWrapper;
    }


    /**
     * @param wholePanelHighlightWrapper the wholePanelHighlightWrapper to set
     */
    public void setWholePanelHighlightWrapper( EffectWrapper wholePanelHighlightWrapper )
    {
        this.wholePanelHighlightWrapper = wholePanelHighlightWrapper;
    }


    /**
     * @return the wholePanelFadeAndAppearWrapper
     */
    public EffectWrapper getWholePanelFadeAndAppearWrapper()
    {
        return wholePanelFadeAndAppearWrapper;
    }


    /**
     * @param wholePanelFadeAndAppearWrapper the wholePanleFadeAndAppearWrapper to set
     */
    public void setWholePanelFadeAndAppearWrapper( EffectWrapper wholePanelFadeAndAppearWrapper )
    {
        this.wholePanelFadeAndAppearWrapper = wholePanelFadeAndAppearWrapper;
    }


    /**
     * @return the summaryAmountErrorEW
     */
    public EffectWrapper getSummaryAmountErrorEW()
    {
        return summaryAmountErrorEW;
    }


    /**
     * @param summaryAmountErrorEW the summaryAmountErrorEW to set
     */
    public void setSummaryAmountErrorEW( EffectWrapper summaryAmountErrorEW )
    {
        this.summaryAmountErrorEW = summaryAmountErrorEW;
    }


    /**
     * @return the summaryDateErrorEW
     */
    public EffectWrapper getSummaryDateErrorEW()
    {
        return summaryDateErrorEW;
    }


    /**
     * @param summaryDateErrorEW the summaryDateErrorEW to set
     */
    public void setSummaryDateErrorEW( EffectWrapper summaryDateErrorEW )
    {
        this.summaryDateErrorEW = summaryDateErrorEW;
    }


    /**
     * @return the birthDateErrorEW
     */
    public EffectWrapper getBirthDateErrorEW()
    {
        return birthDateErrorEW;
    }


    /**
     * @param birthDateErrorEW the birthDateErrorEW to set
     */
    public void setBirthDateErrorEW( EffectWrapper birthDateErrorEW )
    {
        this.birthDateErrorEW = birthDateErrorEW;
    }


    /**
     * @return the fileOpenErrorEW
     */
    public EffectWrapper getFileOpenErrorEW()
    {
        return fileOpenErrorEW;
    }


    /**
     * @param fileOpenErrorEW the fileOpenErrorEW to set
     */
    public void setFileOpenErrorEW( EffectWrapper fileOpenErrorEW )
    {
        this.fileOpenErrorEW = fileOpenErrorEW;
    }


    /**
     * @return the accidentDateErrorEW
     */
    public EffectWrapper getAccidentDateErrorEW()
    {
        return accidentDateErrorEW;
    }


    /**
     * @param accidentDateErrorEW the accidentDateErrorEW to set
     */
    public void setAccidentDateErrorEW( EffectWrapper accidentDateErrorEW )
    {
        this.accidentDateErrorEW = accidentDateErrorEW;
    }


    /**
     * @return the recoveryDateErrorEW
     */
    public EffectWrapper getRecoveryDateErrorEW()
    {
        return recoveryDateErrorEW;
    }


    /**
     * @param recoveryDateErrorEW the recoveryDateErrorEW to set
     */
    public void setRecoveryDateErrorEW( EffectWrapper recoveryDateErrorEW )
    {
        this.recoveryDateErrorEW = recoveryDateErrorEW;
    }


    /**
     * @return the recoveryAmountErrorEW
     */
    public EffectWrapper getRecoveryAmountErrorEW()
    {
        return recoveryAmountErrorEW;
    }


    /**
     * @param recoveryAmountErrorEW the recoveryAmountErrorEW to set
     */
    public void setRecoveryAmountErrorEW( EffectWrapper recoveryAmountErrorEW )
    {
        this.recoveryAmountErrorEW = recoveryAmountErrorEW;
    }


    /**
     * @return the lawyerFeeErrorEW
     */
    public EffectWrapper getLawyerFeeErrorEW()
    {
        return lawyerFeeErrorEW;
    }


    /**
     * @param lawyerFeeErrorEW the lawyerFeeErrorEW to set
     */
    public void setLawyerFeeErrorEW( EffectWrapper lawyerFeeErrorEW )
    {
        this.lawyerFeeErrorEW = lawyerFeeErrorEW;
    }


    /**
     * @return the netRecoveryErrorEW
     */
    public EffectWrapper getNetRecoveryErrorEW()
    {
        return netRecoveryErrorEW;
    }


    /**
     * @param netRecoveryErrorEW the netRecoveryErrorEW to set
     */
    public void setNetRecoveryErrorEW( EffectWrapper netRecoveryErrorEW )
    {
        this.netRecoveryErrorEW = netRecoveryErrorEW;
    }


    /**
     * @return the wlsDateErrorEW
     */
    public EffectWrapper getWlsDateErrorEW()
    {
        return wlsDateErrorEW;
    }


    /**
     * @param wlsDateErrorEW the wlsDateErrorEW to set
     */
    public void setWlsDateErrorEW( EffectWrapper wlsDateErrorEW )
    {
        this.wlsDateErrorEW = wlsDateErrorEW;
    }


    /**
     * @return the llsDateErrorEW
     */
    public EffectWrapper getLlsDateErrorEW()
    {
        return llsDateErrorEW;
    }


    /**
     * @param llsDateErrorEW the llsDateErrorEW to set
     */
    public void setLlsDateErrorEW( EffectWrapper llsDateErrorEW )
    {
        this.llsDateErrorEW = llsDateErrorEW;
    }


    /**
     * @return the misDateErrorEW
     */
    public EffectWrapper getMisDateErrorEW()
    {
        return misDateErrorEW;
    }


    /**
     * @param misDateErrorEW the misDateErrorEW to set
     */
    public void setMisDateErrorEW( EffectWrapper misDateErrorEW )
    {
        this.misDateErrorEW = misDateErrorEW;
    }


    /**
     * @return the wlfDateErrorEW
     */
    public EffectWrapper getWlfDateErrorEW()
    {
        return wlfDateErrorEW;
    }


    /**
     * @param wlfDateErrorEW the wlfDateErrorEW to set
     */
    public void setWlfDateErrorEW( EffectWrapper wlfDateErrorEW )
    {
        this.wlfDateErrorEW = wlfDateErrorEW;
    }


    /**
     * @return the llfDateErrorEW
     */
    public EffectWrapper getLlfDateErrorEW()
    {
        return llfDateErrorEW;
    }


    /**
     * @param llfDateErrorEW the llfDateErrorEW to set
     */
    public void setLlfDateErrorEW( EffectWrapper llfDateErrorEW )
    {
        this.llfDateErrorEW = llfDateErrorEW;
    }


    /**
     * @return the mifDateErrorEW
     */
    public EffectWrapper getMifDateErrorEW()
    {
        return mifDateErrorEW;
    }


    /**
     * @param mifDateErrorEW the mifDateErrorEW to set
     */
    public void setMifDateErrorEW( EffectWrapper mifDateErrorEW )
    {
        this.mifDateErrorEW = mifDateErrorEW;
    }


    /**
     * @return the ilDateErrorEW
     */
    public EffectWrapper getIlDateErrorEW()
    {
        return ilDateErrorEW;
    }


    /**
     * @param ilDateErrorEW the ilDateErrorEW to set
     */
    public void setIlDateErrorEW( EffectWrapper ilDateErrorEW )
    {
        this.ilDateErrorEW = ilDateErrorEW;
    }


    /**
     * @return the ilfDateErrorEW
     */
    public EffectWrapper getIlfDateErrorEW()
    {
        return ilfDateErrorEW;
    }


    /**
     * @param ilfDateErrorEW the ilfDateErrorEW to set
     */
    public void setIlfDateErrorEW( EffectWrapper ilfDateErrorEW )
    {
        this.ilfDateErrorEW = ilfDateErrorEW;
    }


    /**
     * @return the spsDateErrorEW
     */
    public EffectWrapper getSpsDateErrorEW()
    {
        return spsDateErrorEW;
    }


    /**
     * @param spsDateErrorEW the spsDateErrorEW to set
     */
    public void setSpsDateErrorEW( EffectWrapper spsDateErrorEW )
    {
        this.spsDateErrorEW = spsDateErrorEW;
    }


    /**
     * @return the spfDateErrorEW
     */
    public EffectWrapper getSpfDateErrorEW()
    {
        return spfDateErrorEW;
    }


    /**
     * @param spfDateErrorEW the spfDateErrorEW to set
     */
    public void setSpfDateErrorEW( EffectWrapper spfDateErrorEW )
    {
        this.spfDateErrorEW = spfDateErrorEW;
    }


    /**
     * @return the commentPanelSlideUpWrapper
     */
    public EffectWrapper getCommentPanelSlideUpWrapper()
    {
        return commentPanelSlideUpWrapper;
    }


    /**
     * @param commentPanelSlideUpWrapper the commentPanelSlideUpWrapper to set
     */
    public void setCommentPanelSlideUpWrapper( EffectWrapper commentPanelSlideUpWrapper )
    {
        this.commentPanelSlideUpWrapper = commentPanelSlideUpWrapper;
    }


    /**
     * @return the commentPanelSlideDownWrapper
     */
    public EffectWrapper getCommentPanelSlideDownWrapper()
    {
        return commentPanelSlideDownWrapper;
    }


    /**
     * @param commentPanelSlideDownWrapper the commentPanelSlideDownWrapper to set
     */
    public void setCommentPanelSlideDownWrapper( EffectWrapper commentPanelSlideDownWrapper )
    {
        this.commentPanelSlideDownWrapper = commentPanelSlideDownWrapper;
    }


    /**
     * @return the fromToDateErrorEW
     */
    public EffectWrapper getFromToDateErrorEW()
    {
        return fromToDateErrorEW;
    }


    /**
     * @param fromToDateErrorEW the fromToDateErrorEW to set
     */
    public void setFromToDateErrorEW( EffectWrapper fromToDateErrorEW )
    {
        this.fromToDateErrorEW = fromToDateErrorEW;
    }


    /**
     * @return the fileCountInfoEFWrapper
     */
    public EffectWrapper getFileCountInfoEFWrapper()
    {
        return fileCountInfoEFWrapper;
    }


    /**
     * @param fileCountInfoEFWrapper the fileCountInfoEFWrapper to set
     */
    public void setFileCountInfoEFWrapper( EffectWrapper fileCountInfoEFWrapper )
    {
        this.fileCountInfoEFWrapper = fileCountInfoEFWrapper;
    }


    /**
     * Wrapper class to make it easy to display the different Effects with a
     * discriptive label.
     */
    public class EffectWrapper
    {
        private Effect effect;
        private String title;


        public EffectWrapper( String title, Effect effect )
        {
            this.effect = effect;
            this.effect.setFired( true );
            this.title = title;
        }


        public Effect getEffect()
        {
            return effect;
        }


        public void setEffect( Effect effect )
        {
            this.effect = effect;
        }


        /**
         * @return the title
         */
        public String getTitle()
        {
            return title;
        }


        /**
         * @param title the title to set
         */
        public void setTitle( String title )
        {
            this.title = title;
        }

    }
}