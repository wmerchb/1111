
package ca.medavie.subrogation.ui;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;

import ca.bluecode.ui.model.ModelBase;
import ca.bluecross.atl.utilization.blo.Message;
import ca.medavie.subrogation.SubrogationService;
import ca.medavie.subrogation.effect.FacesUtils;
import ca.medavie.subrogation.ui.controller.SubrogationController;
import ca.medavie.subrogation.ui.model.FilesCountModel;

/**
 * Base bean manager class for the Subrogation web application.
 */
public class BaseBean extends ModelBase
{

    private static final Logger logger = Logger.getLogger( BaseBean.class );

    protected SubrogationController subrogationController;

    private static final String NAV_WELCOME_PAGE = "welcomePage";

    // component example related beans
    /**
     * Effects Model bean name property
     */
    protected static final String EFFECTS_MODEL = "effectsModel";

    /**
     * Effects Controller bean name property
     */
    protected static final String EFFECTS_CONTROLLER = "effectsController";

    /**
     * Subrogation Model bean name property
     */
    protected static final String SUBROGATION_MODEL = "subrogationModel";

    /**
     * Subrogation Controller bean name property
     */
    protected static final String SUBROGATION_CONTROLLER = "subrogationController";

    /**
     * File Count Controller bean name property
     */
    protected static final String FILE_COUNT_CONTROLLER = "filesCountController";

    /**
     *File Count Model bean name property
     */
    protected static final String FILE_COUNT_MODEL = "filesCountModel";

    /**
     * A PopUpBean used to manage the pop-up window
     */
    private static PopUpBean popUpBean = PopUpBean.getInstance();

    /**
     * A MessageFactory used to map service message to web application message
     * bundles
     */
    protected static MessageFactory messageFactory = MessageFactory.getInstance();

    /**
     * DIN number pattern xxxxxxxxxxx
     */
    private static final String DIN_NUM = "[0-9]{11}";

    protected TimeZone timeZone = TimeZone.getDefault();

    /**
     * SubrogationService
     */
    protected SubrogationService subrogationService;


    public BaseBean()
    {

    }


    /**
     * Iterates through all the elements associated with the passed html object.
     * The elements and the parent objects and then set to the default values.
     * 
     * @param obj
     *            - HTML Object
     * @param params
     *            - list of the fields that are not to be cleared
     */
    public void clearSubmittedValues( Object obj, List<String> params )
    {
        /**
         * Checks that the object is not a UIComponent
         */
        if ( ! ( obj instanceof UIComponent ) )
        {
            return;
        }

        /**
         * Regressively calls this method for each component associated with the
         * passed object.
         */
        Iterator<UIComponent> chld = ( (UIComponent) obj ).getFacetsAndChildren();
        while ( chld.hasNext() )
        {
            clearSubmittedValues( chld.next(), params );
        }

        /**
         * Sets all objects that take user input to there default values
         */
        if ( obj instanceof UIInput )
        {

            UIInput input = (UIInput) obj;
            /**
             * Values within the parameter List cannot be edited by the user
             * therefore should not be cleared
             */
            if ( params == null || !params.contains( input.getId() ) )
            {
                input.setSubmittedValue( null );
                input.setValue( "" );
                input.setLocalValueSet( false );
            }
        }
    }


    public static class CurrencyConverter implements Converter
    {

        /**
         * Convert the specified string value, which is associated with the
         * specified UIComponent, into a BigDecimal object that is appropriate
         * for being stored during the Apply Request Values phase of the request
         * processing lifecycle.
         * 
         * @param context
         *            - FacesContext for the request being processed
         * @param component
         *            - UIComponent with which this model object value is
         *            associated
         * @param value
         *            - String value to be converted (may be null)
         * @return null if the value to convert is null, otherwise the result of
         *         the conversion
         * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext,
         *      javax.faces.component.UIComponent, java.lang.String)
         */
        public Object getAsObject( FacesContext context, UIComponent component, String value )
        {
            BigDecimal bigDecimal = null;
            boolean isNegative = false;
            try
            {
                /**
                 * negative number
                 */
                if ( value.indexOf( '(' ) != -1 )
                {
                    value = value.substring( value.indexOf( '(' ) + 1, value.indexOf( ')' ) );
                    isNegative = true;
                }

                /**
                 * positive in French currency format
                 */
                if ( value.indexOf( '$' ) != -1 && value.indexOf( '$' ) == value.length() - 1 )
                {
                    value = value.substring( 0, value.indexOf( '$' ) );
                }
                /**
                 * positive in English currency format
                 */
                else if ( value.indexOf( '$' ) != -1 && value.indexOf( '$' ) != value.length() )
                {
                    value = value.substring( value.indexOf( '$' ) + 1 );
                }
                /**
                 * has French decimals format
                 */
                if ( value.indexOf( ',' ) != -1 )
                {
                    value = value.replace( ',', '.' );
                }
                if ( isNegative )
                {
                    value = "-".concat( value );
                }

                logger.debug( value );
                if ( value.length() > 0 )
                {
                    bigDecimal = new BigDecimal( value.trim() );
                }
            }
            catch ( Exception e )
            {
                logger.error( e.getMessage() );
                Object[] params =
                { component.getAttributes().get( "name" ) };
                handleParameterizedMessages( "INVALID_INPUT", params, FacesMessage.SEVERITY_ERROR );
                return ( (UIInput) component ).getValue();
            }
            return bigDecimal;
        }


        /**
         * Convert the specified object value, which is associated with the
         * specified UIComponent, into a String object that is appropriate for
         * being displayed to the user.
         * 
         * @param context
         *            - FacesContext for the request being processed
         * @param component
         *            - UIComponent with which this model object value is
         *            associated
         * @param value
         *            - Object value to be converted (may be null)
         * @return null if the value to convert is null, otherwise the result of
         *         the conversion
         * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext,
         *      javax.faces.component.UIComponent, java.lang.Object)
         */
        public String getAsString( FacesContext context, UIComponent component, Object value )
        {
            if ( ! ( value instanceof BigDecimal ) )
            {
                return null;
            }

            if ( value.toString().length() <= 0 )
            {
                return null;
            }

            NumberFormat numberFormat = NumberFormat.getCurrencyInstance(
                getLocale().getLanguage().equalsIgnoreCase( "en" ) ? Locale.CANADA : Locale.CANADA_FRENCH );

            double doublePayment = ( (BigDecimal) value ).doubleValue();
            return numberFormat.format( doublePayment );
        }
    }


    /**
     * An extension of javax.faces.convert.BigDecimalConverter, this class
     * catches the exceptions thrown by parent and displays appropriate
     * conversion error messages.
     */
    public static class BigDecimalConverter extends javax.faces.convert.BigDecimalConverter
    {

        /**
         * Convert the specified string value, which is associated with the
         * specified UIComponent, into a BigDecimal object that is appropriate
         * for being stored during the Apply Request Values phase of the request
         * processing lifecycle.
         * 
         * @param context
         *            - FacesContext for the request being processed
         * @param component
         *            - UIComponent with which this model object value is
         *            associated
         * @param value
         *            - String value to be converted (may be null)
         * @return null if the value to convert is null, otherwise the result of
         *         the conversion
         */
        @Override
        public Object getAsObject( FacesContext context, UIComponent component, String value )
        {
            BigDecimal bigDecimal = null;
            try
            {
                bigDecimal = (BigDecimal) super.getAsObject( context, component, value );
            }
            catch ( Exception e )
            {
                logger.error( e.getMessage() );
                Object[] params =
                { component.getAttributes().get( "name" ) };
                handleParameterizedMessages( "INVALID_INPUT", params, FacesMessage.SEVERITY_ERROR );
                return ( (UIInput) component ).getValue();
            }

            return bigDecimal;
        }

    }


    /**
     * An extension of javax.faces.convert.DateTimeConverter, this class
     * catches the exceptions thrown by parent and displays appropriate
     * conversion error messages.
     */
    public static class DateConverter extends javax.faces.convert.DateTimeConverter
    {

        /**
         * Convert the specified string value, which is associated with the
         * specified UIComponent, into a BigDecimal object that is appropriate
         * for being stored during the Apply Request Values phase of the request
         * processing lifecycle.
         * 
         * @param context
         *            - FacesContext for the request being processed
         * @param component
         *            - UIComponent with which this model object value is
         *            associated
         * @param value
         *            - String value to be converted (may be null)
         * @return null if the value to convert is null, otherwise the result of
         *         the conversion
         */
        @Override
        public Object getAsObject( FacesContext context, UIComponent component, String value )
        {
            Date date = null;

            try
            {
                super.setPattern( "yyyy/MM/dd" );
                date = (Date) super.getAsObject( context, component, value );
                logger.debug( "date pattern is " + getPattern() );
                return date;
            }
            catch ( Exception e )
            {
                logger.error( e.getMessage() );
                Object[] params =
                { component.getAttributes().get( "attribute" ) };
                handleParameterizedMessages( "INVALID_DATE_INPUT", params, FacesMessage.SEVERITY_ERROR );
                return ( (UIInput) component ).getValue();
            }

        }

    }


    /**
     * @see BaseBean#handleParameterizedMessages(String, Object[], Severity)
     * 
     * @param key
     *            - resource bundle key
     * @param severity
     *            - the severity of the message
     */
    public void handleMessages( Object key, Severity severity )
    {
        if ( key instanceof String )
        {
            handleParameterizedMessages( (String) key, null, severity );
        }
        if ( key instanceof Collection )
        {
            handleParameterizedMessages( (Collection<Message>) key, null, severity );
        }
    }


    /**
     * Gets from the message from the resource bundle associated with the key
     * and populates the parameter place-holders with parameter String
     * parameter. The message is added to the FacesContext to be later
     * displayed.
     * 
     * @param key
     *            - resource bundle key
     * @param params
     *            - parameters for message place-holders
     * @param severity
     *            - the severity of the message
     */
    public static void handleParameterizedMessages( String key, Object[] params, Severity severity )
    {
        logger.debug( "handleParameterizedMessages executed" );

        FacesContext context = FacesContext.getCurrentInstance();
        String msg = messageFactory.getMessageStringForKey( key, getLocale() );
        MessageFormat formatter = new MessageFormat( msg, getLocale() );
        String message = formatter.format( params );
        context.addMessage( null, new FacesMessage( severity, message, null ) );
    }


    /**
     * @see BaseBean#handleParameterizedMessages(String, Object[], Severity)
     * 
     * @param keys
     *            - Collection of resource bundle keys
     * @param params
     *            - parameters for message place-holders
     * @param severity
     *            - the severity of the message
     */
    public void handleParameterizedMessages( Collection<Message> keys, Object[] params, Severity severity )
    {

        for ( Message eachMessage : keys )
        {
            handleParameterizedMessages( eachMessage.getMessageCode(), params, severity );
        }

    }


    /**
     * 
     * 
     * @param event
     *            - ActionEvent thrown when button pressed
     */
    public String refreshAndGoBack()
    {

        // reset file count bean
        FilesCountModel aFilesCountModel = (FilesCountModel) FacesUtils.getManagedBean( BaseBean.FILE_COUNT_MODEL );
        aFilesCountModel.reset();

        return NAV_WELCOME_PAGE;
    }


    /**
     * Determines if the string value of a UIComponent is a valid din number,
     * i.e. [0-9]{11}
     * 
     * 
     * @param value
     * @return true if valid din, i.e. [0-9]{11} <br>
     *         false otherwise
     */
    protected boolean hasValidDin( Object value )
    {
        Pattern mask = Pattern.compile( DIN_NUM );

        if ( value != null )
        {

            /**
             * Retrieve string value of the field
             */
            String dinNum = (String) value;

            /**
             * Checks to ensure that the field's value is a din number
             */
            Matcher matcher = mask.matcher( dinNum );

            return matcher.matches();
        }
        return false;
    }


    protected static Locale getLocale()
    {
        return FacesContext.getCurrentInstance().getViewRoot().getLocale();
    }


    /**
     * @return Returns the popUpBean.
     */
    public PopUpBean getPopUpBean()
    {
        return popUpBean;
    }


    /**
     * @return the timeZone
     */
    public TimeZone getTimeZone()
    {
        return timeZone;
    }


    /**
     * @param timeZone the timeZone to set
     */
    public void setTimeZone( TimeZone timeZone )
    {
        this.timeZone = timeZone;
    }


    public SubrogationController getSubrogationController()
    {
        return subrogationController;
    }


    public void setSubrogationController( SubrogationController subrogationController )
    {
        this.subrogationController = subrogationController;
    }


    /**
     * @return the subrogationService
     */
    public SubrogationService getSubrogationService()
    {
        return subrogationService;
    }


    /**
     * @param subrogationService the subrogationService to set
     */
    public void setSubrogationService( SubrogationService subrogationService )
    {
        this.subrogationService = subrogationService;
    }
}
