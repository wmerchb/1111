
package ca.medavie.subrogation.ui;

/**
 * Manages the web application's menu bar.
 */
public class MenuBean extends BaseBean
{

    private final static String SUBROGATION_MAINT_MSG_KEY = "ui_tab_search_update_subrogation";
    private final static String FILE_COUNT_MSG_KEY = "ui_tab_file_counts_dash_board";


    public String getLocalizedSubMaintValue()
    {

        String value = MessageFactory.getInstance().getMessageStringForKey( SUBROGATION_MAINT_MSG_KEY, this.getLocale() );

        return value;

    }


    public String getLocalizedFilesCountValue()
    {
        String value = MessageFactory.getInstance().getMessageStringForKey( FILE_COUNT_MSG_KEY, this.getLocale() );

        return value;
    }

}
