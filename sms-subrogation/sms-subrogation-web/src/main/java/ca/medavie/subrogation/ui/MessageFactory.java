
package ca.medavie.subrogation.ui;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import ca.bluecross.atl.utilization.blo.Message;
import ca.medavie.subrogation.common.SubrogationMessage;

/**
 * A MessageFactory used to map service message to web application message
 * bundles.
 * 
 */
public class MessageFactory
{

    private static final Logger logger = Logger.getLogger( MessageFactory.class );
    /**
     * The  message bundle location
     * 
     */
    private static final String MESSAGE_BUNDLE_SUBROGATION = "ca.medavie.subrogation.ui.subrogation";

    /**
     * The singleton message instance
     * 
     */
    private static MessageFactory instance = null;

    private static final String parameterPlaceHolder = "{?}";

    private static final String regExpParam = "\\{\\?\\}";

    private static final String unspecified = "\\[ unspecified \\]";

    /**
     * A map containing all messages
     * 
     */
    private Map<SubrogationMessage, String> messages;


    /**
     * Private constructor.
     */
    private MessageFactory()
    {
        init();
    }


    /**
     * Getter for singleton instance of MessageFactory.
     * 
     * @return the singleton instance of MessageFactory
     */
    public static MessageFactory getInstance()
    {
        if ( instance == null )
        {
            instance = new MessageFactory();
        }
        return instance;
    }


    /**
     * Gets the loaclized String message associated with the Message.
     * 
     * @param key - the Message key used to find the String associated with the
     *            Message
     * @param locale - the Locale
     * @return the String associated with the Message
     */
    public String getMessageString( Message key, Locale locale )
    {
        if ( key == null )
        {
            logger.error( "Key passed is null" );
            return "";
        }
        String bundleKey = messages.get( key );
        if ( bundleKey == null )
        {
            logger.error( " Message code [" + key.getMessageCode() + "] was not found in the MessageFactory HashMap" );
            logger.error( " Message Text: [" + key.getMessageText() + "]" );
            return key.getMessageText();
        }

        String message = getMessageStringForKey( bundleKey, locale );
        if ( message == null )
        {
            logger.error( "BundleKey [" + bundleKey + "] was not found in the messageBundle" );
            message = key.getMessageText();
        }

        return message;
    }


    /**
     * Gets from the message bundle the message String associated with the key
     * String parameter.
     * 
     * @param string - the resource bundle key
     * @param locale - the Locale
     */
    public String getMessageStringForKey( String bundleKey, Locale locale )
    {
        // Retrieve the message string from the bundle using the key
        String messageStr = getStringFromBundle( MESSAGE_BUNDLE_SUBROGATION, bundleKey, locale );
        if ( messageStr != null )
        {
            return messageStr;
        }

        logger.error( " The message string could not be found for key: " + bundleKey );

        return "";
    }


    /**
     * Gets the message String from the message bundle and replaces the placeholders with the values of the parameter Map.
     * 
     * @param string - the resource bundle key
     * @param locale - the Locale
     * @param parameters - parameters for the placeholders
     */
    public String getParameterizedMessageStringForKey( String bundleKey, Map<Long, String> parameters, Locale locale )
    {
        // Retrieve the message string from the bundle using the key

        String messageStr = getStringFromBundle( MESSAGE_BUNDLE_SUBROGATION, bundleKey, locale );
        if ( messageStr != null )
        {
            messageStr = replaceParameters( messageStr, parameters );
            return messageStr;
        }

        logger.error( " The message string could not be found for key: " + bundleKey );

        return "";

    }


    /**
     * Replaces the parameters {?} in a message string with values.
     * 
     * @param - text
     * @param - parameters
     * @return string without {?}
     */
    private String replaceParameters( String text, Map<Long, String> parameters )
    {
        int loc = 1;

        if ( text.indexOf( parameterPlaceHolder ) == -1 )
        {
            return text;
        }

        if ( parameters.isEmpty() )
        {
            text = text.replaceAll( regExpParam, unspecified );
        }

        while ( text.indexOf( parameterPlaceHolder ) != -1 )
        {
            String value = parameters.get( Long.valueOf( loc ) );
            if ( value == null || value.length() == 0 )
            {
                value = unspecified;
            }
            text = text.replaceFirst( regExpParam, value );
            loc++;
        }

        return text;
    }


    /**
     * Gets the message String from the message bundle.
     *
     * @param bundleName - resource bundle name
     * @param bundleKey - the properties key
     * @param locale - the Locale
     * @return the message String
     */
    private String getStringFromBundle( String bundleName, String bundleKey, Locale locale )
    {
        ResourceBundle bundle;
        String messageStr = null;
        try
        {
            bundle = ResourceBundle.getBundle( bundleName, locale );
            messageStr = (String) bundle.getObject( bundleKey );
        }
        catch ( MissingResourceException e )
        {
            logger.error( "Could not find key [" + bundleKey + "] in [" + bundleName + "]" );
            logger.error( e.getMessage() );
        }
        return messageStr;
    }


    /**
     * Initializes messages in map
     * 
     */
    private void init()
    {
        messages = new HashMap<SubrogationMessage, String>();

        messages.put( SubrogationMessage.messagestub, "" );
    }
}
