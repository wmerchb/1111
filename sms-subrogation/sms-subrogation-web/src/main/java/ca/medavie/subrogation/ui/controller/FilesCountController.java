
package ca.medavie.subrogation.ui.controller;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import org.apache.log4j.Logger;

import com.icesoft.faces.component.ext.HtmlOutputText;

import ca.medavie.subrogation.SubrogationService;
import ca.medavie.subrogation.effect.EffectsController;
import ca.medavie.subrogation.effect.FacesUtils;
import ca.medavie.subrogation.ui.BaseBean;
import ca.medavie.subrogation.ui.PopUpBean;
import ca.medavie.subrogation.ui.model.FilesCountModel;
import ca.medavie.subrogation.ui.security.SubrogationSecurityManager;
import ca.medavie.utilization.RequestResult;
import ca.medavie.utilization.common.DateRange;

/**
 *File Controller class holding all actions that could take place in the UI
 */
public class FilesCountController extends BaseBean
{

    private static final Logger logger = Logger.getLogger( FilesCountController.class );
    /**
     * FilesCountModel that was associated with this controller
     */
    private FilesCountModel filesCountModel = (FilesCountModel) FacesUtils.getManagedBean( BaseBean.FILE_COUNT_MODEL );

    /**
     * Navigation to the Welcome Page
     */
    private static final String WELCOME_PAGE = "welcomePage";

    /**
     * Services for the controller
     */
    private SubrogationService subrogationService;


    /**
     * Action bound to from date change listener
     *
     * @param event from the ice:selectInputDate component
     * 
     * may be we can make a comment method for date change event
     * 
     */
    public String fileCountsFromDateChangeListener( ValueChangeEvent event )
    {
        /**
         * If the user does not have access to this action, do nothing.
         */
        if ( ! ((SubrogationSecurityManager) getSecurityManager()).hasSubrogationAdminRole() )
        {
            logger.error( "The User does not have the permission one executing this method!" );
            return "";
        }

        if ( event.getComponent().getId().equals( FilesCountModel.UI_COM_FROM_DATE_ID ) )
        {

            Date fromDate = (Date) event.getNewValue();
            logger.debug( "fileCountsFromDateChangeListener is called and from date is " + fromDate );

            filesCountModel.setFromDate( fromDate );
            // if TO date is selected. do the search action 
            FacesMessage.Severity severity = FacesContext.getCurrentInstance().getMaximumSeverity();
            int severityLevelError = FacesMessage.SEVERITY_ERROR.getOrdinal();
            int severityLevelFatal = FacesMessage.SEVERITY_FATAL.getOrdinal();

            if ( severity == null
                || ( severity.getOrdinal() != severityLevelError && severity.getOrdinal() != severityLevelFatal ) )
            {
                logger.debug( "There is no error in this page, starting doing the action..." );

                Date from = filesCountModel.getFromDate();
                Date to = filesCountModel.getToDate();
                // only if both date range being set and from date before or equal to date the proceed.
                if ( from != null && to != null && from.compareTo( to ) <= 0 )
                {

                    DateRange dr = new DateRange( from, to );
                    RequestResult rr1 = this.subrogationService.findNumberOfFilesOpened( dr );
                    RequestResult rr2 = this.subrogationService.findNumberOfFilesClosed( dr );
                    RequestResult rr3 = this.subrogationService.findSumOfRecoveryAmount( dr );
                    RequestResult rr4 = this.subrogationService.findSumOfSummaryAmount( dr );

                    boolean isAllSucceeded = ( rr1 != null && rr2 != null && rr3 != null && rr4 != null )
                        && ( rr1.isSuccess() && rr2.isSuccess() && rr3.isSuccess() && rr4.isSuccess() );

                    logger.debug( "DateRange is " + dr.toString() );
                    logger.debug( "succeedeed in reqeust result is " + isAllSucceeded );

                    // If everything is OK
                    if ( isAllSucceeded )

                    {
                        Integer filesOpened = (Integer) ( (Collection<Integer>) rr1.getRequestResult() ).toArray()[0];
                        if ( filesOpened != null )
                        {
                            logger.debug( "filesOpened is " + filesOpened.intValue() );
                            filesCountModel.setFilesOpened( filesOpened.toString() );
                        }
                        else
                        {
                            logger.debug( "filesOpened is NOTHING" );
                            filesCountModel.setFilesOpened( "0" );
                        }

                        Integer filesClosed = (Integer) ( (Collection<Integer>) rr2.getRequestResult() ).toArray()[0];
                        if ( filesClosed != null )
                        {
                            logger.debug( "filesClosed is " + filesClosed.intValue() );
                            filesCountModel.setFilesClosed( filesClosed.toString() );
                        }
                        else
                        {
                            logger.debug( "filesClosed is NOTHING" );
                            filesCountModel.setFilesClosed( "0" );
                        }

                        BigDecimal recoveryAmount = (BigDecimal) ( (Collection<BigDecimal>) rr3.getRequestResult() ).toArray()[0];
                        if ( recoveryAmount != null )
                        {
                            logger.debug( "recoveryAmount is " + recoveryAmount.doubleValue() );
                            filesCountModel.setRecoveryAmount( recoveryAmount );
                        }
                        else
                        {
                            logger.debug( "recoveryAmount is NOTHING" );
                            filesCountModel.setRecoveryAmount( BigDecimal.ZERO );
                        }

                        BigDecimal summaryAmount = (BigDecimal) ( (Collection<BigDecimal>) rr4.getRequestResult() ).toArray()[0];
                        if ( summaryAmount != null )
                        {

                            logger.debug( "summaryAmount is " + summaryAmount.doubleValue() );
                            filesCountModel.setSummaryAmount( summaryAmount );
                        }
                        else
                        {

                            logger.debug( "summaryAmount is NOTHING" );
                            filesCountModel.setSummaryAmount( BigDecimal.ZERO );
                        }

                        doSetUIComponentValue();

                        // hide the message.
                        filesCountModel.setDateRangeEMRendered( false );

                        // do highlight the results
                        EffectsController effectsController = (EffectsController) FacesUtils.getManagedBean( BaseBean.EFFECTS_CONTROLLER );
                        effectsController.filesCountHighlight( null );

                    }

                    else
                    {
                        handleMessages( "SEARCH_FAILED", FacesMessage.SEVERITY_WARN );
                    }
                }
                // reset the field
                else
                {
                    filesCountModel.setFilesOpened( "0" );
                    filesCountModel.setFilesClosed( "0" );
                    filesCountModel.setRecoveryAmount( BigDecimal.ZERO );
                    filesCountModel.setSummaryAmount( BigDecimal.ZERO );
                    doSetUIComponentValue();

                    // only when both date being selected
                    if ( from != null && to != null )
                    {
                        // show the message.
                        filesCountModel.setDateRangeEMRendered( true );
                        // show the pulse effect
                        EffectsController effectsController = (EffectsController) FacesUtils.getManagedBean( BaseBean.EFFECTS_CONTROLLER );
                        effectsController.pulseFromToDateError( null );

                    }
                }
            }

        }
        return "";
    }


    /**
     * Action bound to to date change listener
     *
     * @param event from the ice:selectInputDate component
     * 
     * may be we can make a comment method for date change event
     * 
     */
    public String fileCountsToDateChangeListener( ValueChangeEvent event )
    {

        /**
         * If the user does not have access to this action, do nothing.
         */
        if ( ! ((SubrogationSecurityManager) getSecurityManager()).hasSubrogationAdminRole() )
        {
            logger.error( "The User does not have the permission one executing this method!" );
            return "";
        }

        if ( event.getComponent().getId().equals( FilesCountModel.UI_COM_TO_DATE_ID ) )
        {

            Date toDate = (Date) event.getNewValue();
            logger.debug( "fileCountsToDateChangeListener is called and from date is " + toDate );
            filesCountModel.setToDate( toDate );

            // if TO date is selected. do the search action 
            FacesMessage.Severity severity = FacesContext.getCurrentInstance().getMaximumSeverity();
            int severityLevelError = FacesMessage.SEVERITY_ERROR.getOrdinal();
            int severityLevelFatal = FacesMessage.SEVERITY_FATAL.getOrdinal();

            if ( severity == null
                || ( severity.getOrdinal() != severityLevelError && severity.getOrdinal() != severityLevelFatal ) )
            {
                logger.debug( "There is no error in this page starting doing the action" );

                Date from = filesCountModel.getFromDate();
                Date to = filesCountModel.getToDate();
                // only if both date range being set and from date before or equal to date the proceed.
                if ( from != null && to != null && from.compareTo( to ) <= 0 )
                {

                    DateRange dr = new DateRange( from, to );
                    RequestResult rr1 = this.subrogationService.findNumberOfFilesOpened( dr );
                    RequestResult rr2 = this.subrogationService.findNumberOfFilesClosed( dr );
                    RequestResult rr3 = this.subrogationService.findSumOfRecoveryAmount( dr );
                    RequestResult rr4 = this.subrogationService.findSumOfSummaryAmount( dr );

                    boolean isAllSucceeded = ( rr1 != null && rr2 != null && rr3 != null && rr4 != null )
                        && ( rr1.isSuccess() && rr2.isSuccess() && rr3.isSuccess() && rr4.isSuccess() );

                    logger.debug( "DateRange is " + dr.toString() );
                    logger.debug( "succeedeed in reqeust result is " + isAllSucceeded );

                    // If everything is OK
                    if ( isAllSucceeded )

                    {
                        Integer filesOpened = (Integer) ( (Collection<Integer>) rr1.getRequestResult() ).toArray()[0];
                        if ( filesOpened != null )
                        {
                            logger.debug( "filesOpened is " + filesOpened.intValue() );
                            filesCountModel.setFilesOpened( filesOpened.toString() );
                        }
                        else
                        {
                            logger.debug( "filesOpened is NOTHING" );
                            filesCountModel.setFilesOpened( "0" );
                        }

                        Integer filesClosed = (Integer) ( (Collection<Integer>) rr2.getRequestResult() ).toArray()[0];
                        if ( filesClosed != null )
                        {
                            logger.debug( "filesClosed is " + filesClosed.intValue() );
                            filesCountModel.setFilesClosed( filesClosed.toString() );
                        }
                        else
                        {
                            logger.debug( "filesClosed is NOTHING" );
                            filesCountModel.setFilesClosed( "0" );
                        }

                        BigDecimal recoveryAmount = (BigDecimal) ( (Collection<BigDecimal>) rr3.getRequestResult() ).toArray()[0];
                        if ( recoveryAmount != null && recoveryAmount != BigDecimal.ZERO )
                        {
                            logger.debug( "recoveryAmount is " + recoveryAmount.doubleValue() );
                            filesCountModel.setRecoveryAmount( recoveryAmount );
                        }
                        else
                        {
                            logger.debug( "recoveryAmount is NOTHING" );
                            filesCountModel.setRecoveryAmount( BigDecimal.ZERO );
                        }

                        BigDecimal summaryAmount = (BigDecimal) ( (Collection<BigDecimal>) rr4.getRequestResult() ).toArray()[0];
                        if ( summaryAmount != null )
                        {

                            logger.debug( "summaryAmount is " + summaryAmount.doubleValue() );
                            filesCountModel.setSummaryAmount( summaryAmount );
                        }
                        else
                        {

                            logger.debug( "summaryAmount is NOTHING" );
                            filesCountModel.setSummaryAmount( BigDecimal.ZERO );
                        }

                        doSetUIComponentValue();

                        // hide the message.
                        filesCountModel.setDateRangeEMRendered( false );

                        // do highlight the results
                        EffectsController effectsController = (EffectsController) FacesUtils.getManagedBean( BaseBean.EFFECTS_CONTROLLER );
                        effectsController.filesCountHighlight( null );
                    }

                    else
                    {
                        handleMessages( "SEARCH_FAILED", FacesMessage.SEVERITY_WARN );
                    }
                }

                // reset the field
                else
                {
                    filesCountModel.setFilesOpened( "0" );
                    filesCountModel.setFilesClosed( "0" );
                    filesCountModel.setRecoveryAmount( BigDecimal.ZERO );
                    filesCountModel.setSummaryAmount( BigDecimal.ZERO );
                    doSetUIComponentValue();
                    // only when both date being selected
                    if ( from != null && to != null )
                    {
                        // show the message.
                        filesCountModel.setDateRangeEMRendered( true );
                        // show the pulse effect
                        EffectsController effectsController = (EffectsController) FacesUtils.getManagedBean( BaseBean.EFFECTS_CONTROLLER );
                        effectsController.pulseFromToDateError( null );
                    }

                }

            }

        }
        return "";
    }

//	public SubrogationSecurityManager getSecurityManager() {
//		return new SubrogationSecurityManager();
//	}
	
    /**
     * Load Action for the welcome Page.
     * 
     * @return navigation string to the welcome Page
     */
    public String executeGoWelcomePage()
    {
        /**
         * If the user does not have access to this action, do nothing.
         */
        if ( !  ((SubrogationSecurityManager) getSecurityManager()).hasSubrogationAdminRole() )
        {
            return "";
        }
        // reset back bean
        this.filesCountModel.reset();

        return WELCOME_PAGE;
    }


    /**
     * Utility method for setting the UI component corresponding values when selected a certain subscriber.
     *
     */
    private void doSetUIComponentValue()
    {

        UIViewRoot currentUIRoot = FacesContext.getCurrentInstance().getViewRoot();
        // set files opened on the fly.
        HtmlOutputText filesOpened = (HtmlOutputText) currentUIRoot.findComponent( FilesCountModel.UI_COM_FILES_OPENED );
        filesOpened.setValue( filesCountModel.getFilesOpened() );

        // set files closed on the fly.
        HtmlOutputText filesClosed = (HtmlOutputText) currentUIRoot.findComponent( FilesCountModel.UI_COM_FILES_CLOSED );
        filesClosed.setValue( filesCountModel.getFilesClosed() );

        // set recovery amount on the fly.
        HtmlOutputText recoveryAmount = (HtmlOutputText) currentUIRoot.findComponent( FilesCountModel.UI_COM_RECOVERY_AMOUNT );
        recoveryAmount.setValue( filesCountModel.getRecoveryAmount() );

        // set summary amount  on the fly.
        HtmlOutputText summaryAmount = (HtmlOutputText) currentUIRoot.findComponent( FilesCountModel.UI_COM_SUMMARY_AMOUNT );
        summaryAmount.setValue( filesCountModel.getSummaryAmount() );

    }


    public SubrogationService getSubrogationService()
    {
        return subrogationService;
    }


    public void setSubrogationService( SubrogationService subrogationService )
    {
        this.subrogationService = subrogationService;
    }

}
