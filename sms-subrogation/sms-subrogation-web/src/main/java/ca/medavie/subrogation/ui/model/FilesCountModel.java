
package ca.medavie.subrogation.ui.model;

import java.math.BigDecimal;
import java.util.Date;

import ca.medavie.subrogation.ui.BaseBean;

/**
 * File Count Model class holding all information of the file count UI page
 */
public class FilesCountModel extends BaseBean
{

    /**
     * UI component id name for file count from date
     */
    public static final String UI_COM_FROM_DATE_ID = "fileCountFromDate";

    /**
     * UI component id name for file count to date
     */
    public static final String UI_COM_TO_DATE_ID = "fileCountToDate";

    /**
     * UI component id name for file opened to date
     */
    public static final String UI_COM_FILES_OPENED = "filesCountForm:filesCountFilesOpen";

    /**
     * UI component id name for file opened to date
     */
    public static final String UI_COM_FILES_CLOSED = "filesCountForm:filesCountFilesClosed";

    /**
     * UI component id name for recovery amount
     */
    public static final String UI_COM_RECOVERY_AMOUNT = "filesCountForm:filesRecoveryAmount";

    /**
     * UI component id name for summary amount
     */
    public static final String UI_COM_SUMMARY_AMOUNT = "filesCountForm:filesSummaryAmount";

    /**
     * From date for the files counts
     */
    private Date fromDate;

    /**
     * To date for the files counts
     */
    private Date toDate;

    /**
     * the number of files opened in the certain period of time
     */
    private String filesOpened = "0";

    /**
     * the number of files closed in the certain period of time
     */
    private String filesClosed = "0";

    /**
     * the recovery amount in the certain period
     */
    private BigDecimal recoveryAmount = BigDecimal.ZERO;

    /**
     * the summary amount in the certain period
     */
    private BigDecimal summaryAmount = BigDecimal.ZERO;

    /**
     * date range error message rendered or not
     */
    private boolean dateRangeEMRendered = false;


    /**
     * @return the fromDate
     */
    public Date getFromDate()
    {
        return fromDate;
    }


    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate( Date fromDate )
    {
        this.fromDate = fromDate;
    }


    /**
     * @return the toDate
     */
    public Date getToDate()
    {
        return toDate;
    }


    /**
     * @param toDate the toDate to set
     */
    public void setToDate( Date toDate )
    {
        this.toDate = toDate;
    }


    /**
     * @return the filesOpened
     */
    public String getFilesOpened()
    {
        return filesOpened;
    }


    /**
     * @param filesOpened the filesOpened to set
     */
    public void setFilesOpened( String filesOpened )
    {
        this.filesOpened = filesOpened;
    }


    /**
     * @return the recoveryAmount
     */
    public BigDecimal getRecoveryAmount()
    {
        return recoveryAmount;
    }


    /**
     * @param recoveryAmount the recoveryAmount to set
     */
    public void setRecoveryAmount( BigDecimal recoveryAmount )
    {
        this.recoveryAmount = recoveryAmount;
    }


    /**
     * @return the summaryAmount
     */
    public BigDecimal getSummaryAmount()
    {
        return summaryAmount;
    }


    /**
     * @param summaryAmount the summaryAmount to set
     */
    public void setSummaryAmount( BigDecimal summaryAmount )
    {
        this.summaryAmount = summaryAmount;
    }


    /**
     * @return the filesClosed
     */
    public String getFilesClosed()
    {
        return filesClosed;
    }


    /**
     * @param filesClosed the filesClosed to set
     */
    public void setFilesClosed( String filesClosed )
    {
        this.filesClosed = filesClosed;
    }


    /**
     * @return the dateRangeEMRendered
     */
    public boolean isDateRangeEMRendered()
    {
        return dateRangeEMRendered;
    }


    /**
     * @param dateRangeEMRendered the dateRangeEMRendered to set
     */
    public void setDateRangeEMRendered( boolean dateRangeEMRendered )
    {
        this.dateRangeEMRendered = dateRangeEMRendered;
    }


    public void reset()
    {

        this.fromDate = null;
        this.toDate = null;
        this.filesClosed = "0";
        this.filesOpened = "0";
        this.recoveryAmount = BigDecimal.ZERO;
        this.summaryAmount = BigDecimal.ZERO;

    }

}
