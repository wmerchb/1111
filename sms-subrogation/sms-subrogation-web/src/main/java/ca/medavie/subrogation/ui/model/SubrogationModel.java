
package ca.medavie.subrogation.ui.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.faces.model.SelectItem;

import ca.bluecode.logging.Log;
import ca.medavie.subrogation.entities.FollowUp;
import ca.medavie.subrogation.entities.Subrogation;
import ca.medavie.subrogation.ui.BaseBean;

import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.component.selectinputtext.SelectInputText;

/**
 * Subrogation Model class holding all information in the UI
 * 
 */
public class SubrogationModel extends BaseBean
{
    /**
     * Visible property for the hide button
     */
    private boolean searchButtonVisableHide;
    /**
     * Visible property for the search button
     */
    private boolean searchButtonVisableSearch;

    /**
     * Indicates weather the outer subscriber panel(just holding the search button) rendered or not
     */
    private boolean subscriberSearchPanelRendered;

    /**
     * Indicates weather the inner subscriber panel (show search details) rendered or not
     */
    private boolean subscriberSearchPanelRendered2;

    /**
     * Indicates weather the buttons in inner subscriber panel (show search details) rendered or not
     */
    private boolean subscriberSearchPanelButtonsVisible;
    /**
     * Indicates searchpanel hide as default
     */
    private String subscriberSearchPanelStyle = "display: none";

    /**
     * Indicates new icon hide as default
     */
    private String newIconStyle = "display: none";

    /**
     * First name items length property in search form
     */
    private static final int SELECTED_FIRST_NAME_LENGTH = 10;

    /**
     * Last name items length property in search form
     */
    private static final int SELECTED_LAST_NAME_LENGTH = 10;

    /**
     * Group ID Items length property in search form
     */
    private static final int SELECTED_GROUP_ID_LENGTH = 10;
    /**
     * Sub ID items length property in search form
     */
    private static final int SELECTED_SUB_ID_LENGTH = 10;

    /**
     * First name property in search form
     */
    private String selectedFirstName;
    /**
     * First Name items that meet the search criteria
     */
    private List<SelectItem> listOfFirstNameItems;

    /**
     * Indication of whether or not the user wants follow up
     */
    private boolean followUp;

    /**
     * Last name property in search form
     */
    private String selectedLastName;
    /**
     * Last Name items that meet the search criteria
     */
    private List<SelectItem> listOfLastNameItems;

    /**
     * Group ID property in search form
     */
    private String selectedGroupID;
    /**
     * Group ID items that meet the search criteria
     */
    private List<SelectItem> listOfGroupIDItems;

    /**
     * Sub ID property in search form
     */
    private String selectedSubID;
    /**
     * Sub ID items that meet the search criteria
     */
    private List<SelectItem> listOfSubIDItems;

    /**
    * Selected Subrogation information for the UI
    */
    private Subscriber selectedSubscriber;

    /**
     * New subscriber to add in the UI
     */
    private Subscriber newSubscriberToAdd;

    /**
     * Indicates which operation is for the current page. set current operation as maint
     */
    private boolean newSubscriber = false;

    /**
     * Searching results of Subrogation for the UI
     */
    private Collection<Subrogation> subrogations;

    /**
     * searchedSubscribers information associated for the UI
     */
    private Collection<Subscriber> searchedSubscribers;

    /**
     * select input text field for first name
     */
    private static SelectInputText firstnameInputText;

    /**
     * UI component id name for  file number
     */
    public static final String FILE_NUMBER = "subrogationForm:subrogationFileNumber";

    /**
     * UI component id name for  first name
     */
    public static final String UI_COM_SEARCH_SUBSC_INFO_FIRST_NAME = "subrogationForm:searchMainPageFirstName";

    /**
     * UI component id name for  first name
     */
    public static final String UI_COM_SEARCH_SUBSC_INFO_FIRST_NAME_ID = "searchMainPageFirstName";

    /**
     * select input text field for last name
     */
    private static SelectInputText lastnameInputText;

    /**
     * UI component id name for last name
     */
    public static final String UI_COM_SEARCH_SUBSC_INFO_LAST_NAME = "subrogationForm:searchMainPageLastName";

    /**
     * select input text field for group id
     */
    private static SelectInputText groupIDSelectInputText;

    /**
     * UI component id name for group id name
     */
    public static final String UI_COM_SEARCH_SUBSC_INFO_GRP = "subrogationForm:searchMainPageGroupId";

    /**
     * select html text field for group id
     */
    private static HtmlInputText groupIDHtmlInputText;

    /**
     * UI component id name for group id name
     */
    public static final String UI_COM_NAME_GRP_HTMLINPUTTEXT = "subrogationForm:subrogationMainPageGroupId";

    /**
     * select input text field for sub id
     */
    private static SelectInputText subIDInputText;

    /**
     * UI component id name for sub id
     */
    public static final String UI_COM_SEARCH_SUBSC_INFO_SUB_ID = "subrogationForm:searchMainPageSubId";

    /**
     * UI component id name for accident date
     */
    public static final String UI_COM_SUMMARY_ACCIDENT_DATE = "subrogationForm:subrogationMainPageDateOfAccident";

    /**
     * UI component id name for accident date
     */
    public static final String UI_COM_SUMMARY_ACCIDENT_DATE_ID = "subrogationMainPageDateOfAccident";

    /**
     * UI component id name for date file open
     */
    public static final String UI_COM_DATE_FILE_OPEN_ID = "subrogationFileOpenDate";

    /**
     * UI component id name for summary date
     */
    public static final String UI_COM_SUMMARY_SUMMARY_DATE = "subrogationForm:subrogationMainPageSummaryDate";

    /**
     * UI component id name for summary date
     */
    public static final String UI_COM_SUMMARY_SUMMARY_DATE_ID = "subrogationMainPageSummaryDate";

    /**
     * UI component id name for birth date
     */
    public static final String UI_COM_SUBSC_INFO_BIRTH_DATE = "subrogationForm:subrogationMainPageBirthDate";

    /**
     * UI component id name for birth date
     */
    public static final String UI_COM_SUBSC_INFO_BIRTH_DATE_ID = "subrogationMainPageBirthDate";

    /**
     * UI component id name for summary amount
     */
    public static final String UI_COM_SUMMARY_SUMMARY_AMOUNT = "subrogationForm:subrogationMainPageSummaryAmount";

    /**
     * UI component id name for recovery date
     */
    public static final String UI_COM_RECOVERY_DATE = "subrogationForm:subrogationMainPageRecoveryDate";

    /**
     * UI component id name for recovery date picker
     */
    public static final String UI_COM_RECOVERY_DATE_PICKER = "subrogationForm:subrogationMainPageRecoveryDate_cb";

    /**
     * UI component id name for recovery date
     */
    public static final String UI_COM_RECOVERY_DATE_ID = "subrogationMainPageRecoveryDate";

    /**
     * UI component id name for recovery amount
     */
    public static final String UI_COM_RECOVERY_AMOUNT = "subrogationForm:subrogationMainPageRecoveryAmount";

    /**
     * UI component id name for recovery amount
     */
    public static final String UI_COM_RECOVERY_AMOUNT_ID = "subrogationMainPageRecoveryAmount";

    /**
     * UI component id name for lawyer fee
     */
    public static final String UI_COM_LAWYER_FEE = "subrogationForm:subrogationMainPageLawyerFee";

    /**
     * UI component id name for lawyer fee 
     */
    public static final String UI_COM_LAWYER_FEE_ID = "subrogationMainPageLawyerFee";

    /**
     * UI component id name for net recovery
     */
    public static final String UI_COM_NET_RECOVERY = "subrogationForm:subrogationMainPageNetRecovery";

    /**
     * UI component id name for net recovery
     */
    public static final String UI_COM_NET_RECOVERY_ID = "subrogationMainPageNetRecovery";

    /**
     * UI component id name for waiver letter sent
     */
    public static final String UI_COM_WAIVER_LETTER_SENT = "subrogationForm:subrogationMainPageWaiverLetterSent";

    /**
     * UI component id name for waiver letter sent
     */
    public static final String UI_COM_WAIVER_LETTER_SENT_ID = "subrogationMainPageWaiverLetterSent";

    /**
     * UI component id name for lawyer letter sent
     */
    public static final String UI_COM_LAWYER_LETTER_SENT = "subrogationForm:subrogationMainPageLawyerLetterSent";

    /**
     * UI component id name for lawyer letter sent
     */
    public static final String UI_COM_LAWYER_LETTER_SENT_ID = "subrogationMainPageLawyerLetterSent";

    /**
     * UI component id name for more information letter sent
     */
    public static final String UI_COM_MORE_INFO_LETTER_SENT = "subrogationForm:subrogationMainPageMoreInfoLetterSent";

    /**
     * UI component id name for more information letter sent
     */
    public static final String UI_COM_MORE_INFO_LETTER_SENT_ID = "subrogationMainPageMoreInfoLetterSent";

    /**
     * UI component id name for waiver letter follow up
     */
    public static final String UI_COM_MORE_WAIVER_LETTER_FOLLOWUP = "subrogationForm:subrogationMainPageWaiverLetterFollowup";

    /**
     * UI component id name for waiver letter follow up
     */
    public static final String UI_COM_MORE_WAIVER_LETTER_FOLLOWUP_ID = "subrogationMainPageWaiverLetterFollowup";
    /**
     * UI component id name for lawyer letter follow up
     */
    public static final String UI_COM_MORE_LAWYER_LETTER_FOLLOWUP = "subrogationForm:subrogationMainPageLawyerLetterFollowup";

    /**
     * UI component id name for lawyer letter follow up
     */
    public static final String UI_COM_MORE_LAWYER_LETTER_FOLLOWUP_ID = "subrogationMainPageLawyerLetterFollowup";

    /**
     * UI component id name for more information follow up
     */
    public static final String UI_COM_MORE_INFORMATION_FOLLOWUP = "subrogationForm:subrogationMainPageMoreInfoFollowup";

    /**
     * UI component id name for more information follow up
     */
    public static final String UI_COM_MORE_INFORMATION_FOLLOWUP_ID = "subrogationMainPageMoreInfoFollowup";

    /**
     * UI component id name for insurance letter sent
     */
    public static final String UI_COM_INSURANCE_LETTER_SENT = "subrogationForm:subrogationMainPageInsuranceLetterSent";

    /**
     * UI component id name for insurance letter sent
     */
    public static final String UI_COM_INSURANCE_LETTER_SENT_ID = "subrogationMainPageInsuranceLetterSent";

    /**
     * UI component id name for insurance letter follow up
     */
    public static final String UI_COM_INSURANCE_LETTER_FOLLOW_UP = "subrogationForm:subrogationMainPageInsuranceLetterFollowup";

    /**
     * UI component id name for insurance letter follow up
     */
    public static final String UI_COM_INSURANCE_LETTER_FOLLOW_UP_ID = "subrogationMainPageInsuranceLetterFollowup";

    /**
     * UI component id name for summary provide sent
     */
    public static final String UI_COM_SUMMARY_PROVIDED_SENT = "subrogationForm:subrogationMainPageSummaryProvidedSent";

    /**
     * UI component id name for summary provide sent
     */
    public static final String UI_COM_SUMMARY_PROVIDED_SENT_ID = "subrogationMainPageSummaryProvidedSent";

    /**
     * UI component id name for summary provided follow up
     */
    public static final String UI_COM_SUMMARY_PROVIDED_FOLLOWUP = "subrogationForm:subrogationMainPageSummaryProvidedFollowup";

    /**
     * UI component id name for summary provided follow up
     */
    public static final String UI_COM_SUMMARY_PROVIDED_FOLLOWUP_ID = "subrogationMainPageSummaryProvidedFollowup";

    /**
     * UI component id name for comments
     */
    public static final String UI_COM_COMMENTS = "subrogationForm:subrogationSubscriberComments";

    /**
     * select date input text field for file open date
     */
    private static SelectInputDate fileOpenDate;

    /**
     * UI component id name for file open date
     */
    public static final String UI_COM_FILE_OPEN_DATE = "subrogationForm:subrogationFileOpenDate";

    /**
     * boolean value indicates weather the UI components initialised or not
     */
    private static boolean uiComponentInitialised;

    /**
     * UI component id name for first name in subscriber information section
     */
    public static final String UI_COM_MAIN_SUBSC_INFO_FIRST_NAME = "subrogationForm:subrogationMainPageFirstName";

    /**
     * UI component id name for last name in subscriber information section
     */
    public static final String UI_COM_MAIN_SUBSC_INFO_LAST_NAME = "subrogationForm:subrogationMainPageLastName";

    /**
     * UI component id name for sub id in subscriber information section
     */
    public static final String UI_COM_MAIN_SUBSC_INFO_SUB_ID = "subrogationForm:subrogationMainPageSubId";

    /**
     * UI component id name for extra info 2 in subscriber information section
     */
    public static final String UI_COM_MAIN_SUBSC_INFO_EXTRA_INFO_2 = "subrogationForm:subrogationMainPageExtraInfo2";

    /**
     * UI component id name for extra info 1 in subscriber information section
     */
    public static final String UI_COM_MAIN_SUBSC_INFO_EXTRA_INFO_1 = "subrogationForm:subrogationMainPageExtraInfo1";

    /**
     * UI component id name for province in subscriber information section
     */
    public static final String UI_COM_MAIN_SUBSC_INFO_PROVINCE = "subrogationForm:subrogationMainPageProvince";

    /**
     * UI component id name for series in subscriber information section
     */
    public static final String UI_COM_MAIN_SUBSC_INFO_SERIES = "subrogationForm:subrogationMainPageSeries";

    /**
     * UI component id name for section in subscriber information section
     */
    public static final String UI_COM_MAIN_SUBS_INFO_SECTION = "subrogationForm:subrogationMainPageSection";

    /**
     * UI component id name for group id subscriber information section
     */
    public static final String UI_COM_MAIN_SUBSC_INFO_GRP = "subrogationForm:subrogationMainPageGroupId";

    /**
     * Indicates it is a maint operation
     */
    private boolean maintOperation;

    /**
     * Indicates it is a adding new record operation
     */
    private boolean addNewOperation;

    /**
     * Indicates it is a selected operation. set default value as maint
     */
    private String whatOperation;

    /**
     * Map representing the Benefit Packages
     */
    private Map<String, List<SelectItem>> selectedOperation;

    private String selectedOperationValue;

    private String maint = "maint";

    private String addNew = "addnew";
    /**
     * Indicates a operation name of maint
     */
    public static String MAINT_EXISTING_SELECTED = "maint";

    /**
     * Indicates a operation name of adding a new one
     */
    public static String ADD_NEW_SELECTED = "addNew";

    /**
     * radio button component id for subrogation operations
     */
    public static final String OPERATION_ITEM = "subrogationFormSelectSubscriberOperation";

    /**
     * subsciber search button id
     */
    public static final String SUBSCRIBER_SEARCH_BUTTON = "subroscriberSearchButton";

    /**
     * subsciber search button id
     */
    public static final String SUBSCRIBER_HIDE_BUTTON = "subscriberSearchPanelHideButton";

    /**
     * Set close summary panel button to be rendered as default
     */
    private boolean closeSummaryButtonRendered;

    /**
     * close summary panel button id
     */
    public static final String CLOSE_SUMMARY_BUTTON_ID = "closeSummaryPanel";

    /**
     * Set open summary panel button to be rendered as default
     */
    private boolean expandSummaryButtonRendered;

    /**
     * Expand summary panel button id
     */
    public static final String EXPAND_SUMMARY_BUTTON_ID = "expandSummaryPanel";

    /**
     * Set close recovery info button to be rendered as true
     */
    private boolean closeRecoveryInfoPanelButtonRendered;

    /**
     * Expand summary panel button id
     */
    public static final String CLOSE_RECOVER_INFO_BUTTON_ID = "closeRecoveryInfoPanel";

    /**
     * Set open recovery info button to be rendered as false
     */
    private boolean expandRecoveryInfoPanelButtonRendered;

    /**
     * Expand summary panel button id
     */
    public static final String EXPAND_RECOVER_INFO_BUTTON_ID = "expandRecoveryInfoPanel";

    /**
     * set summary information panel rendered as true
     */
    private String summaryInfoPanelRendered = "";

    /**
     * set recovery information panel rendered as true
     */
    private String recoveryInfoPanelRendered = "";

    /**
     * close summary panel button id
     */
    public static final String CLOSE_FILE_STATUS_INFO_BUTTON_ID = "closeFileStatusPanel";

    /**
     * expand summary panel button id
     */
    public static final String EXPAND_FILE_STATUS_INFO_BUTTON_ID = "expandFileStatusPanel";

    /**
     * close comment panel button id
     */
    public static final String CLOSE_COMMENT_INFO_BUTTON_ID = "closeCommentPanel";

    /**
     * expand comment panel button id
     */
    public static final String EXPAND_COMMENT_INFO_BUTTON_ID = "expandCommentPanel";

    /**
     * set recovery information panel rendered as true
     */
    private String fileStatusInfoPanelRendered = "";
    /**
     * set file status close button show as default 
     */
    private boolean closeFileStatusButtonRendered;

    /**
     * set file status open button hide as default 
     */
    private boolean expandFileStatusButtonRendered;

    /**
     * set comment close button show as default 
     */
    private boolean closeCommentButtonRendered;

    /**
     * set comment open button hide as default 
     */
    private boolean expandCommentButtonRendered;

    /**
     * set recovery information panel rendered as true
     */
    private String commentPanelRendered = "";

    /**
     * subrogation save button id
     */
    public static final String SUBROGATION_SAVE_BUTTON_ID = "subrogationSaveButton";

    /**
     * subrogation save button id
     */
    public static final String SUBROGATION_DELETE_BUTTON_ID = "subrogationDeleteButton";

    /**
     * subrogation reset button id
     */
    public static final String SUBROGATION_RESET_BUTTON_ID = "subrogationResetButton";

    /**
     * subrogation cancel button id
     */
    public static final String SUBROGATION_CANCEL_BUTTON_ID = "subrogationCancelButton";

    /**
     * A subrogation to save or update in the UI
     */
    // private Subrogation aSubrogationToSaveUpdate;
    /**
     * indicates if the save button disabled or not. set disabled as default
     */
    private boolean saveDisabled;

    /**
     * indicates if the delete button disabled or not. set disabled as default
     */
    private boolean deleteDisabled;

    /**
     * indicates the last row clicked by user in the subscriber search result table
     */
    private int lastClickedRow;

    /** The Summary Date lower limit to search on */
    private Date searchSummaryFromDate;

    /** The Summary Date upper limit to search on */
    private Date searchSummaryToDate;

    /** The Summary Amount to search on */
    private BigDecimal searchSummaryAmount = null;
   
    /** The UI component ID for Search Summary From Date */
    public static final String UI_COM_SEARCH_SUBSC_INFO_SUMMARY_FROM_DATE_ID = "searchMainPageFromDate";
    
    /** The UI component ID for Search Summary To Date */
    public static final String UI_COM_SEARCH_SUBSC_INFO_SUMMARY_TO_DATE_ID = "searchMainPageToDate";
    
    /** The UI component ID for Search Summary Amount */
    public static final String UI_COM_SEARCH_SUBSC_INFO_SUMMARY_AMOUNT_ID = "searchMainPageSummaryAmount";
    
    /** The UI component for Search Summary From Date */
    public static final String UI_COM_SEARCH_SUBSC_INFO_SUMMARY_FROM_DATE = "subrogationForm:searchMainPageFromDate";
    
    /** The UI component for Search Summary To Date */
    public static final String UI_COM_SEARCH_SUBSC_INFO_SUMMARY_TO_DATE = "subrogationForm:searchMainPageToDate";
    
    /** The UI component for Search Summary Amount */
    public static final String UI_COM_SEARCH_SUBSC_INFO_SUMMARY_AMOUNT = "subrogationForm:searchMainPageSummaryAmount";

    public SubrogationModel()
    {

        init();

    }


    /**
     * Do initialization at start
     */
    private void init()
    {
        searchButtonVisableHide = false;
        searchButtonVisableSearch = true;
        subscriberSearchPanelRendered = true;
        subscriberSearchPanelRendered2 = false;
        closeSummaryButtonRendered = true;
        expandSummaryButtonRendered = false;
        closeRecoveryInfoPanelButtonRendered = true;
        expandRecoveryInfoPanelButtonRendered = false;
        closeFileStatusButtonRendered = true;
        expandFileStatusButtonRendered = false;
        closeCommentButtonRendered = true;
        expandCommentButtonRendered = false;

        saveDisabled = true;
        deleteDisabled = true;

        whatOperation = "maint";
        lastClickedRow = 0;

        if ( selectedSubscriber == null )
        {
            selectedSubscriber = new Subscriber();
        }

        if ( newSubscriberToAdd == null )
        {

            newSubscriberToAdd = new Subscriber();
        }

        if ( selectedOperation == null )
        {

            selectedOperation = new HashMap<>();

            List<SelectItem> englishList = new ArrayList<>();
            List<SelectItem> frenchList = new ArrayList<>();

            String englishMessage1 = messageFactory.getMessageStringForKey( "ui_application_add_maint_short",
                Locale.CANADA );
            SelectItem s1 = new SelectItem( "maint", englishMessage1 );
            String englishMessage2 = messageFactory.getMessageStringForKey( "ui_application_add_new_short",
                Locale.CANADA );
            SelectItem s2 = new SelectItem( "addNew", englishMessage2 );
            englishList.add( 0, s1 );
            englishList.add( 1, s2 );

            String frenchMessage1 = messageFactory.getMessageStringForKey( "ui_application_add_maint_short",
                Locale.CANADA_FRENCH );
            SelectItem f1 = new SelectItem( "maint", frenchMessage1 );
            String frenchMessage2 = messageFactory.getMessageStringForKey( "ui_application_add_new_short",
                Locale.CANADA_FRENCH );
            SelectItem f2 = new SelectItem( "addNew", frenchMessage2 );
            frenchList.add( 0, f1 );
            frenchList.add( 1, f2 );

            selectedOperation.put( Locale.ENGLISH.getLanguage(), englishList );
            selectedOperation.put( Locale.FRENCH.getLanguage(), frenchList );

        }

        if ( selectedOperationValue == null )
        {
            selectedOperationValue = ( ( selectedOperation.get( getLocale().getLanguage() ) ).get( 0 ) ).getValue().toString();
        }
        Log.logDebug( this.getClass(), "init()", "SubrogationModel initilisation is done!!!" );
    }


    /**
     * @return the firstnameInputText
     */
    public SelectInputText getFirstnameInputText()
    {
        return firstnameInputText;
    }


    /**
     * @param firstnameInputText the firstnameInputText to set
     */
    public void setFirstnameInputText( SelectInputText firstnameInputText )
    {
        SubrogationModel.firstnameInputText = firstnameInputText;
    }


    /**
     * Utility method for reseting the search form after navigated to another page
     * 
     * */
    public void resetSearchForm()
    {

        searchButtonVisableHide = false;
        searchButtonVisableSearch = true;
    }


    public boolean isSearchButtonVisableSearch()
    {
        return searchButtonVisableSearch;
    }


    public void setSearchButtonVisableSearch( boolean searchButtonVisable_search )
    {
        this.searchButtonVisableSearch = searchButtonVisable_search;
    }


    public boolean isSearchButtonVisableHide()
    {
        return searchButtonVisableHide;
    }


    public void setSearchButtonVisableHide( boolean searchButtonVisablem )
    {
        this.searchButtonVisableHide = searchButtonVisablem;
    }


    /**
     * @return the subscriberSearchPanelRendered
     */
    public boolean isSubscriberSearchPanelRendered()
    {
        return subscriberSearchPanelRendered;
    }


    /**
     * @param subscriberSearchPanelRendered the subscriberSearchPanelRendered to set
     */
    public void setSubscriberSearchPanelRendered( boolean subscriberSearchPanelRendered )
    {
        this.subscriberSearchPanelRendered = subscriberSearchPanelRendered;
    }


    /**
     * @return the subscriberSearchPanelRendered
     */
    public boolean isSubscriberSearchPanelRendered2()
    {
        return subscriberSearchPanelRendered2;
    }


    /**
     * @param subscriberSearchPanelRendered the subscriberSearchPanelRendered to set
     */
    public void setSubscriberSearchPanelRendered2( boolean subscriberSearchPanelRendered2 )
    {
        this.subscriberSearchPanelRendered2 = subscriberSearchPanelRendered2;
    }


    /**
     * @return the subscriberSearchPanelButtonsVisible
     */
    public boolean isSubscriberSearchPanelButtonsVisible()
    {
        return subscriberSearchPanelButtonsVisible;
    }


    /**
     * @param subscriberSearchPanelButtonsVisible the subscriberSearchPanelButtonsVisible to set
     */
    public void setSubscriberSearchPanelButtonsVisible( boolean subscriberSearchPanelButtonsVisible )
    {
        this.subscriberSearchPanelButtonsVisible = subscriberSearchPanelButtonsVisible;
    }


    /**
     * @return the subscriberSearchPanelStyle
     */
    public String getSubscriberSearchPanelStyle()
    {
        return subscriberSearchPanelStyle;
    }


    /**
     * @param subscriberSearchPanelStyle the subscriberSearchPanelStyle to set
     */
    public void setSubscriberSearchPanelStyle( String subscriberSearchPanelStyle )
    {
        this.subscriberSearchPanelStyle = subscriberSearchPanelStyle;
    }


    /**
     * @return the newIconStyle
     */
    public String getNewIconStyle()
    {
        return newIconStyle;
    }


    /**
     * @param newIconStyle the newIconStyle to set
     */
    public void setNewIconStyle( String newIconStyle )
    {
        this.newIconStyle = newIconStyle;
    }


    /**
     * @return the selectedFirstName
     */
    public int getSelectedFirstNameLength()
    {
        return SELECTED_FIRST_NAME_LENGTH;
    }


    /**
     * @return the selectedFirstName
     */
    public String getSelectedFirstName()
    {
        return selectedFirstName;
    }


    /**
     * @param selectedFirstName the selectedFirstName to set
     */
    public void setSelectedFirstName( String selectedFirstName )
    {
        this.selectedFirstName = selectedFirstName;
    }


    /**
     * @return the listOfFirstNameItems
     */
    public List<SelectItem> getListOfFirstNameItems()
    {
        return listOfFirstNameItems;
    }


    /**
     * @param listOfFirstNameItems the listOfFirstNameItems to set
     */
    public void setListOfFirstNameItems( List<SelectItem> listOfFirstNameItems )
    {
        this.listOfFirstNameItems = listOfFirstNameItems;
    }


    /**
     * @return the followUp
     */
    public boolean getFollowUp()
    {
        return followUp;
    }


    /**
     * @param followUp the boolean value of a checkbox
     */
    public void setFollowUp( boolean followUp )
    {
        this.followUp = followUp;
    }


    /**
     * @return the selectedLastNameLength
     */
    public int getSELECTED_LAST_NAME_LENGTH()
    {
        return SELECTED_LAST_NAME_LENGTH;
    }


    /**
     * @return the selectedLastName
     */
    public String getSelectedLastName()
    {
        return selectedLastName;
    }


    /**
     * @param selectedLastName the selectedLastName to set
     */
    public void setSelectedLastName( String selectedLastName )
    {
        this.selectedLastName = selectedLastName;
    }


    /**
     * @return the listOfLastNameItems
     */
    public List<SelectItem> getListOfLastNameItems()
    {
        return listOfLastNameItems;
    }


    /**
     * @param listOfLastNameItems the listOfLastNameItems to set
     */
    public void setListOfLastNameItems( List<SelectItem> listOfLastNameItems )
    {
        this.listOfLastNameItems = listOfLastNameItems;
    }


    /**
     * @return the selectedGroupIDLength
     */
    public int getSelectedGroupIDLength()
    {
        return SELECTED_GROUP_ID_LENGTH;
    }


    /**
     * @return the selectedGroupID
     */
    public String getSelectedGroupID()
    {
        return selectedGroupID;
    }


    /**
     * @param selectedGroupID the selectedGroupID to set
     */
    public void setSelectedGroupID( String selectedGroupID )
    {
        this.selectedGroupID = selectedGroupID;
    }


    /**
     * @return the listOfGroupIDItems
     */
    public List<SelectItem> getListOfGroupIDItems()
    {
        return listOfGroupIDItems;
    }


    /**
     * @param listOfGroupIDItems the listOfGroupIDItems to set
     */
    public void setListOfGroupIDItems( List<SelectItem> listOfGroupIDItems )
    {
        this.listOfGroupIDItems = listOfGroupIDItems;
    }


    /**
     * @return the selectedSubIDLength
     */
    public int getSELECTED_SUB_ID_LENGTH()
    {
        return SELECTED_SUB_ID_LENGTH;
    }


    /**
     * @return the selectedSubID
     */
    public String getSelectedSubID()
    {
        return selectedSubID;
    }


    /**
     * @param selectedSubID the selectedSubID to set
     */
    public void setSelectedSubID( String selectedSubID )
    {
        this.selectedSubID = selectedSubID;
    }


    /**
     * @return the listOfSubIDItems
     */
    public List<SelectItem> getListOfSubIDItems()
    {
        return listOfSubIDItems;
    }


    /**
     * @param listOfSubIDItems the listOfSubIDItems to set
     */
    public void setListOfSubIDItems( List<SelectItem> listOfSubIDItems )
    {
        this.listOfSubIDItems = listOfSubIDItems;
    }


    /**
     * @return the subscribers associated with the corresponding subrogations
     */
    public Collection<Subscriber> getSearchedSubscribers()
    {

        return searchedSubscribers;
    }


    /**
     * @param subscribers the subscribers to set
     */
    public void setSearchedSubscribers( Collection<Subscriber> subscribers )
    {
        this.searchedSubscribers = subscribers;
    }


    /**
     * Utility method to set searched subrogations into the searchedSubsrcibers.
     * 
     * @param Collection -  subrogations, the subrogations to set into the subscribers
     */
    public void doSetSearchedSubscribersBySubrogations( Collection<Subrogation> subrogations )
    {

        if ( subrogations != null )
        {
            searchedSubscribers = new ArrayList<Subscriber>();

            for ( Subrogation subrogation : subrogations )
            {

                Subscriber aSubscriber = (Subscriber) doConvertSubscriberSubrogation( subrogation );
                searchedSubscribers.add( aSubscriber );

            }

        }

    }


    /**
     * Utility method to either convert an subrogation into a subscriber or do the opposite
     * 
     * @param Object - object to be converted to
     * 
     * @return Object - converted object
     */
    public Object doConvertSubscriberSubrogation( Object aObject )
    {

        Object aConvertedObject = null;

        if ( aObject != null )
        {
            //do convert from subscribe to subrogation
            if ( aObject instanceof Subscriber )
            {

                Subscriber aSubscriberObj = ( (Subscriber) aObject );
                Subrogation aSubrogation = new Subrogation();

                Long id = aSubscriberObj.getId();
                aSubrogation.setId( id );

                Date fileOpenDate = aSubscriberObj.getDateFileOpened();
                aSubrogation.setDateFileOpened( fileOpenDate );

                String firstName = aSubscriberObj.getFirstName();
                aSubrogation.setFirstName( firstName );

                String lastName = aSubscriberObj.getLastName();
                aSubrogation.setLastName( lastName );

                String groupID = aSubscriberObj.getGroupID();
                aSubrogation.setGroupID( groupID );

                String subID = aSubscriberObj.getSubID();
                aSubrogation.setSubID( subID );

                String extraInfo1 = aSubscriberObj.getExtraInfo1();
                aSubrogation.setExtraInfo1( extraInfo1 );

                String extraInfo2 = aSubscriberObj.getExtraInfo2();
                aSubrogation.setExtraInfo2( extraInfo2 );

                String province = aSubscriberObj.getProvince();
                aSubrogation.setProvince( province );

                String section = aSubscriberObj.getSection();
                aSubrogation.setSection( section );

                String series = aSubscriberObj.getSeries();
                aSubrogation.setSeries( series );

                Date accidentDate = aSubscriberObj.getAccidentDate();
                aSubrogation.setAccidentDate( accidentDate );

                Date insuranceLetterFollowUp = aSubscriberObj.getInsuranceLetterFollowUp();
                aSubrogation.setInsuranceLetterFollowUp( insuranceLetterFollowUp );

                Date insuranceLetterSent = aSubscriberObj.getInsuranceLetterSent();
                aSubrogation.setInsuranceLetterSent( insuranceLetterSent );

                Date lawyerLetterFollowUp = aSubscriberObj.getLawyerLetterFollowUp();
                aSubrogation.setLawyerLetterFollowUp( lawyerLetterFollowUp );

                Date lawyerLetterSent = aSubscriberObj.getLawyerLetterSent();
                aSubrogation.setLawyerLetterSent( lawyerLetterSent );

                Date moreInfoFollowUp = aSubscriberObj.getMoreInfoFollowUp();
                aSubrogation.setMoreInfoFollowUp( moreInfoFollowUp );

                Date moreInfoLetterSent = aSubscriberObj.getMoreInfoLetterSent();
                aSubrogation.setMoreInfoLetterSent( moreInfoLetterSent );

                Date recoveryDate = aSubscriberObj.getRecoveryDate();
                aSubrogation.setRecoveryDate( recoveryDate );

                BigDecimal recoveryAmount = aSubscriberObj.getRecoveryAmount();
                aSubrogation.setRecoveryAmount( recoveryAmount );

                String comment = aSubscriberObj.getComment();
                aSubrogation.setComment( comment );

                BigDecimal netRecovery = aSubscriberObj.getNetRecovery();
                aSubrogation.setNetRecovery( netRecovery );

                Date summaryDate = aSubscriberObj.getSummaryDate();
                aSubrogation.setSummaryDate( summaryDate );

                Date birthDate = aSubscriberObj.getBirthDate();
                aSubrogation.setBirthDate( birthDate );

                Date summaryProvidedFollowUp = aSubscriberObj.getSummaryProvidedFollowUp();
                aSubrogation.setSummaryProvidedFollowUp( summaryProvidedFollowUp );

                Date summaryProvidedSent = aSubscriberObj.getSummaryProvidedSent();
                aSubrogation.setSummaryProvidedSent( summaryProvidedSent );

                BigDecimal summaryAmount = aSubscriberObj.getSummaryAmount();
                aSubrogation.setSummaryAmount( summaryAmount );

                BigDecimal lawyerFee = aSubscriberObj.getLawyerFee();
                aSubrogation.setLawyerFee( lawyerFee );

                Date waiverLetterSent = aSubscriberObj.getWaiverLetterSent();
                aSubrogation.setWaiverLetterSent( waiverLetterSent );

                Date waiverLetterFollowUp = aSubscriberObj.getWaiverLetterFollowUp();
                aSubrogation.setWaiverLetterFollowUp( waiverLetterFollowUp );

                Set<FollowUp> followUps = aSubscriberObj.getFollowUps();
                aSubrogation.setFollowUps( followUps );

                return aSubrogation;

            }

            //do convert from subrogation to subscribe 
            if ( aObject instanceof Subrogation )
            {
                Subscriber aSubscriberObj = new Subscriber();
                Subrogation aSubrogation = (Subrogation) aObject;

                Long id = aSubrogation.getId();
                aSubscriberObj.setId( id );

                Date fileOpenDate = aSubrogation.getDateFileOpened();
                aSubscriberObj.setDateFileOpened( fileOpenDate );

                String firstName = aSubrogation.getFirstName();
                aSubscriberObj.firstName = firstName;

                String lastName = aSubrogation.getLastName();
                aSubscriberObj.lastName = lastName;

                String groupID = aSubrogation.getGroupID();
                aSubscriberObj.groupID = groupID;

                String subID = aSubrogation.getSubID();
                aSubscriberObj.subID = subID;

                String extraInfo1 = aSubrogation.getExtraInfo1();
                aSubscriberObj.setExtraInfo1( extraInfo1 );

                String extraInfo2 = aSubrogation.getExtraInfo2();
                aSubscriberObj.setExtraInfo2( extraInfo2 );

                String province = aSubrogation.getProvince();
                aSubscriberObj.setProvince( province );

                String section = aSubrogation.getSection();
                aSubscriberObj.setSection( section );

                String series = aSubrogation.getSeries();
                aSubscriberObj.setSeries( series );

                Date accidentDate = aSubrogation.getAccidentDate();
                aSubscriberObj.setAccidentDate( accidentDate );

                Date insuranceLetterFollowUp = aSubrogation.getInsuranceLetterFollowUp();
                aSubscriberObj.setInsuranceLetterFollowUp( insuranceLetterFollowUp );

                Date insuranceLetterSent = aSubrogation.getInsuranceLetterSent();
                aSubscriberObj.setInsuranceLetterSent( insuranceLetterSent );

                Date lawyerLetterFollowUp = aSubrogation.getLawyerLetterFollowUp();
                aSubscriberObj.setLawyerLetterFollowUp( lawyerLetterFollowUp );

                Date lawyerLetterSent = aSubrogation.getLawyerLetterSent();
                aSubscriberObj.setLawyerLetterSent( lawyerLetterSent );

                Date moreInfoFollowUp = aSubrogation.getMoreInfoFollowUp();
                aSubscriberObj.setMoreInfoFollowUp( moreInfoFollowUp );

                Date moreInfoLetterSent = aSubrogation.getMoreInfoLetterSent();
                aSubscriberObj.setMoreInfoLetterSent( moreInfoLetterSent );

                Date recoveryDate = aSubrogation.getRecoveryDate();
                aSubscriberObj.setRecoveryDate( recoveryDate );

                BigDecimal recoveryAmount = aSubrogation.getRecoveryAmount();
                aSubscriberObj.setRecoveryAmount( recoveryAmount );

                String comment = aSubrogation.getComment();
                aSubscriberObj.setComment( comment );

                BigDecimal netRecovery = aSubrogation.getNetRecovery();
                aSubscriberObj.setNetRecovery( netRecovery );

                Date summaryDate = aSubrogation.getSummaryDate();
                aSubscriberObj.setSummaryDate( summaryDate );

                Date birthDate = aSubrogation.getBirthDate();
                aSubscriberObj.setBirthDate( birthDate );

                Date summaryProvidedFollowUp = aSubrogation.getSummaryProvidedFollowUp();
                aSubscriberObj.setSummaryProvidedFollowUp( summaryProvidedFollowUp );

                Date summaryProvidedSent = aSubrogation.getSummaryProvidedSent();
                aSubscriberObj.setSummaryProvidedSent( summaryProvidedSent );

                BigDecimal summaryAmount = aSubrogation.getSummaryAmount();
                aSubscriberObj.setSummaryAmount( summaryAmount );

                BigDecimal lawyerFee = aSubrogation.getLawyerFee();
                aSubscriberObj.setLawyerFee( lawyerFee );

                Date waiverLetterSent = aSubrogation.getWaiverLetterSent();
                aSubscriberObj.setWaiverLetterSent( waiverLetterSent );

                Date waiverLetterFollowUp = aSubrogation.getWaiverLetterFollowUp();
                aSubscriberObj.setWaiverLetterFollowUp( waiverLetterFollowUp );

                Set<FollowUp> followUps = aSubrogation.getFollowUps();
                aSubscriberObj.setFollowUps( followUps );

                return aSubscriberObj;
            }

        }
        return aConvertedObject;

    }


    /**
     * Utility method for refreshing the property "searchedSubscribers"
     * What it does is, when user did save or update to a subscriber, property "searchedSubscribers" in this class should be updated
     * Otherwise, when user click back to this subscriber in the result table, this subscriber's information will not be updated in the UI.
     * 
     * @param Collection - searchedSubscribers, searched subscribers in the table
     * @param Subscriber - modifiedSubscriber, saved or updated subscriber.
     */
    public Collection<Subscriber> refreshSearchedSubscribers( Collection<Subscriber> searchedSubscribers,
            Subscriber modifiedSubscriber )
    {
        ArrayList<Subscriber> temList = new ArrayList<Subscriber>( searchedSubscribers );
        for ( Subscriber subs : temList )
        {
            if ( subs.equals( modifiedSubscriber ) )
            {
                Log.logDebug( this.getClass(), "refreshSearchedSubscribers", "Updated subscriber is : "
                    + modifiedSubscriber );
                int index = temList.indexOf( subs );
                temList.remove( index );
                temList.add( index, modifiedSubscriber );
                break;

            }

        }
        return temList;
    }


    /**
     * @return the subrogation
     */
    public Collection<Subrogation> getSubrogations()
    {
        return subrogations;
    }


    /**
     * @param subrogation the subrogation to set
     */
    public void setSubrogations( Collection<Subrogation> subrogation )
    {
        this.subrogations = subrogation;
        // do set its corresponding subscriber as at same time
        // doSetSubscribersBySubrogation( subrogation );
    }


    /**
     * @return the lastnameInputText
     */
    public SelectInputText getLastnameInputText()
    {
        return lastnameInputText;
    }


    /**
     * @param lastnameInputText the lastnameInputText to set
     */
    public void setLastnameInputText( SelectInputText lastnameInputText )
    {
        SubrogationModel.lastnameInputText = lastnameInputText;
    }


    /**
     * @return the groupIDInputText
     */
    public SelectInputText getGroupIDSelectInputText()
    {
        return groupIDSelectInputText;
    }


    /**
     * @param groupIDInputText the groupIDInputText to set
     */
    public void setGroupIDSelectInputText( SelectInputText groupIDInputText )
    {
        SubrogationModel.groupIDSelectInputText = groupIDInputText;
    }


    /**
     * @return the subIDInputText
     */
    public SelectInputText getSubIDInputText()
    {
        return subIDInputText;
    }


    /**
     * @param subIDInputText the subIDInputText to set
     */
    public void setSubIDInputText( SelectInputText subIDInputText )
    {
        SubrogationModel.subIDInputText = subIDInputText;
    }


    /**
     * @return the uiComponentInitialized
     */
    public static boolean isUiComponentInitialised()
    {
        uiComponentInitialised = ! ( firstnameInputText == null || lastnameInputText == null
            || groupIDSelectInputText == null || subIDInputText == null );
        return uiComponentInitialised;
    }


    /**
     * @param uiComponentInitialized the uiComponentInitialized to set
     */
    public static void setUiComponentInitialised( boolean uiComponentInitialized )
    {
        SubrogationModel.uiComponentInitialised = uiComponentInitialized;
    }


    /**
     * @return the fileOpenDate
     */
    public static SelectInputDate getFileOpenDate()
    {
        return fileOpenDate;
    }


    /**
     * @param fileOpenDate the fileOpenDate to set
     */
    public static void setFileOpenDate( SelectInputDate fileOpenDate )
    {
        SubrogationModel.fileOpenDate = fileOpenDate;
    }


    /**
     * @return the groupIDHtmlInputText
     */
    public static HtmlInputText getGroupIDHtmlInputText()
    {
        return groupIDHtmlInputText;
    }


    /**
     * @param groupIDHtmlInputText the groupIDHtmlInputText to set
     */
    public static void setGroupIDHtmlInputText( HtmlInputText groupIDHtmlInputText )
    {
        SubrogationModel.groupIDHtmlInputText = groupIDHtmlInputText;
    }


    /**
     * @return the selectedSubscirber
     */
    public Subscriber getSelectedSubscriber()
    {
        return selectedSubscriber;
    }


    /**
     * @param selectedSubscirber the selectedSubscirber to set
     */
    public void setSelectedSubscriber( Subscriber selectedSubscirber )
    {
        //System.out.println("Class : " + this.getClass() + ("--SET Mehtod : setSelectedSubscriber-- Hashcode :")+ selectedSubscirber.hashCode()+ "--groupID to set --" + selectedSubscirber.getGroupID());
        this.selectedSubscriber = selectedSubscirber;
    }


    /**
     * @return the newSubscriberToAdd
     */
    public Subscriber getNewSubscriberToAdd()
    {
        return newSubscriberToAdd;
    }


    /**
     * @param newSubscriberToAdd the newSubscriberToAdd to set
     */
    public void setNewSubscriberToAdd( Subscriber newSubscriberToAdd )
    {
        this.newSubscriberToAdd = newSubscriberToAdd;
    }


    /**
     * @return the whichSubscriber
     */
    public Subscriber getWhichSubscriber()
    {

        return ( newSubscriber ) ? newSubscriberToAdd : selectedSubscriber;

    }


    /**
     * @return the whatSubscriber
     */
    public boolean isNewSubscriber()
    {
        return newSubscriber;
    }


    /**
     * @param whatSubscriber the whatSubscriber to set
     */
    public void setNewSubscriber( boolean whatSubscriber )
    {
        this.newSubscriber = whatSubscriber;
    }


    /**
     * @return the whatOperation
     */
    public boolean isMaintOperation()
    {

        return maintOperation;
    }


    /**
     * @param whatOperation the whatOperation to set
     */
    public void setMaintOperation( boolean whatOperation )
    {
        this.maintOperation = whatOperation;
    }


    /**
     * @return the addNewOperation
     */
    public boolean isAddNewOperation()
    {
        return addNewOperation;
    }


    /**
     * @param addNewOperation the addNewOperation to set
     */
    public void setAddNewOperation( boolean addNewOperation )
    {
        this.addNewOperation = addNewOperation;
    }


    /**
     * @return the selectedOperation
     */
    public Map<String, List<SelectItem>> getSelectedOperation()
    {
        return selectedOperation;
    }


    /**
     * @param selectedOperation the selectedOperation to set
     */
    public void setSelectedOperation( Map<String, List<SelectItem>> selectedOperation )
    {
        this.selectedOperation = selectedOperation;
    }


    /**
     * @return the selectedOperationValue
     */
    public String getSelectedOperationValue()
    {
        return selectedOperationValue;
    }


    /**
     * @param selectedOperationValue the selectedOperationValue to set
     */
    public void setSelectedOperationValue( String selectedOperationValue )
    {
        this.selectedOperationValue = selectedOperationValue;
    }


    /**
     * @return the maint
     */
    public String getMaint()
    {
        return maint;
    }


    /**
     * @param maint the maint to set
     */
    public void setMaint( String maint )
    {
        this.maint = maint;
    }


    /**
     * @return the addNew
     */
    public String getAddNew()
    {
        return addNew;
    }


    /**
     * @param addNew the addNew to set
     */
    public void setAddNew( String addNew )
    {
        this.addNew = addNew;
    }


    /**
     * @return the closeSummaryButtonRendered
     */
    public boolean isCloseSummaryButtonRendered()
    {
        return closeSummaryButtonRendered;
    }


    /**
     * @param closeSummaryButtonRendered the closeSummaryButtonRendered to set
     */
    public void setCloseSummaryButtonRendered( boolean closeSummaryButtonRendered )
    {
        this.closeSummaryButtonRendered = closeSummaryButtonRendered;
    }


    /**
     * @return the expandSummaryButtonRendered
     */
    public boolean isExpandSummaryButtonRendered()
    {
        return expandSummaryButtonRendered;
    }


    /**
     * @param expandSummaryButtonRendered the expandSummaryButtonRendered to set
     */
    public void setExpandSummaryButtonRendered( boolean expandSummaryButtonRendered )
    {
        this.expandSummaryButtonRendered = expandSummaryButtonRendered;
    }


    /**
     * @return the closeRecoveryInfoPanelButtonRendered
     */
    public boolean isCloseRecoveryInfoPanelButtonRendered()
    {
        return closeRecoveryInfoPanelButtonRendered;
    }


    /**
     * @param closeRecoveryInfoPanelButtonRendered the closeRecoveryInfoPanelButtonRendered to set
     */
    public void setCloseRecoveryInfoPanelButtonRendered( boolean closeRecoveryInfoPanelButtonRendered )
    {
        this.closeRecoveryInfoPanelButtonRendered = closeRecoveryInfoPanelButtonRendered;
    }


    /**
     * @return the expandRecoveryInfoPanelButtonRendered
     */
    public boolean isExpandRecoveryInfoPanelButtonRendered()
    {
        return expandRecoveryInfoPanelButtonRendered;
    }


    /**
     * @param expandRecoveryInfoPanelButtonRendered the expandRecoveryInfoPanelButtonRendered to set
     */
    public void setExpandRecoveryInfoPanelButtonRendered( boolean expandRecoveryInfoPanelButtonRendered )
    {
        this.expandRecoveryInfoPanelButtonRendered = expandRecoveryInfoPanelButtonRendered;
    }


    /**
     * @return the summaryIfoPanelRendered
     */
    public String getSummaryInfoPanelRendered()
    {
        return summaryInfoPanelRendered;
    }


    /**
     * @param summaryIfoPanelRendered the summaryIfoPanelRendered to set
     */
    public void setSummaryInfoPanelRendered( String summaryIfoPanelRendered )
    {
        this.summaryInfoPanelRendered = summaryIfoPanelRendered;
    }


    /**
     * @return the recoveryInfoPanelRendered
     */
    public String getRecoveryInfoPanelRendered()
    {
        return recoveryInfoPanelRendered;
    }


    /**
     * @param recoveryInfoPanelRendered the recoveryInfoPanelRendered to set
     */
    public void setRecoveryInfoPanelRendered( String recoveryInfoPanelRendered )
    {
        this.recoveryInfoPanelRendered = recoveryInfoPanelRendered;
    }


    /**
     * @return the fileStatusInfoPanelRendered
     */
    public String getFileStatusInfoPanelRendered()
    {
        return fileStatusInfoPanelRendered;
    }


    /**
     * @param fileStatusInfoPanelRendered the fileStatusInfoPanelRendered to set
     */
    public void setFileStatusInfoPanelRendered( String fileStatusInfoPanelRendered )
    {
        this.fileStatusInfoPanelRendered = fileStatusInfoPanelRendered;
    }


    /**
     * @return the closeFileStatusButtonRendered
     */
    public boolean isCloseFileStatusButtonRendered()
    {
        return closeFileStatusButtonRendered;
    }


    /**
     * @param closeFileStatusButtonRendered the closeFileStatusButtonRendered to set
     */
    public void setCloseFileStatusButtonRendered( boolean closeFileStatusButtonRendered )
    {
        this.closeFileStatusButtonRendered = closeFileStatusButtonRendered;
    }


    /**
     * @return the expandFileStatusButtonRendered
     */
    public boolean isExpandFileStatusButtonRendered()
    {
        return expandFileStatusButtonRendered;
    }


    /**
     * @param expandFileStatusButtonRendered the expandFileStatusButtonRendered to set
     */
    public void setExpandFileStatusButtonRendered( boolean expandFileStatusButtonRendered )
    {
        this.expandFileStatusButtonRendered = expandFileStatusButtonRendered;
    }


    /**
     * @return the closeCommentButtonRendered
     */
    public boolean isCloseCommentButtonRendered()
    {
        return closeCommentButtonRendered;
    }


    /**
     * @param closeCommentButtonRendered the closeCommentButtonRendered to set
     */
    public void setCloseCommentButtonRendered( boolean closeCommentButtonRendered )
    {
        this.closeCommentButtonRendered = closeCommentButtonRendered;
    }


    /**
     * @return the expandCommentButtonRendered
     */
    public boolean isExpandCommentButtonRendered()
    {
        return expandCommentButtonRendered;
    }


    /**
     * @param expandCommentButtonRendered the expandCommentButtonRendered to set
     */
    public void setExpandCommentButtonRendered( boolean expandCommentButtonRendered )
    {
        this.expandCommentButtonRendered = expandCommentButtonRendered;
    }


    /**
     * @return the commentPanelRendered
     */
    public String getCommentPanelRendered()
    {
        return commentPanelRendered;
    }


    /**
     * @param commentPanelRendered the commentPanelRendered to set
     */
    public void setCommentPanelRendered( String commentPanelRendered )
    {
        this.commentPanelRendered = commentPanelRendered;
    }


    /**
     * @return the saveEnabled
     */
    public boolean isSaveDisabled()
    {
        return saveDisabled;
    }


    /**
     * @param saveEnabled the saveEnabled to set
     */
    public void setSaveDisabled( boolean saveEnabled )
    {
        this.saveDisabled = saveEnabled;
    }


    /**
     * @return the deleteEnabled
     */
    public boolean isDeleteDisabled()
    {
        return deleteDisabled;
    }


    /**
     * @param deleteEnabled the deleteEnabled to set
     */
    public void setDeleteDisabled( boolean deleteEnabled )
    {
        this.deleteDisabled = deleteEnabled;
    }


    /**
     * @return the whatOperation
     */
    public String getWhatOperation()
    {
        return whatOperation;
    }


    /**
     * @param whatOperation the whatOperation to set
     */
    public void setWhatOperation( String whatOperation )
    {
        this.whatOperation = whatOperation;
    }


    /**
     * @return the lastClickedRow
     */
    public int getLastClickedRow()
    {
        return lastClickedRow;
    }


    /**
     * @param lastClickedRow the lastClickedRow to set
     */
    public void setLastClickedRow( int lastClickedRow )
    {
        this.lastClickedRow = lastClickedRow;
    }


    /**
     * Get The Summary Date lower limit to search on
     * @return Date
     */
    public Date getSearchSummaryFromDate()
    {
        return searchSummaryFromDate;
    }


    /**
     * Set The Summary Date lower limit to search on
     * @param aSearchSummaryFromDate
     */
    public void setSearchSummaryFromDate( Date aSearchSummaryFromDate )
    {
        this.searchSummaryFromDate = aSearchSummaryFromDate;
    }


    /**
     * Get The Summary Date upper limit to search on
     * @return
     */
    public Date getSearchSummaryToDate()
    {
        return searchSummaryToDate;
    }


    /**
     * Set The Summary Date upper limit to search on
     * @param aSearchSummaryToDates
     */
    public void setSearchSummaryToDate( Date aSearchSummaryToDate )
    {
        this.searchSummaryToDate = aSearchSummaryToDate;
    }


    /**
     * Get The Summary Amount to search on
     * @return BigDecimal
     */
    public BigDecimal getSearchSummaryAmount()
    {
        return searchSummaryAmount;
    }


    /** 
     * Set The Summary Amount to search on 
     * @param aSearchSummaryAmount
     */
    public void setSearchSummaryAmount( BigDecimal aSearchSummaryAmount )
    {
        this.searchSummaryAmount = aSearchSummaryAmount;
    }


    /**
     * Inner subscriber class representing subscribers in the search form
     */
    public class Subscriber
    {
        private Long id;

        private Date dateFileOpened;

        public String groupID;

        private String section;

        private String series;

        private String province;

        private String extraInfo2;

        public String subID;

        public String firstName;

        public String lastName;

        private String extraInfo1;

        private Date accidentDate;

        private Date summaryDate;

        private Date birthDate;

        private BigDecimal summaryAmount;

        private Date recoveryDate;

        private BigDecimal recoveryAmount;

        private BigDecimal lawyerFee;

        private BigDecimal netRecovery;

        private Date waiverLetterSent;

        private Date lawyerLetterSent;

        private Date moreInfoLetterSent;

        private Date waiverLetterFollowUp;

        private Date lawyerLetterFollowUp;

        private Date moreInfoFollowUp;

        private Date insuranceLetterSent;

        private Date insuranceLetterFollowUp;

        private Date summaryProvidedSent;

        private Date summaryProvidedFollowUp;

        private String comment;

        private boolean selected;

        private Set<FollowUp> followUps;


        public Long getId()
        {

            return id;
        }


        public void setId( Long id )
        {

            this.id = id;
        }


        public Date getDateFileOpened()
        {

            return dateFileOpened;

        }


        public void setDateFileOpened( Date dateFileOpened )
        {

            this.dateFileOpened = dateFileOpened;
        }


        public String getGroupID()
        {

            return groupID;
        }


        public void setGroupID( String groupID )
        {
            this.groupID = groupID;
        }


        public String getSection()
        {

            return section;
        }


        public void setSection( String section )
        {

            this.section = section;
        }


        public String getSeries()
        {
            return series;
        }


        public void setSeries( String series )
        {
            this.series = series;
        }


        public String getProvince()
        {
            return province;
        }


        public void setProvince( String province )
        {
            this.province = province;
        }


        public String getExtraInfo2()
        {
            return extraInfo2;
        }


        public void setExtraInfo2( String extraInfo2 )
        {
            this.extraInfo2 = extraInfo2;
        }


        public String getSubID()
        {
            return subID;
        }


        public void setSubID( String subID )
        {
            this.subID = subID;
        }


        public String getFirstName()
        {
            return firstName;
        }


        public void setFirstName( String firstName )
        {
            this.firstName = firstName;
        }


        public String getLastName()
        {
            return lastName;
        }


        public void setLastName( String lastName )
        {
            this.lastName = lastName;
        }


        public String getExtraInfo1()
        {
            return extraInfo1;
        }


        public void setExtraInfo1( String extraInfo1 )
        {
            this.extraInfo1 = extraInfo1;
        }


        public Date getAccidentDate()
        {
            return accidentDate;
        }


        public void setAccidentDate( Date accidentDate )
        {
            this.accidentDate = accidentDate;
        }


        public Date getSummaryDate()
        {
            return summaryDate;
        }


        public void setSummaryDate( Date summaryDate )
        {
            this.summaryDate = summaryDate;
        }


        public Date getBirthDate()
        {
            return birthDate;
        }


        public void setBirthDate( Date birthDate )
        {
            this.birthDate = birthDate;
        }


        public BigDecimal getSummaryAmount()
        {
            return summaryAmount;
        }


        public void setSummaryAmount( BigDecimal summaryAmount )
        {
            this.summaryAmount = summaryAmount;
        }


        public Date getRecoveryDate()
        {
            return recoveryDate;
        }


        public void setRecoveryDate( Date recoveryDate )
        {
            this.recoveryDate = recoveryDate;
        }


        public BigDecimal getRecoveryAmount()
        {
            return recoveryAmount;
        }


        public void setRecoveryAmount( BigDecimal recoveryAmount )
        {
            this.recoveryAmount = recoveryAmount;
        }


        public BigDecimal getLawyerFee()
        {
            return lawyerFee;
        }


        public void setLawyerFee( BigDecimal lawyerFee )
        {
            this.lawyerFee = lawyerFee;
        }


        public BigDecimal getNetRecovery()
        {
            if ( recoveryAmount != null && lawyerFee != null )
            {
                return recoveryAmount.subtract( lawyerFee );
            }
            return netRecovery;
        }


        public void setNetRecovery( BigDecimal netRecovery )
        {
            this.netRecovery = netRecovery;
        }


        public Date getWaiverLetterSent()
        {
            return waiverLetterSent;
        }


        public void setWaiverLetterSent( Date waiverLetterSent )
        {
            this.waiverLetterSent = waiverLetterSent;
        }


        public Date getLawyerLetterSent()
        {
            return lawyerLetterSent;
        }


        public void setLawyerLetterSent( Date lawyerLetterSent )
        {
            this.lawyerLetterSent = lawyerLetterSent;
        }


        public Date getMoreInfoLetterSent()
        {
            return moreInfoLetterSent;
        }


        public void setMoreInfoLetterSent( Date moreInfoLetterSent )
        {
            this.moreInfoLetterSent = moreInfoLetterSent;
        }


        public Date getWaiverLetterFollowUp()
        {
            return waiverLetterFollowUp;
        }


        public void setWaiverLetterFollowUp( Date waiverLetterFollowUp )
        {
            this.waiverLetterFollowUp = waiverLetterFollowUp;
        }


        public Date getLawyerLetterFollowUp()
        {
            return lawyerLetterFollowUp;
        }


        public void setLawyerLetterFollowUp( Date lawyerLetterFollowUp )
        {
            this.lawyerLetterFollowUp = lawyerLetterFollowUp;
        }


        public Date getMoreInfoFollowUp()
        {
            return moreInfoFollowUp;
        }


        public void setMoreInfoFollowUp( Date moreInfoFollowUp )
        {
            this.moreInfoFollowUp = moreInfoFollowUp;
        }


        public Date getInsuranceLetterSent()
        {
            return insuranceLetterSent;
        }


        public void setInsuranceLetterSent( Date insuranceLetterSent )
        {
            this.insuranceLetterSent = insuranceLetterSent;
        }


        public Date getInsuranceLetterFollowUp()
        {
            return insuranceLetterFollowUp;
        }


        public void setInsuranceLetterFollowUp( Date insuranceLetterFollowUp )
        {
            this.insuranceLetterFollowUp = insuranceLetterFollowUp;
        }


        public Date getSummaryProvidedSent()
        {
            return summaryProvidedSent;
        }


        public void setSummaryProvidedSent( Date summaryProvidedSent )
        {
            this.summaryProvidedSent = summaryProvidedSent;
        }


        public Date getSummaryProvidedFollowUp()
        {
            return summaryProvidedFollowUp;
        }


        public void setSummaryProvidedFollowUp( Date summaryProvidedFollowUp )
        {
            this.summaryProvidedFollowUp = summaryProvidedFollowUp;
        }


        public String getComment()
        {
            return comment;
        }


        public void setComment( String comment )
        {
            this.comment = comment;
        }


        /**
         * @return the subscriberSelected
         */
        public boolean isSelected()
        {
            return selected;
        }


        public Set<FollowUp> getFollowUps()
        {
            return followUps;
        }


        public void setFollowUps( Set<FollowUp> followUps )
        {
            this.followUps = followUps;
        }


        public boolean isDateFileClosed()
        {

            if ( this.recoveryDate != null && recoveryDate.before( new Date() ) )
            {

                return true;
            }

            return false;
        }


        /**
         * @param subscriberSelected the subscriberSelected to set
         */
        public void setSelected( boolean subscriberSelected )
        {
            this.selected = subscriberSelected;
        }


        public String toString()
        {
            StringBuffer buffer = new StringBuffer();
            buffer.append( "Selected Subscriber[" );
            buffer.append( " ID: " ).append( id );
            buffer.append( " ,dateFileOpened: " ).append( dateFileOpened ).append( '\n' );
            buffer.append( " ,groupID: " ).append( groupID ).append( '\n' );
            buffer.append( ",section: " ).append( section ).append( '\n' );
            buffer.append( ", series: " ).append( series ).append( '\n' );
            buffer.append( " ,province: " ).append( province ).append( '\n' );
            buffer.append( ", extraInfo2: " ).append( extraInfo2 ).append( '\n' );

            buffer.append( ", subID: " ).append( subID ).append( '\n' );
            buffer.append( ", firstName: " ).append( firstName ).append( '\n' );
            buffer.append( ", lastName: " ).append( lastName ).append( '\n' );
            buffer.append( ", birthDate: " ).append( birthDate ).append( '\n' );
            buffer.append( ", extraInfo1: " ).append( extraInfo1 ).append( '\n' );
            buffer.append( ", accidentDate: " ).append( accidentDate ).append( '\n' );
            buffer.append( ", summaryDate: " ).append( summaryDate ).append( '\n' );
            buffer.append( ", summaryAmount: " ).append( summaryAmount ).append( '\n' );

            buffer.append( ", recoveryDate: " ).append( recoveryDate ).append( '\n' );
            buffer.append( ", recoveryAmount: " ).append( recoveryAmount ).append( '\n' );
            buffer.append( ", lawyerFee: " ).append( lawyerFee ).append( '\n' );
            buffer.append( ", netRecovery: " ).append( netRecovery ).append( '\n' );
            buffer.append( ", waiverLetterSent: " ).append( waiverLetterSent ).append( '\n' );
            buffer.append( ", lawyerLetterSent: " ).append( lawyerLetterSent ).append( '\n' );
            buffer.append( ", moreInfoLetterSent: " ).append( moreInfoLetterSent ).append( '\n' );

            buffer.append( ", waiverLetterFollowUp: " ).append( waiverLetterFollowUp ).append( '\n' );
            buffer.append( ", lawyerLetterFollowUp: " ).append( lawyerLetterFollowUp ).append( '\n' );
            buffer.append( ", moreInfoFollowUp: " ).append( moreInfoFollowUp ).append( '\n' );
            buffer.append( ", insuranceLetterSent: " ).append( insuranceLetterSent ).append( '\n' );
            buffer.append( ", insuranceLetterFollowUp: " ).append( insuranceLetterFollowUp ).append( '\n' );
            buffer.append( ", summaryProvidedSent: " ).append( summaryProvidedSent ).append( '\n' );
            buffer.append( ", summaryProvidedFollowUp: " ).append( summaryProvidedFollowUp ).append( '\n' );
            buffer.append( ", comment: " ).append( comment );

            buffer.append( " ]" );
            return buffer.toString();
        }


        /**
         * Equal method
         * 
         * **/
        public boolean equals( Object o )
        {

            if ( ! ( o instanceof Subscriber ) )
            {
                return false;
            }

            return ( this.id == ( (Subscriber) o ).getId() );
        }

    }

}
