
package ca.medavie.subrogation.ui.security;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import ca.bluecode.logging.Log;

/**
 * The subrogation specific implementation of Filter, used for security
 */
public class SubrogationSecurityFilter implements Filter
{
    /** 
     * An array of valid CashFlow role names 
     * 
     */
//    private static final String[] roleNames =
//    { "SubrogationAdminRole" };


    /*
     * (non-Javadoc)
     * 
     * @see javax.servlet.Filter#destroy()
     */
    public void destroy()
    {
        Log.logDebug( this.getClass(), "destroy", "SubrogationSecurityFilter destroy called." );
    }


    /*
     * (non-Javadoc)
     * 
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
     * javax.servlet.ServletResponse, javax.servlet.FilterChain)
     */
    public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException,
            ServletException
    {     

        if ( request instanceof HttpServletRequest )
        {
            HttpServletRequest req = (HttpServletRequest) request;
            Principal principal = req.getUserPrincipal(); //assumed not null through authentication
            SecurityContext ctx = SecurityContextHolder.getContext();

            // Here we're checking to see if the Spring SecurityContext already has an Authentication whose principal
            // is different from that of the Principal in the HttpServletRequest.  If it is then we�re going to log
            // an error. We're doing this because there was a bug (JUWA-333) where we wouldn't update the Spring SecurityContext
            // Authentication if it was already set.  This would mean that within a given request it was possible to
            // have credentials and authorized roles in the Spring SecurityContext Authentication that were different
            // from the HttpServletRequest's Principal.  By doing this check and logging the error we'll be able to
            // determine how often this would have been happening.
            if ( ctx != null && ctx.getAuthentication() != null && ctx.getAuthentication().getPrincipal() != null
                && !principal.getName().equals( ctx.getAuthentication().getPrincipal() ) )
            {

                Log.logError( this.getClass(), "doFilter",
                    "JUWA-333 - the old user is: " + principal.getName() + " The Current logged in user is: "
                        + ( (String) ctx.getAuthentication().getPrincipal() ));

            }

            // Always create new Spring Authentication Context based upon the Servlet Request Principal. No matter what!
            ctx.setAuthentication( new PreAuthenticatedAuthenticationToken( principal.getName(), "credentials",
                getGrantedAuthority( req ) ) );
        }
        else
        {
            Log.logInfo( this.getClass(), "doFilter", "Request was not an instance of HttpServletRequest" );
            throw new IllegalArgumentException( "Only HttpServletRequest is Acceptable" );

        }
        chain.doFilter( request, response );

    }


    /**
     * Returns a list of user granted authority objects.
     * 
     * @return List<GrantedAuthority> The grants
     */
    private List<GrantedAuthority> getGrantedAuthority( HttpServletRequest request )
    {
        List<GrantedAuthority> grants = new ArrayList<GrantedAuthority>();
        for ( SubrogationSecurityRole securityRole : SubrogationSecurityRole.values() )
        {
        	String role = securityRole.name();
        	System.err.println("look for role == "+ role);
        	
            Log.logDebug( this.getClass(), "getGrantedAuthority", "roleName is  " + role);
            if ( request.isUserInRole( role ) )
            {
                Log.logDebug( this.getClass(), "getGrantedAuthority", "user is in role " + role );
                GrantedAuthorityImpl grant = new GrantedAuthorityImpl( "ROLE_" + role );
                grants.add( grant );
            }
        }
        Log.logDebug( this.getClass(), "getGrantedAuthority", "grants are  " + grants.size() );
        return grants;
    }


    /*
     * (non-Javadoc)
     * 
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    public void init( FilterConfig arg0 ) throws ServletException
    {
        Log.logDebug( this.getClass(), "init", "SubrogationSecurityFilter init called." );
    }

}
