
package ca.medavie.subrogation.ui.security;

import java.lang.reflect.Method;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import ca.bluecode.security.ISecurityManager;
import ca.bluecode.security.User;
import ca.medavie.uwa.businessowner.BusinessOwnerService;
import javax.servlet.http.HttpServletRequest;

/**
 * This implementation of the ISecurityManager interface uses JSF FacesContext
 * to determine the currently active user. The implementation is intended solely
 * for the subrogation application
 * 
 * @see ca.bluecode.security.ISecurityManager
 */
public class SubrogationSecurityManager implements ISecurityManager {

	private static final Logger logger = Logger.getLogger(SubrogationSecurityManager.class);
	/**
	 * the currently active Subrogation User
	 * 
	 */
	private SubrogationUser user;

	/**
	 * a list of roles belonging to the current user
	 * 
	 */
	private List<String> roles = new ArrayList<String>();

	/**
	 * the BusinessOwnerService used to populate the user's list of available
	 * Business Owners/Control Plans.
	 * 
	 * 
	 * /** Base public constructor
	 * 
	 */
	public SubrogationSecurityManager() {
	}

	/**
	 * This method will return a User object for the currently active user.
	 * 
	 * @return User - returns the current user
	 * @see ca.bluecode.security.ISecurityManager#getUser()
	 */
	public User getUser() {
		if (this.user == null) {
			ExternalContext ext = FacesContext.getCurrentInstance().getExternalContext();
			Principal p = ext.getUserPrincipal();
			user = new SubrogationUser();

			if (p == null) {
				logger.debug("Principal was null");
			} else {
				logger.debug(" Principal name = [" + p.getName() + "]");
				user.setUserId(p.getName());
			}

		}
		return this.user;
	}

	/**
	 * Returns a List of role names for the currently active user as Strings.
	 * 
	 * @return List - role names as Strings
	 * @see ca.bluecode.security.ISecurityManager#getRoles()
	 */
	public List<String> getRoles() {
		logger.debug("executed");
		if (roles.size() == 0) {
			SecurityContext ctx = SecurityContextHolder.getContext();
			Collection<? extends GrantedAuthority> auths = ctx.getAuthentication().getAuthorities();

			for (GrantedAuthority auth : auths) {
				if (auth.getAuthority().equals("ROLE_SubrogationAdminRole")) {
					roles.add("SubrogationAdminRole");
				}
			}
		}
		return roles;
	}

	/**
	 * Decides it a user can execute a method based on their roles.
	 * 
	 * @param method
	 *            - fully qualified method name in the form
	 *            package.subpackage1.subpackagen.class.method
	 * @return true if the user can execute the given method, false if they cannot
	 * @see ca.bluecode.security.ISecurityManager#canDo(java.lang.String)
	 */
	public boolean canDo(String roleName) {
		logger.debug("executed");
		String role = roleName.substring(roleName.lastIndexOf('.') + 1);
		boolean hasRole = hasRole(role);
		logger.debug("Has Role: " + hasRole);

		return hasRole;
	}

	/**
	 * Decides it a user can execute a method based on their roles.
	 * 
	 * @param method
	 *            - Method object for the method we are testing
	 * @return true if the user can execute the given method, false if they cannot
	 * @see ca.bluecode.security.ISecurityManager#canDo(java.lang.reflect.Method)
	 */
	public boolean canDo(Method method) {
		boolean result = false;
		String className = method.getDeclaringClass().getName();
		String methodName = method.getName();
		String fullName = className + "." + methodName;
		result = this.canDo(fullName);
		return result;
	}

	/**
	 * Tests to see if the currently active user has a specific role
	 * 
	 * @param roleCN
	 *            - role name we are testing to see if the user has
	 * @return boolean - true if the user has the given role, false if they do not
	 * @see ca.bluecode.common.ui.security.ISecurityManager#hasRole(java.lang.String)
	 * @see ca.bluecode.security.ISecurityManager#hasRole(java.lang.String)
	 */
	public boolean hasRole(String roleString) {
		logger.debug("executed");
		
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();

		boolean result = request.isUserInRole(roleString);

		System.err.println("res " + result);

		// boolean result = false;
		//
		// List<String> roles = this.getRoles();
		// if ( roles != null )
		// {
		// result = roles.contains( roleString );
		// }
		return result;
	}

	/** revision start **/

	public boolean hasSubrogationAdminRole() {
		
		return hasRole(SubrogationSecurityRole.SubrogationAdminRole.name());
		
	}

}
