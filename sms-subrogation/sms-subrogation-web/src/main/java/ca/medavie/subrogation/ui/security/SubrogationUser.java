
package ca.medavie.subrogation.ui.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.faces.model.SelectItem;

import ca.bluecode.security.User;
import ca.medavie.uwa.businessowner.BusinessOwner;

/**
 * Class customizing User for front end use in the Subrogation application
 */
public class SubrogationUser extends User
{

    /**
     * A map containing Lists keys by Locale. The lists contain localized
     * SelectItems of BusinessOwner descriptions and codes. These lists are
     * based on the Business Owners available to the user.
     */
    private Map<String, List<SelectItem>> controlPlanSelectItemsMap;

    /**
     * A map of Business Owners available to the owner.
     * 
     */
    private Map<String, BusinessOwner> controlPlanMap;


    /**
     * Find a business owner by code
     * 
     * @param code
     * @return
     */
    public BusinessOwner findBusinessOwner( String code )
    {
        if ( code == null )
        {
            throw new IllegalArgumentException( "code cannot be null" );
        }

        BusinessOwner bo = controlPlanMap.get( code );
        return bo;
    }


    /**
     * @param controlPlans
     *            The controlPlans to set - a collection of BusinessOwner
     *            objects.
     */
    public void setControlPlans( Collection<BusinessOwner> businessOwners )
    {
        List<SelectItem> controlPlansEn = new ArrayList<>();
        List<SelectItem> controlPlansFr = new ArrayList<>();

        controlPlanSelectItemsMap = new HashMap<>();

        controlPlanMap = new HashMap<>();

        for ( BusinessOwner bo : businessOwners )
        {
            SelectItem selectItemEn = new SelectItem( bo.getCode(),
                bo.getDescription( Locale.CANADA ).getDescription() );
            controlPlansEn.add( selectItemEn );

            SelectItem selectItemFr = new SelectItem( bo.getCode(),
                bo.getDescription( Locale.CANADA_FRENCH ).getDescription() );
            controlPlansFr.add( selectItemFr );

            controlPlanMap.put( bo.getCode(), bo );
        }

        controlPlanSelectItemsMap.put( Locale.CANADA.getLanguage(), controlPlansEn );
        controlPlanSelectItemsMap.put( Locale.CANADA_FRENCH.getLanguage(), controlPlansFr );
    }


    /**
     * @return Returns deep copy of the controlPlanSelectItemsMap.
     */
    public Map<String, List<SelectItem>> getControlPlanSelectItemsMap()
    {
        if ( controlPlanSelectItemsMap == null )
        {
            controlPlanSelectItemsMap = new HashMap<>();
        }

        return new HashMap<>( controlPlanSelectItemsMap );
    }


    /**
     * @param controlPlanSelectItemsMap
     *            The controlPlanSelectItemsMap to set.
     */
    public void setControlPlanSelectItemsMap( Map<String, List<SelectItem>> controlPlanSelectItemsMap )
    {
        this.controlPlanSelectItemsMap = controlPlanSelectItemsMap;
    }


    /**
     * @return Returns the controlPlanMap.
     */
    public Map<String, BusinessOwner> getControlPlanMap()
    {
        return controlPlanMap;
    }


    /**
     * @param controlPlanMap
     *            The controlPlanMap to set.
     */
    public void setControlPlanMap( Map<String, BusinessOwner> controlPlanMap )
    {
        this.controlPlanMap = controlPlanMap;
    }
}
