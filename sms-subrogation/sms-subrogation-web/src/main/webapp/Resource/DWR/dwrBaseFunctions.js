function dwrDoNothingErrorHandler()
{
}

function dwrDefaultResponseProcessor (data)
{
}

function deregisterCurrentWindow()
{
	Environment.deregisterWindow(window.name, logOutFromAppResult);
}

function registerCurrentWindow()
{
	Environment.registerWindow(window.name, dwrDefaultResponseProcessor);
}

function deregisterWindowOnBrowserClose()
{
	if ((typeof window.event.clientX == 'undefined' || window.event.clientX < 0 )
	     && (typeof window.event.clientY == 'undefined' || window.event.clientY < 0))
	{ 
		deregisterCurrentWindow();    
	} 
} 
