var DW_ERROR_LABEL = "Error";
var DW_WARNING_LABEL = "Warning";
var DW_INFO_LABEL = "Information";

// button labels
var DW_CLOSE_LABEL = "Close";
var DW_LOGOUT_CONFIRM_LABEL = "Yes - Logout";
var DW_LOGOUT_CANCEL_LABEL = "No - Cancel";

// String constants
var DW_PLEASE_WAIT_MSG = "Please wait...";
var DW_SEARCH_IN_PROGRESS_MSG = "Search in progress...";
var DW_REP_GEN_IN_PROGRESS_MSG = "The selected report is being generated...";
var DW_LOGOUT_MSG = "Are you sure you want to quit the application ?";
