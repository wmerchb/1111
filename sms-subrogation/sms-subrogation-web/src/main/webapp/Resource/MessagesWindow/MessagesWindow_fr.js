var DW_ERROR_LABEL = "Erreur";
var DW_WARNING_LABEL = "Attention";
var DW_INFO_LABEL = "Information";

// button labels
var DW_CLOSE_LABEL = "Fermer";
var DW_LOGOUT_CONFIRM_LABEL = "Oui - Quitter";
var DW_LOGOUT_CANCEL_LABEL = "Non - Annuler";

// String constants
// To use Unicode characters, use the static method fromCharCode(value) of the class String.
// Ex to get a capital N with a tilde (unicode: 0x00D1): 
// var aString="blabla"+String.fromCharCode(0x00D1)+"end of blabla";
var DW_PLEASE_WAIT_MSG = "Veuillez patientez s'il-vous-pla"+String.fromCharCode(0x00EE)+"t";
var DW_SEARCH_IN_PROGRESS_MSG = "Recherche en cours...";
var DW_REP_GEN_IN_PROGRESS_MSG = "Le rapport s"+String.fromCharCode(0x00E9)+"lectionn"+String.fromCharCode(0x00E9)+" est en cours de cr"+String.fromCharCode(0x00E9)+"ation...";
var DW_LOGOUT_MSG = String.fromCharCode(0x00CA)+"tes-vous s"+String.fromCharCode(0x00FB)+"r de vouloir quitter l'application ?";