/* 
 * Standard JavaScript functions to build the menu with the default brand.
 */


/* 
 * Setting the default brand values found in the banner.
 */

brandLogoURL      = "http://www.atl.bluecross.ca/website.nsf/Pages/Home"
brandHomeURL   = "/sms-uwa-web/welcome.do"
brandHomeURL_en   = "/sms-uwa-web/welcome.do?locale=en";
brandHomeURL_fr   = "/sms-uwa-web/welcome.do?locale=fr";
brandEmailAddress = "mailto:bc_utilization@medavie.bluecross.ca"
brandLogoutURL    = "logOutFromApp();"
brandHelp         = "" 


   function BrandLogoURL ()
   {
      location.href=brandLogoURL;
   }

   function BrandHome ()
   {
      window.location=brandHomeURL;
   }
   
   function BrandHome_en ()
   {
      window.location=brandHomeURL_en;
   }
   
   function BrandHome_fr ()
   {
      window.location=brandHomeURL_fr;
   }
   
   function BrandEmail ()
   {
      window.location=brandEmailAddress;
   }
      
   function BrandLogout ()
   {
      logOutFromApp();
   }
   
   function BrandHelp ()
   {
      window.location=brandHelp;
   }
   function logOutFromAppResult(data)
   {
	   window.close();
   }

   function logOutFromApp()
   {
	   	dw = new MessagesWindow(DW_WARNING_WINDOW, DW_MEDIUM_SIZE);
  		dw.addText(DW_LOGOUT_MSG);
  		dw.setTextAlignment(DW_ALIGN_LEFT);
  		dw.addButtonNoAction(DW_LOGOUT_CANCEL_LABEL);
  		dw.addButton(DW_LOGOUT_CONFIRM_LABEL,'invalidateSession()');
  		dw.setButtonAlignment(DW_ALIGN_CENTER);
  		dw.display();
	   
   }
   

   function invalidateSession()
   {
	   // default implementation is to use DWR to make a call to the environment to 
	   // invalidate the session
	   DWREngine.setErrorHandler(dwrDoNothingErrorHandler);
	   DWREngine.setWarningHandler(dwrDoNothingErrorHandler);
	
	   deregisterCurrentWindow(window.name, logOutFromAppResult);
   }    
   