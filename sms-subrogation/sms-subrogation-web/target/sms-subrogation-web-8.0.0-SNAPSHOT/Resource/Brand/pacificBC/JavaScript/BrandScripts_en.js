/** 
 * Standard JavaScript functions to build the menu brand specific for the English Local. All
 entries will override the following default settings:
 
						brandLogoURL      = "home_url=http://www.atl.bluecross.ca/website.nsf/Pages/Home"
						brandWelcomeURL   = "http://www.medavie.bluecross.ca";
						brandEmailAddress = "mailto:nobody@acme.domain"
						brandLogoutURL    = "logOutFromApp();"
						brandHelp         = "" 

 
 * Override the above brand values for the English Local.
 */
brandEmailAddress = "mailto:marc.arsenault@medavie.bluecross.ca"


