/* This set of methods has been copied and enhanced from http://www.clagnut.com/sandbox/imagefades/ by Herve Seger (November 2005)*/
/* This library depends on x_core.js (part of the X library) for the variables xIE4Up and xMoz. */
/* The X library can be downloaded for free at www.cross-browser.com (try it, you'll love it) */

function setOpacity(divId, opacity) 
{
  opacity = (opacity == 100)?99.999:opacity; /* Done to avoid flicking effect in Mozilla-based browsers */
  opacity = (opacity < 0)?0:opacity; 
  
  var obj = document.getElementById(divId);
  // IE/Win
  if (xIE4Up)
  {
	obj.style.filter = "alpha(opacity:"+opacity+")";
  }
  if (xMoz)
  {
    // Older Mozilla and Firefox
    obj.style.MozOpacity = opacity/100;
    // Safari 1.2, newer Firefox and Mozilla, CSS3
    obj.style.opacity = opacity/100;
  }
}

function getOpacity(divId)
{
	if (xIE4Up)
	{
		var filter = document.getElementById(divId).style.filter;
        if (filter == "")
        {
            return(-1);
        }
		var i = filter.indexOf(":");
		var j = filter.indexOf(")");
		return(parseInt(filter.substr(i+1, j-i-1)));
	}
	if (xMoz)
	{	
        var opacity = document.getElementById(divId).style.opacity;
        if (opacity == "")
        {
            return(-1);
        }
		return(parseInt(opacity*100));
	}
}

function fadeManager(objId, opacity, beforeEffectFunction, afterEffectFunction)
{
    /* Determines what fade to apply (in or out) */ 
    
    if (beforeEffectFunction != '')
    {
        eval(beforeEffectFunction);
    }
    
    var currentOpacity = getOpacity(objId);
    if (currentOpacity == -1)
    {
        setOpacity(objId, 100);
    }
    
    /* IE Bug: the opacity cannot be set up if there is no height or width property */
    if ((document.getElementById(objId).style.height == "") && (xIE4Up))
    {
        document.getElementById(objId).style.height = xHeight(objId);
    }
    
    if (getOpacity(objId) < opacity)
    {
        fadeIn(objId, opacity, afterEffectFunction);
    }
    else
    {
        fadeOut(objId, opacity, afterEffectFunction);
    }
}

function fadeIn(objId,opacity, afterEffectFunction) 
{
  if (document.getElementById) 
  {
    obj = document.getElementById(objId);
	var currentOpacity = getOpacity(objId);
	currentOpacity += 10;
    if (currentOpacity < opacity) 
	{
	  setOpacity(objId, currentOpacity);
      var timeOutCmd = "fadeIn('"+objId+"', "+opacity+", '"+afterEffectFunction+"')";
      window.setTimeout(timeOutCmd, 30);
    }
	else
	{
		setOpacity(objId, opacity);
		window.clearTimeout();
        if (afterEffectFunction != '')
        {
            eval(afterEffectFunction);
        }
	}
  }
}

function fadeOut(objId, opacity, afterEffectFunction) 
{
  if (document.getElementById) 
  {
    obj = document.getElementById(objId);
	var currentOpacity = getOpacity(objId);
	
    if (currentOpacity > opacity) 
	{
      currentOpacity -= 10;
	  setOpacity(objId, currentOpacity);
      var timeOutCmd = "fadeOut('"+objId+"', "+opacity+", '"+afterEffectFunction+"')";
      window.setTimeout(timeOutCmd, 30);
    }
	else
	{
        /* End of the fade out process. The object is hidden */
		window.clearTimeout();
        if (afterEffectFunction != '')
        {
            eval(afterEffectFunction);
        }
	}
  }
}
