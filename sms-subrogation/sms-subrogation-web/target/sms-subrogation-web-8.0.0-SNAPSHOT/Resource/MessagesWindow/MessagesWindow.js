/*
* MessagesWindow version 1.2 - Medavie Blue Cross at www.atl.bluecross.ca
*	This class uses X library (www.cross-browser.com) which is driven by GNU/LGPL license.
*	Therefore and because of the rules of the GNU/LGPL license, this class is also driven by GNU/LGPL license.
*	It can be used, copied, distributed under the same terms as the ones defined in GNU/LGPL license. 
*
* This class allows you to create:
* - Warning dialog boxes (with or without buttons)
* - Error dialog boxes (with or without buttons)
* - Information boxes (with or without buttons)
*         Version 1.2 (22-11-2005):
*                    - Added a fade in/fade out effect to the generated object.
*                    - Changed the use of isIE variable to use xIE4Up (from X Library) instead.
*                    - The bullet is displayed only in error windows.
*                    - Fixed a bug: when no positionning was specified, the window was not displayed at the center of the screen.
*	Version 1.1: 
*		- setWindowSize updated to resize automatically the container (pb in FF).
*		- Changed the names and content of CSS classes
*		- Window can be positionned beside an HTML Element
*		- Window can be dragged and dropped
*	version 1.0:
*		- Initial release.
*/

// External Constants to be used in the calling javascript code
var DW_ERROR_WINDOW = 1;
var DW_WARNING_WINDOW = 2;
var DW_INFO_WINDOW = 3;

var DW_SMALL_SIZE = 100;  		//in pixels
var DW_MEDIUM_SIZE = 300;
var DW_BIG_SIZE = 600;

var DW_ALIGN_LEFT = 1;
var DW_ALIGN_CENTER = 2;
var DW_ALIGN_RIGHT = 3;

var DW_TIMEOUT = 5; 			// in seconds

var TOPCENTER   = 'topCenter';
var BOTTOMRIGHT = 'bottomRight';
var TOPRIGHT    = 'topRight';
var BOTTOMLEFT  = 'bottomLeft';
var TOPLEFT     = 'topLeft';
var CENTER      = 'center';

var DW_FADE_STEP = 10;			// Opacity step
var DW_FADE_INTERVAL = 30;      // in milliseconds

function MessagesWindow (windowType, windowSize)
{
	// Constants (Size)
	this.LOW_SIZE = 100;
	this.MEDIUM_SIZE = 300;
	this.HIGH_SIZE = 600;
	
	// Constants (Window type)
	this.ERROR_WINDOW = 1;
	this.WARNING_WINDOW = 2;
	this.INFO_WINDOW = 3;
	
	// Constants (Alignment of buttons & text)
	this.ALIGN_LEFT = 1;
	this.ALIGN_CENTER = 2;
	this.ALIGN_RIGHT = 3;
	
	// Attributes of the object
	this.wt = 0; 					//Window Size
	this.ws = 0;					//Window Type
	this.text = new Array();			//List of error messages
	//this.buttonNoAction = DW_CLOSE_LABEL;			//Text of the 'button with no action' - Typically, a 'Close' button.
	this.buttonNoAction = '';			//Text of the 'button with no action' - Typically, a 'Close' button.
	this.buttonList = new Array();	//List of buttons with actions (javascript functions or URL).
	this.buttonAlignment = DW_ALIGN_RIGHT;		//Specifies the alignment of buttons at the bottom of the window. 3 values: ALIGN_LEFT, ALIGN_CENTER and ALIGN_RIGHT
	this.timeout = '';				//Number of seconds before the window automatically hides
	this.textAlignment = DW_ALIGN_LEFT;			//Specifies the alignment of error messages. 3 values: ALIGN_LEFT, ALIGN_CENTER and ALIGN_RIGHT
	this.posElement = '';			//Specifies the name of the HTML element where the window will be positionned
	this.posQuadrant = '';				//Specifies the quadrant in relation to the HTML element where the window will be positionned.
	this.fadeStep = DW_FADE_STEP;			//Increment for the opacity attribute
	this.fadeInterval = DW_FADE_INTERVAL;	//Sets up the interval in milliseconds to fade in/out
		
	// External Methods
	this.addText = addTextDW;
	this.addErrorMessage = addErrorMessageDW;
	this.display = displayDW;
	this.hide = hideDW;
	this.addButtonNoAction = addButtonNoActionDW;
	this.addButton = addButtonDW;
	this.setButtonAlignment = setButtonAlignmentDW;
	this.setTimeout = setTimeoutDW;
	this.setTextAlignment = setTextAlignmentDW;
	this.setPosition = setPositionDW;
	this.positionWindow=positionWindowDW;
    this.activateDialogWindow = activateDialogWindowDW;
    this.setFadeStep = setFadeStepDW;
    this.setFadeInterval = setFadeIntervalDW;
	
	// Internal (private) Methods
	this.setWindowType = setWindowTypeDW;
	this.setWindowSize = setWindowSizeDW;
	this.createHeader = createHeaderDW;
	this.createContent = createContentDW;
	this.createFooter = createFooterDW;
	this.setContainerSize = setContainerSizeDW;
	this.setContainerHeightSize = setContainerHeightSizeDW;
	
	/* Internal variables */
	numberOfButtons = 0;   // Number of buttons WITH actions (you can have solely ONE button with no action)
	

	/* Constructor 
	*
	* Checks that the DIV DWId exists in the current page.
	* If not, the div is created automatically.
	* Sets the window type and the window size of the object
	*
	*/ 

	if (xGetElementById('DWId') == null)
  	{
  		/* The div element does not exist - it will be created */
    	var bodyNode = document.getElementsByTagName('body')[0];
    	var divNode = document.createElement('div');
    	var idNode = document.createAttribute('id');
    	idNode.value = 'DWId';
    	divNode.setAttributeNode(idNode);
    	bodyNode.appendChild(divNode); 
  	}	
	
	if (windowType == undefined)
	{
	   this.setWindowType(this.ERROR_WINDOW);	
/*		alert("Window Type -> parameter mandatory");		
		return;
*/		
	}
	if (windowType != this.ERROR_WINDOW && windowType != this.WARNING_WINDOW && windowType != this.INFO_WINDOW)
	{
		alert("Window Type -> invalid parameter ");
		return;
	}
	
	if (windowSize == undefined)
	{
		// No size provided, use the standard size
		this.setWindowSize(this.MEDIUM_SIZE);
	}
	else
	{
		this.setWindowSize(windowSize);
	}

    this.setWindowType(windowType);

}
	

function setWindowSizeDW(value)
{
	this.ws = value;
	
	/* resizes the container to avoid horizontal scrolling bars with FF */
	this.setContainerSize(this.ws);
}

function setWindowTypeDW(type)
{
	this.wt = type;
}

function setPositionDW(element,quadrant)
{	
	this.posElement = element;
	this.posQuadrant = quadrant;
}

function setContainerSizeDW(sizeInPixels)
{
	xWidth('DWId',sizeInPixels);
}

function setContainerHeightSizeDW(sizeInPixels)
{
	xHeight('DWId',sizeInPixels);
}

function setFadeStepDW(intStep)
{
	this.fadeStep = intStep;
}

function setFadeIntervalDW(milliseconds)
{
	this.fadeInterval = milliseconds;
}

/* addText
*
* Adds a new string (constant) to the current dialog box
* Input: 	String text to insert into the current array of strings
* 
*/
function addTextDW(newText)
{
	this.text.push(newText);
}


/* addErrorMessage
* Input:	errorText -> error message to display on the window
*/

function addErrorMessageDW(errorText)
{
	var sentence = new String();
	
	sentence = '<li class="DWErrorText">'+errorText+'</li>';
	this.text.push(sentence);
}


/* addButtonNoAction
*
* Creates a buttton with no action associated to it.
* Example: usually a CLOSE button just hides the window and does not execute any javascript function
* Input: 	buttonText -> text displayed in the button
*/
function addButtonNoActionDW(buttonText)
{
	if (this.timeout != '')
	{
		alert ('MessagesWindow.addButton -> Timeout and Buttons are mutually exclusive');
		return;
	}
	
	if (this.buttonNoAction == '')
	{
		this.buttonNoAction = buttonText;
	}
	else
	{
		alert('MessagesWindow.addButtonNoAction -> You cannot define more than ONE \'No action button\'');
		return;
	}
}


/* addButton
*
* Creates a buttton with an action associated to it.
* Example: usually an OK button triggers some code to be executed.
* Input: 	buttonLabel -> label of the button
*               jsFunction -> name of the javascript function to execute when the button is clicked
*/
function addButtonDW(buttonLabel, jsFunction)
{
	if (this.timeout != '')
	{
		alert ('MessagesWindow.addButton -> Timeout and Buttons are mutually exclusive');
		return;
	}
	this.buttonList[numberOfButtons] = new Button(buttonLabel, jsFunction);
	numberOfButtons ++;
}


/*
*
* This method defines the way buttons are aligned at the bottom of the dialog box: CENTER or RIGHT.
* If not specified, the buttons will be centered.
*
*/
function setButtonAlignmentDW(alignment)
{
	if (alignment != this.ALIGN_LEFT && alignment != this.ALIGN_CENTER && alignment != this.ALIGN_RIGHT)
	{
		alert ('MessagesWindow.setButtonAlignment -> the alignment parameter specified is invalid.');
		return;
	}
	this.buttonAlignment = alignment;
}


/*
*
* Creates the HTML Code for the header 
*
*/
function createHeaderDW()
{
	var title = new String();
	var HTMLCode = new String();
	var fontColor = new String();
	
	/* Builds the header */
	HTMLCode = '<table border=0 cellspacing=0 id="DWTable" cellpadding=0 class="DWBox" style="width: '+this.ws+'px;">';
	HTMLCode += '<tr>';
	switch (this.wt)
	{
		case this.ERROR_WINDOW:
			HTMLCode += '<td class="DWErrorHeader">'+DW_ERROR_LABEL;
			break;
		case this.WARNING_WINDOW:
			HTMLCode += '<td class="DWWarningHeader">'+DW_WARNING_LABEL;
			break;
		case this.INFO_WINDOW:
			HTMLCode += '<td class="DWInfoHeader">'+DW_INFO_LABEL;
			break;
		default:
			alert ('Unknown window type - STOP');
			return;
	}
	HTMLCode += '</tr>';
	
	return HTMLCode;
}


/*
*
* Creates the HTML Code for the content 
*
*/
function createContentDW()
{
	var HTMLCode = new String();
	
	for (i=0; i < this.text.length; i++)
	{
		HTMLCode += '<tr><td class="'
		if (this.textAlignment == '')
		{
			HTMLCode += 'DWTextLeft"';
		}
		else
		{
			switch (this.textAlignment)
			{
				case this.ALIGN_LEFT:
					HTMLCode += 'DWTextLeft"';
					break;
				case this.ALIGN_CENTER:
					HTMLCode += 'DWTextCenter"';
					break;
				case this.ALIGN_RIGHT:
					HTMLCode += 'DWTextRight"';
					break;
			}
		}
		
		HTMLCode += '>'+ this.text[i]+'</td></tr>';
	}
	return HTMLCode;
}


/*
*
* Creates the HTML Code for the footer 
*
*/
function createFooterDW()
{
	var HTMLCode = new String();

	/* Align the buttons(s) */
	HTMLCode += '<tr><td class="DWButtonBar ';
	if (this.buttonAlignment == this.ALIGN_RIGHT)
	{
		HTMLCode += 'DWTextAlignRight"';
	}
	else
	{
		HTMLCode += 'DWTextAlignCenter"';
	}
	HTMLCode += '>';
	
	/* Loops through the array of button(s) to display them */	
	for (i=0; i<this.buttonList.length;i++)
	{
		HTMLCode += '<a class="DWButton" href="javascript:';
		HTMLCode += this.buttonList[i].getJSFunction()+'">';
		HTMLCode += this.buttonList[i].getButtonLabel()+'</a>';
	}
	if (this.buttonNoAction != '')
	{
		HTMLCode += '<a class="DWButton" href="javascript:hideDW('+this.fadeStep+', '+this.fadeInterval+')">'+this.buttonNoAction+'</a>';
	}

	HTMLCode += '</td></tr></table>';
	
	return HTMLCode;
}


/*
*
* Called by to activate the window before the fade in process
*
*/
function activateDialogWindowDW()
{
    setOpacity('DWId', 0);
    if (xIE4Up)
    {
        xShow('DWiframe');
    }
    xShow('DWId');
}


/*
*
* Builds and displays the current dialog box 
*
*/
function displayDW()
{
	HTMLCode = new String();
	
	/* Builds the header of the window */
	HTMLCode = this.createHeader();
	
	/* Builds the main content of the window */
	HTMLCode += this.createContent();
	
	/* Builds the footer of the window */
	//alert ('nb de boutons ->'+this.buttonList.length);
	HTMLCode += this.createFooter();
	/*alert('HTMLCode - >'+HTMLCode);*/
	
	// Populates the hidden container with the HTML code built
	dialogBoxID = document.getElementById('DWId');
	dialogBoxID.innerHTML = HTMLCode;
	
	/* Makes the window draggable and position it at the right position on the screen */
	DWSetup();
	
	/* Adjusts the size of the container */
	var tableWidthSize = xWidth('DWTable');
	var tableHeightSize = xHeight('DWTable');
	
	if (tableWidthSize > this.ws)
	{
		/* It means that the width size specified is too small to accomodate the real size of the table generated. Therefore, the container is resized */
		this.setContainerSize(tableWidthSize);
	}
	
	if (tableHeightSize > xHeight('DWId'))
	{
		/* It means that the height size specified is too small to accomodate the real size of the table generated. Therefore, the container is resized */
		this.setContainerHeightSize(tableHeightSize);
	}
	
	/* The window will be positionned beside the field provided */
	if(!this.positionWindow())
	{
 	   moveWindowDWToCenterPage() 
	}
	
    /* iframe is only created for IE */	
	if(xIE4Up)
	{
	   if (!document.getElementById("DWiframe"))	  
	   {
	      var iframeElement = document.createElement("iframe");
          iframeElement.id = "DWiframe";
          dialogBoxID.parentNode.appendChild (iframeElement);
       }
       
       var iframe = document.getElementById("DWiframe");
       iframe.style.top = xPageY('DWId');
       iframe.style.left = xPageX('DWId');
       iframe.style.width = xWidth('DWId');
       iframe.style.height = xHeight('DWId');
       iframe.style.position = "absolute";
       iframe.style.display = "inline";
       iframe.style.zIndex = 0;
	  
     }

	fadeManager('DWId', 100, 'this.activateDialogWindowDW()', '',this.fadeStep, this.fadeInterval);
	if (this.timeout != '')
	{
		setTimeout("this.hideDW()", this.timeout);
	}	
}

/* The window will be positionned relative to the field provided and the quadrant*/
function positionWindowDW()
{
	if (this.posElement == '' || xGetElementById(this.posElement) == null)
	{
		return false;
	}
	var curScreenHeight = xClientHeight()+xScrollTop();
	var curScreenWidth = xClientWidth() + xScrollLeft();
// call approprate moveWindow method

	var movedCleanly;

	switch (this.posQuadrant)
	{
		case BOTTOMRIGHT :
			 movedCleanly = moveWindowDWToBottomRight(this.posElement, curScreenHeight, curScreenWidth);
			 break;
		case BOTTOMLEFT :
			movedCleanly = moveWindowDWToBottomLeft(this.posElement, curScreenHeight, curScreenWidth);
			break;
		case TOPRIGHT :
			movedCleanly = moveWindowDWToTopRight(this.posElement, curScreenHeight, curScreenWidth);
			break;
		case TOPLEFT :
			movedCleanly = moveWindowDWToTopLeft(this.posElement, curScreenHeight, curScreenWidth);
			break;
		case CENTER :
			movedCleanly = moveWindowDWToCenter(this.posElement, curScreenHeight, curScreenWidth);
			break;
		case TOPCENTER :
			movedCleanly = moveWindowDWToTopCenter(this.posElement, curScreenHeight, curScreenWidth);
			break;
			
		default :
			movedCleanly = moveWindowDWToCenterPage();
			break;
	}
	if (! movedCleanly)
	{
		movedCleanly = moveWindowDWToBottomRight(this.posElement, curScreenHeight, curScreenWidth);
		if (! movedCleanly)
		{
			movedCleanly = moveWindowDWToBottomLeft(this.posElement, curScreenHeight, curScreenWidth);
		}
		if (!movedCleanly)
		{
			movedCleanly = moveWindowDWToTopRight(this.posElement, curScreenHeight, curScreenWidth);
		}
		if (!movedCleanly)
		{
    		movedCleanly = moveWindowDWToTopLeft(this.posElement, curScreenHeight, curScreenWidth);
		}
		if (!movedCleanly)
		{
    		movedCleanly = moveWindowDWToCenter(this.posElement, curScreenHeight, curScreenWidth);
		}
		if (!movedCleanly)
		{
    		movedCleanly = moveWindowDWToTopCenter(this.posElement, curScreenHeight, curScreenWidth);
		}
        
   }
   if (movedCleanly)
   {
      return true
   }
      else
   {
      return false
   }
   
}


function moveWindowDWToCenterPage ()
{	
    // No field provided - Centered on the active page
	var xPos = xScrollLeft() + (xClientWidth()/2) - (xWidth('DWId')/2);
	var yPos = xScrollTop() + (xClientHeight()/2) - (xHeight('DWId')/2);
	
	xMoveTo('DWId',xPos, yPos);

	return true;
}


function moveWindowDWToTopCenter (posElement, curScreenHeight, curScreenWidth)
{	
	// top Center of component

	var xPos = (xPageX(posElement)+ (xWidth(posElement)/2)) - (xWidth('DWId')/2);
	var yPos = yOffset = xPageY(posElement) - xHeight('DWId');
	
	var xOffset = xPos + xWidth('DWId');
	
	if (xOffset <= curScreenWidth && yOffset <= curScreenHeight)
	{
		xMoveTo('DWId',xPos,yPos);
		return true;
	}
	else
	{
		return false;
	} 
}


function moveWindowDWToCenter (posElement, curScreenHeight, curScreenWidth)
{	
	// Center of component

	var xPos = (xPageX(posElement)+ (xWidth(posElement)/2)) - (xWidth('DWId')/2);
	var yPos = (xPageY(posElement)+ (xHeight(posElement)/2)) - (xHeight('DWId')/2);
	
	var xOffset = xPos + xWidth('DWId');
	var yOffset = yPos + xHeight('DWId');
	

	if (xOffset <= curScreenWidth && yOffset <= curScreenHeight)
	{
		xMoveTo('DWId',xPos,yPos);
		return true;
	}
	else
	{
		return false;
	} 
}


function moveWindowDWToBottomRight (posElement, curScreenHeight, curScreenWidth)
{	
	// Bottom Right
	var xPos = xPageX(posElement)+ xWidth(posElement);
	var yPos = xPageY(posElement)+ xHeight(posElement);
	var xOffset = xPos + xWidth('DWId');
	var yOffset = yPos + xHeight('DWId');

	if (xOffset <= curScreenWidth && yOffset <= curScreenHeight)
	{
		xMoveTo('DWId',xPos,yPos);
		return true;
	}
	else
	{
		return false;
	}
}

function moveWindowDWToBottomLeft (posElement, curScreenHeight, curScreenWidth)
{
	// Bottom Left
	var xPos = xOffset = xPageX(posElement)-xWidth('DWId');
	var yPos = xPageY(posElement)+xHeight(posElement);
	var yOffset = yPos + xHeight('DWId');

	if ((xOffset > 0) && (yOffset <= curScreenHeight))
	{
		xMoveTo('DWId',xPos,yPos);
		return true;
	}
    else
	{
		return false;
	}
}


function moveWindowDWToTopRight (posElement, curScreenHeight, curScreenWidth)
{
	//Top Right
	var xPos = xPageX(posElement)+xWidth(posElement);
	var yPos = yOffset= xPageY(posElement)-xHeight('DWId');
	var xOffset = xPos + xWidth('DWId');

	if ((xOffset <= curScreenWidth) && (yOffset <= curScreenHeight))
	{
		xMoveTo('DWId',xPos,yPos);
		return true;
	}
	else
	{
		return false;
	}
}


function moveWindowDWToTopLeft (posElement, curScreenHeight, curScreenWidth)
{
	// Top Left
	var xPos = xOffset = xPageX(posElement) - xWidth('DWId');
	var yPos = yOffset = xPageY(posElement) - xHeight('DWId');
	
	if ((xOffset > 0) && (yOffset <= curScreenHeight))
	{
		xMoveTo('DWId',xPos,yPos);
		return true;
	}
	else
	{
		return false;
	}
}


/*
*
* Hides the current dialog box
*
*/
function hideDW(fadeStep, fadeInterval)
{
   /* iframe only exists for IE */	
   if(xIE4Up)
   {
      xHide('DWiframe'); 
   }
   fadeManager('DWId', 0, '','xHide("DWId")',fadeStep, fadeInterval);
   //xHide('DWId');
}

/*
*
*	This function sets the timeout to assign to the current object
*
*/

function setTimeoutDW(secondes)
{
	if (isNaN(secondes))
	{
		alert('MessagesWindow.setTimeout -> the parameter must be the number of seconds the window will be displayed');
	}
	if ((this.buttonNoAction == '' && this.buttonList.length == 0))
	{
		this.timeout = secondes*1000;
	}
	else
	{
		alert('MessagesWindow.setTimeout -> Buttons and Timeout are mutually exclusive.');
		return;
	}
}


/*
*
*	This function sets the way the text inside the window will be displayed.
*	Input:	textAlignment, the method to align the text: left, center or right.
*
*/
function setTextAlignmentDW(alignment)
{
	if (alignment != this.ALIGN_LEFT && alignment != this.ALIGN_CENTER && alignment != this.ALIGN_RIGHT)
	{
		alert ('MessagesWindow.setTextAlignment -> the alignment parameter specified is invalid.');
		return;
	}
	this.textAlignment = alignment;
}


/*
*
*	The following class represents a button object.
*
*/
function Button (buttonLabel, jsFunction)
{
	/* Attributes */
	this.bl = new String();
	this.jsfn = new String();
	
	/* External methods */
	this.getButtonLabel = getButtonLabelDW;
	this.getJSFunction = getJSFunctionDW;
	
	/* Internal methods */
	this.setButtonLabel = setButtonLabelDW;
	this.setJSFunction = setJSFunctionDW;
	
	/* Object construction */
	this.setButtonLabel (buttonLabel);
	this.setJSFunction(jsFunction);
}


function setButtonLabelDW(label)
{
	this.bl = label;
}	

function setJSFunctionDW(jsfunction)
{
	this.jsfn = jsfunction;
}

function getButtonLabelDW()
{
	return this.bl;
}	

function getJSFunctionDW()
{
	return this.jsfn;
}

/* The following fucntions use pieces from X Library (www.cross-browser.com) and are used to generate and handle the drag and drop event */
function DWSetup()
{
  var d1 = xGetElementById('DWId');
  xMoveTo(d1, 200, 350);
  xEnableDrag(d1, DWOnDragStart, DWOnDrag);
  xAddEventListener('DWId', 'mouseover', DWonMouseOver, false);
}

function DWonMouseOver()
{ 
  var elem = xGetElementById('DWId');
  elem.style.cursor = "move";
}

function DWOnDragStart(ele, mx, my)
{
  highZ = 1000;
  xZIndex(ele, highZ++);
  ele.totalMX = 0;
  ele.totalMY = 0;
}

function DWOnDrag(ele, mdx, mdy)
{
  var newXLeft = xLeft(ele) + mdx;
  var newXTop = xTop(ele) + mdy;
  
     xMoveTo(ele, newXLeft , newXTop );
     
     /* iframe only exists for IE */	
    // we are moving the iFrame directly here in order to increse its redraw speed.
 	 if(xIE4Up)
 	 {
        xMoveTo("DWiframe", newXLeft, newXTop );  
     }
     ele.totalMX += mdx;
     ele.totalMY += mdy;
}